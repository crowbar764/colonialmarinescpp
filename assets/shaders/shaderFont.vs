#version 330 core
layout (location = 0) in vec4 vertex; // <vec2 pos, vec2 tex>
layout (location = 1) in vec4 myColour; // <vec2 pos, vec2 tex>
out vec2 TexCoords;
out vec4 colour;

uniform mat4 view;
uniform mat4 projection;

void main()
{
    gl_Position = projection * view * vec4(vertex.xy, 0.0, 1.0);
    TexCoords = vertex.zw;
    colour = myColour;
}  