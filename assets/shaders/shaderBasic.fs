#version 330 core
out vec4 FragColor;

in vec2 TexCoord;

// texture samplers
uniform sampler2D texture0;
uniform sampler2D texture1;
uniform bool useLighting;
uniform float opacity = 1.0f;

void main()
{
	vec4 texColor = texture(texture0, TexCoord);

	if (opacity != 1.0f) {
		texColor *= vec4(1.0f, 1.0f, 1.0f, opacity);
	}

	if (useLighting) {
		vec4 lightColor = texture(texture1, TexCoord);
		FragColor = texColor * (lightColor * 4);
	} else {
		FragColor = texColor;
	}
}