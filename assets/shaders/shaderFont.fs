#version 330 core
in vec2 TexCoords;
out vec4 color;

uniform sampler2D text;
in vec4 colour;

void main()
{    
    vec4 sampled = texture(text, TexCoords);
    color = colour * sampled;
} 