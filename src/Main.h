#pragma once
#include <glad/glad.h>
#include <GLFW/glfw3.h>

class Player;

class Main
{
public:
	static GLFWwindow* window;
	inline static GLFWcursor* cursor;
	static int width;
	static int height;
	static bool running;
	static float deltaTime;
	static float currentTime;
	static bool isResizing;
	static Player* player;

	inline static int myvar;
	static inline float magic;

	static void prepareResize(int, int, bool = false);
	static bool init();
	static void close();
	static void loop();
	static bool initGL();
	static void setCursor(int shape = 0);
	
private:
	static float nextFPSPrint;
	static int resize_newWidth, resize_newHeight;
	static bool resize_skipWindowResize;
	static bool vSync;
	

	static void printFPS();
	static void resize();
	static void resize_callback(GLFWwindow*, int, int);
	static void centreWindow();
};