#pragma once

#include "BehavourTree.h"
#include "AITask_CheckForTargets.h"

class BT_Hardpoint : public BehavourTree
{
public:
	BT_Hardpoint(Mob* mob) : BehavourTree(mob) {}

	virtual AiState query() {
		if (AITask_CheckForTargets::query(mob)) {
			return AiState::DEFEND;
		}

		return AiState::IDLE;
	}
};