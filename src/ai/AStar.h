#pragma once
//#include "../global/World.h"
#include "Node.h"
#include "Heap.h"
#include "../common/IntPair.h"
#include "../common/Common.h"

class World;
class Mob;

struct PathingRequest {
	Mob* mob; // The mob which requested the path.
	IntPair start, end; // THIS IS DUMB, START CAN CHANGE MAYBE WHEN REQUEST IS DONE.
};

class AStar
{
public:
	inline static std::vector<PathingRequest*> requests;
	inline static std::vector<std::vector<IntPair*>*> paths; // Track paths to draw if flag is enabled.
	static Node* nodes[MAX_MAP_DIMENSION][MAX_MAP_DIMENSION];
	static IntPair start, end;
	inline static Node* startNode;
	inline static Node* endNode;
	inline static Node* currentNode;
	static Heap open;
	inline static PathingRequest* currentRequest;
	inline static World* world;

	static void draw();
	static void requestPath(Mob* mob, IntPair s, IntPair e);
	static void loadRequest();
	static bool findPath();
	static void getNeighbours(Node*);
	static bool getNeighbour(Node*, int, int, bool = false);
	static int getDistance(int, int, int, int);
	static std::vector<IntPair*>* retracePath();
	static void deletePath(std::vector<IntPair*>* path);
	static void trackPath(std::vector<IntPair*>* path);
	static void untrackPath(std::vector<IntPair*>* path);
	static void deleteRequest(Mob* mob);
};