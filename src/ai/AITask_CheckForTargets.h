#pragma once
//#include "../atom/entity/mob/Mob.h"
#include "../global/World.h"
#include "../common/Directions.h"
#include "../common/Math.h"

class AITask_CheckForTargets
{
public:
	static bool query(Mob* mob) {
		return false;

		int range = mob->range;
		Entity* closestTarget = NULL;
		float closestTargetDistance = INFINITY;
		World* world = mob->world;
		int chunkRange = 1; // TODO: Allow for longer range searches.

		int chunkStartX = (int) floor(mob->position.x / UNITS_PER_TILE) / CHUNK_SIZE;
		int chunkStartY = (int) floor(mob->position.y / UNITS_PER_TILE) / CHUNK_SIZE;


		// Search current and nearby chunks.
		for (int y = chunkStartY - chunkRange; y < chunkStartY + chunkRange + 1; ++y) {
			for (int x = chunkStartX - chunkRange; x < chunkStartX + chunkRange + 1; ++x) {

				// If searching out of map.
				if (x < 0 || y < 0)
					continue;

				// Check all entities in this chunk.
				for ( Entity* occupant : World::activeWorld->chunks[x][y]->occupants ) {
				//// TODO check / type, can be destructed // ????

					if ( occupant->faction == mob->faction )
						continue;

					// Get distance from target centre.
					glm::vec2 targetCentre = occupant->position + glm::vec2(occupant->spriteResolution * .5f);
					float distance = getVectorDistance(mob->position, targetCentre);

					// An very imperfect way to make a entity's size part of the distance check.
					distance -= occupant->spriteResolution / 1.5f;

					if ( distance > range * UNITS_PER_TILE )
						continue;

					if ( distance >= closestTargetDistance )
						continue;

					// THIS IS DONE ABOVE ^
					//if (occupant->faction != mob->faction && occupant->entityType == EntityType::MOB && distance < closestTargetDistance) { // CHECK IS NOT SELF IF DOING FRIENDLY SEARCH
						closestTarget = occupant;																											// occupant != mob
						closestTargetDistance = distance;
					//}
				}
			}
		}

		if (closestTarget) {
			mob->targetRef = World::getEntity(closestTarget->ID);
			//PRINT(closestTarget);
			//PRINT(Main::deltaTimeTotal);
			return true;
		}

		//PRINT("NO TARGET");
		return false;
	}
};