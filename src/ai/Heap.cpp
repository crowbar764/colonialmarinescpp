#include "Heap.h"
#include "../global/World.h"

Node* Heap::nodes[MAX_MAP_DIMENSION * MAX_MAP_DIMENSION];
int Heap::currentNodeCount;

/*
*
* HEAP:
* ///////////////////
*
* Stores all OPEN nodes in a tree like so:
* 
*						0[F:5]
*			1[F:11]					2[F:7]
*	3[F:12]			4[F:24]	5[F:8]			6[F:10]
*						  ...
* 
* node.heapIndex is the position in the tree, first is 0.
* nodes are ORDERED by their FCOST (or HCOST if FCOST is the same).
* Best node is always at position 0.
* 
*
*/

// ADD node to end of tree, then sort it UPWARDS until it fits in a correct spot.
void Heap::add(Node* node) {
	node->heapIndex = currentNodeCount;
	nodes[currentNodeCount] = node;
	sortUp(node);
	node->inOpen = true;
	++currentNodeCount;
}

// REMOVE first (best) node, then to avoid shuffle, move LAST node to 0 and sort it DOWN.
Node* Heap::removeFirst() {
	Node* firstNode = nodes[0];
	--currentNodeCount;
	nodes[0] = nodes[currentNodeCount];
	nodes[0]->heapIndex = 0;
	firstNode->inOpen = false;

	if (currentNodeCount)
		sortDown();
	
	return firstNode;
}

// Compare with children, if our cost is worse, swap down.
void Heap::sortDown() {
	Node* node = nodes[0];

	while (true) {
		int childIndexLeft = node->heapIndex * 2 + 1;
		int childIndexRight = node->heapIndex * 2 + 2;
		int swapIndex = 0;

		if (childIndexLeft < currentNodeCount) {
			swapIndex = childIndexLeft;

			if (childIndexRight < currentNodeCount) {
				if (nodes[childIndexLeft]->CompareTo(nodes[childIndexRight]) < 0) {
					swapIndex = childIndexRight;
				}
			}

			if (node->CompareTo(nodes[swapIndex]) < 0) {
				swap(node, nodes[swapIndex]);
			}
			else {
				return;
			}
		}
		else {
			return;
		}
	}
}

// Compare with parent, if cost is better, swap up.
void Heap::sortUp(Node* node) {
	int parentIndex;

	while (true) {
		parentIndex = (node->heapIndex - 1) / 2;
		Node* parentItem = nodes[parentIndex];
		if (node->CompareTo(parentItem) > 0)
			swap(node, parentItem);
		else
			break;
	}
}

// Swap positions of two elements in tree.
void Heap::swap(Node* nodeA, Node* nodeB) {
	nodes[nodeA->heapIndex] = nodeB;
	nodes[nodeB->heapIndex] = nodeA;
	int nodeAIndex = nodeA->heapIndex;
	nodeA->heapIndex = nodeB->heapIndex;
	nodeB->heapIndex = nodeAIndex;
}

void Heap::reset() {
	currentNodeCount = 0;
}