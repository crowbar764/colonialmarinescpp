#pragma once
#include "../common/IntPair.h"

class Node
{
public:

	int x, y;
	glm::vec4 colour;
	Node* parent;
	bool isOpen, inOpen, inClosed;
	int g, h, f, numOfNeigbours;
	Node* neighbours[8];
	int heapIndex;

	Node(int x, int y, bool isOpen, int g, int h) {
		this->x = x;
		this->y = y;
		this->isOpen = isOpen;

		updateValues(g, h);
	}

	void updateValues(int g, int h) {
		this->g = g;
		this->h = h;
		f = g + h;
	}

	int CompareTo(Node* nodeToCompare) {
		int compare = intCompare(f, nodeToCompare->f);
		if (compare == 0)
			compare = intCompare(h, nodeToCompare->h);

		return -compare;
	}

	int intCompare(int a, int b) {
		if (a < b)
			return -1;
		else if (a > b)
			return 1;

		return 0;
	}

	IntPair* compress() {
		return new IntPair{ x, y };
	}
};