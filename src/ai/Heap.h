#pragma once
#include "Node.h"

class Heap {
public:
	static Node* nodes[];
	static int currentNodeCount;
	
	static void add(Node*);
	static Node* removeFirst();
	static void sortDown();
	static void sortUp(Node*);
	static void swap(Node*, Node*);
	static void reset();
};