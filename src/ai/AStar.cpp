#include "AStar.h"
#include "../rendering/Renderer.h"
#include "../global/Camera.h"
#include "../atom/entity/mob/Mob.h"
#include "../subsystems/Master.h"
#include "../common/Directions.h"
#include "../atom/turf/Turf.h"
#include "../global/World.h"

#include <GLFW/glfw3.h>

IntPair AStar::start, AStar::end;
Node* AStar::nodes[MAX_MAP_DIMENSION][MAX_MAP_DIMENSION];
Heap AStar::open;

void AStar::requestPath(Mob* mob, IntPair s, IntPair e) {
	requests.push_back(new PathingRequest { mob, s, e });
}

// Load the first request in the queue.
void AStar::loadRequest() {
	PathingRequest* request = AStar::requests.front();
	requests.erase(requests.begin());
	currentRequest = request;
	world = currentRequest->mob->world;
	start = request->start;
	end = request->end;

	// Clear heap.
	open.reset();

	for ( int y { 0 }; y < world->height; ++y ) {
		for ( int x { 0 }; x < world->width; ++x ) {
			delete nodes[x][y]; // Keep seperate, tightly-packed array to make deletion faster?
			nodes[x][y] = NULL;
		}
	}

	int startEndDistance = getDistance(start.x, start.y, end.x, end.y);

	startNode = new Node {
		start.x, start.y,
		world->turfs[start.x][start.y]->isOpen,
		0, startEndDistance
	};

	endNode = new Node {
		end.x, end.y,
		world->turfs[end.x][end.y]->isOpen,
		startEndDistance, 0
	};

	nodes[start.x][start.y] = startNode;
	nodes[end.x][end.y] = endNode;

	open.add(startNode);
	startNode->inOpen = true;
}

bool AStar::findPath() {
	while ( open.currentNodeCount ) {
		// CURRENT = open-node with lowest F-COST.
		currentNode = open.removeFirst();

		// Path found.
		if (currentNode == endNode) {
			std::vector<IntPair*>* path = retracePath();

			if (BIT_CHECK(Renderer::renderFlags, RENDER_ASTAR))
				trackPath(path);

			//currentRequest->mob->onPathFound(path);

			delete(currentRequest);
			currentRequest = NULL;

			return true;
		}

		//currentNode->inOpen = false;
		currentNode->inClosed = true;
		currentNode->colour = glm::vec4{1, 0, 0, .2};

		// Get node's neighbours.
		getNeighbours(currentNode);

		for (int i = 0; i < currentNode->numOfNeigbours; ++i) {
			Node* neighbourNode = currentNode->neighbours[i];

			// If neighbour is not traversable, or is in closed, then skip.
			if (!neighbourNode->isOpen || neighbourNode->inClosed)
				continue;

			int newMovementCostToNeightbour = currentNode->g + getDistance(currentNode->x, currentNode->y, neighbourNode->x, neighbourNode->y);

			if (newMovementCostToNeightbour < neighbourNode->g || !neighbourNode->inOpen) {

				neighbourNode->updateValues(
					newMovementCostToNeightbour,
					getDistance(neighbourNode->x, neighbourNode->y, endNode->x, endNode->y)
				);

				neighbourNode->parent = currentNode;

				if (!neighbourNode->inOpen) {
					open.add(neighbourNode);
					neighbourNode->inOpen = true;
					neighbourNode->colour = glm::vec4{0, 0, 1, .2};
				}
				else {
					open.sortUp(neighbourNode);
				}
			}

		}

		if ( TICK_CHECK )
			return false;
	}

	//currentRequest->mob->pathRequested = false;

	delete(currentRequest);
	currentRequest = NULL;

	return true;
}

std::vector<IntPair*>* AStar::retracePath() {
	std::vector<IntPair*>* path = new std::vector<IntPair*>;
	Node* currentPathNode = endNode;

	while (currentPathNode != startNode) {
		path->push_back(currentPathNode->compress());
		currentPathNode = currentPathNode->parent;
	}

	return path;
}

void AStar::getNeighbours(Node* node) {
	int x = node->x;
	int y = node->y;
	unsigned int clearance = 0;

	// N
	if (getNeighbour(node, x, y - 1))
		BIT_1(clearance, DIR_N);

	// E
	if (getNeighbour(node, x + 1, y))
		BIT_1(clearance, DIR_E);

	// S
	if (getNeighbour(node, x, y + 1))
		BIT_1(clearance, DIR_S);

	// W
	if (getNeighbour(node, x - 1, y))
		BIT_1(clearance, DIR_W);
	
	// NE
	if (BIT_CHECK(clearance, DIR_N) && BIT_CHECK(clearance, DIR_E))
		getNeighbour(node, x + 1, y - 1, true);

	// SE
	if (BIT_CHECK(clearance, DIR_S) && BIT_CHECK(clearance, DIR_E))
		getNeighbour(node, x + 1, y + 1, true);

	// SW
	if (BIT_CHECK(clearance, DIR_S) && BIT_CHECK(clearance, DIR_W))
		getNeighbour(node, x - 1, y + 1, true);

	// NW
	if (BIT_CHECK(clearance, DIR_N) && BIT_CHECK(clearance, DIR_W))
		getNeighbour(node, x - 1, y - 1, true);
}

bool AStar::getNeighbour(Node* node, int x, int y, bool skipBoundsCheck) {

	// Check neighbour is inside world grid.
	if (skipBoundsCheck || (x >= 0 && x < world->width && y >= 0 && y < world->height)) {
		bool isOpen = world->turfs[x][y]->isOpen;

		if (!nodes[x][y]) {
			Node* newNode = new Node{
				x, y,
				isOpen,
				getDistance(x, y, start.x, start.y), getDistance(x, y, end.x, end.y)
			};

			nodes[x][y] = newNode;
		}

		node->neighbours[node->numOfNeigbours++] = nodes[x][y];
		
		return isOpen;
	}

	return false;
}

int AStar::getDistance(int x, int y, int x2, int y2) {
	int dstX = abs(x - x2);
	int dstY = abs(y - y2);

	if (dstX > dstY)
		return 14 * dstY + 10 * (dstX - dstY);

	return 14 * dstX + 10 * (dstY - dstX);
}

void AStar::draw() {
	glBindVertexArray(Renderer::shapeVAO);
	glBindBuffer(GL_ARRAY_BUFFER, Renderer::shapeVBO);
	Renderer::shapeShader.use();

	// THIS DRAWS THE LAST CALCULATED PATH
	//for (int y{ 0 }; y < Renderer::visibleYTiles; ++y) {
	//	for (int x{ 0 }; x < Renderer::visibleXTiles; ++x) {
	//		IntPair worldGrid = Renderer::gridToWorldGrid(x, y);
	//		Node* node = nodes[worldGrid.x][worldGrid.y];

	//		if (node) {
	//			Renderer::drawShape(
	//				(float)(node->x * Renderer::tileRes) - (Camera::X)+Renderer::tileRes / 4,
	//				(float)(node->y * Renderer::tileRes) - (Camera::Y)+Renderer::tileRes / 4,
	//				(float)Renderer::tileRes / 2, (float)Renderer::tileRes / 2,
	//				node->colour
	//			);
	//		}
	//	}
	//}

	for (std::vector<IntPair*>* path : paths) {
		for (IntPair* node : *path) {

			glm::vec4 colour = glm::vec4(0.0, .5, .5, 0.2);
			if (node == path->front())
				colour = glm::vec4(0.0, 1.0, 0.0, 0.2);

			Renderer::drawShape(
				(node->x * Renderer::tileRes) - Camera::X + Renderer::tileRes * .25f,
				(node->y * Renderer::tileRes) - Camera::Y + Renderer::tileRes * .25f,
				Renderer::tileRes * .5f, Renderer::tileRes * .5f,
				colour
			);
		}
	}
	
	Renderer::drawToBuffer(NULL, VERTEX_PAYLOAD_SHAPE);
	Renderer::ourShader.use();
	glBindVertexArray(Renderer::VAO);
	glBindBuffer(GL_ARRAY_BUFFER, Renderer::VBO);
}

void AStar::deletePath(std::vector<IntPair*>* path) {
	if (BIT_CHECK(Renderer::renderFlags, RENDER_ASTAR))
		untrackPath(path);

	for (IntPair* node : *path) {
		delete node;
	}

	delete path;
}

void AStar::deleteRequest(Mob* mob) {

	// If currently being processed...
	if ( currentRequest && currentRequest->mob == mob ) {
		delete(currentRequest);
		currentRequest = NULL;
		return;
	}

	// If still in queue...
	for ( int i = 0; i < requests.size(); i++ ) {
		if ( requests[i]->mob == mob ) {
			delete(requests[i]);
			requests.erase(requests.begin() + i);
			return;
		}
	}
}

void AStar::trackPath(std::vector<IntPair*>* path) {
	paths.push_back(path);
}

void AStar::untrackPath(std::vector<IntPair*>* path) {
	//std::vector<std::vector<IntPair*>*>::iterator it = std::find(paths.begin(), paths.end(), path);
	//paths.erase(it); 
	//vErase(paths, path); // try changing to fasterase
	vErase(paths, path);
}