#pragma once

class Mob;

class BehavourTree
{
public:
	Mob* mob;

	BehavourTree(Mob* mob) {
		this->mob = mob;
	}

	virtual AiState query() {
		PRINT("BASE BT QUERY");
		return AiState::IDLE;
	}
};