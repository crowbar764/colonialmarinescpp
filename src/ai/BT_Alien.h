#pragma once

#include "BehavourTree.h"
#include "AITask_CheckForTargets.h"

class BT_Alien : public BehavourTree
{
public:
	BT_Alien(Mob* mob) : BehavourTree(mob) {}

	virtual AiState query() {

		if ( mob->moveRequest && !mob->movePath ) {
			return AiState::WAITING_FOR_PATH;
		}

		if ( mob->movePath ) {
			return AiState::MOVE;
		}

		if ( AITask_CheckForTargets::query(mob) ) {
			return AiState::DEFEND;
		}

		return AiState::IDLE;
	}
};