#pragma once

#include "Flow_Heap.h"
#include "../../common/IntPair.h"

struct Flow_Window;
struct Flow_Network;
class World;
class Mob;

class Flow_AStar {
public:
	inline static unsigned int lastMobPathRunID;
	inline static unsigned int lastFlowPathRunID;
	inline static IntPair startChunk, goalChunk;
	inline static Flow_Heap open;
	inline static Flow_Window* currentWindow;
	inline static Flow_Network* startNetwork;
	inline static Flow_Network* endNetwork;

	static void prepareRequest(IntPair goalGrid, World* world);
	static bool prepareMob(Mob* mob);
	static bool FindMobPath();
	static void retracePath();
	static int getDistance(int x, int y, int x2, int y2);
};