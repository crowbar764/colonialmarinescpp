#include "PathPackage.h"
#include "../../atom/entity/mob/Mob.h"
#include "../../atom/effect/MarkerBig.h"
#include "../../common/Directions.h"
#include "../../common/Conversions.h"
#include "../../common/Math.h"
#include "../../atom/turf/Turf.h"
#include "../../global/World.h"
#include "../../subsystems/Master.h"
#include "../../subsystems/SSPathing.h"

// Copied and modified from movement comp.

/*
* FORMATION GENERATION LOGIC:
* 
* 1. Identify which units should be part of the formation and which are too far away.
* ???
* 
* 2. Decide on formation / pilot starting location.
* ???
* 
* 3. Generate slot positions for each unit.
* 3.1	Decide the formation width based on how many unit to include.
* 3.2	For each row,
*			Check the middle column
*			Check the left columns, from -1 to furthest left.
*			Check the right columns, from 1 to furthest right.
*		If left or right hits a wall, shrink that direction's range forever.
*			To check if blocked, each offset vector is rotated and shifted into the world so it matches up.
* 
* 4. Assign each close unit to a slot.
* 4.1	Populate a list of the units that are close enough.
* 4.2	For each slot,
*			Sort list by distance to the slot (round value and use a lookup table for close enough units).
*				(OR maybe distance is not needed. Just add vector axis and smallest wins).
*			Assign slot to closest unit (top of list).
*			Remove unit from list.
* 
*	[X] [ ] [ ] [ ] [ ]
*	[ ] [ ] [ ] [ ] [ ]
*	[O] [ ] [ ] [ ] [ ]
*	[ ] [ ] [ ] [ ] [ ]
*	[ ] [ ] [ ] [ ] [ ]
*
*	[X] [ ] [O] [ ] [ ]
*	[ ] [ ] [ ] [ ] [ ]
*	[ ] [ ] [ ] [ ] [ ]
*	[ ] [ ] [ ] [ ] [ ]
*	[ ] [ ] [ ] [ ] [ ]
*
*	[ ] [ ] [ ] [ ] [ ]
*	[ ] [ ] [ ] [ ] [ ]
*	[ ] [ ] [X] [ ] [O]
*	[ ] [ ] [ ] [ ] [ ]
*	[ ] [ ] [ ] [ ] [ ]
* 
* 
* 
*	TODO: When removing all units , it does one at a time and lots of pointless code like recalculating speed. Fix this.
*/

// Slow down group movement at all times by this much to let stragglers catch up.
constexpr float GROUP_SPEED_SLOWDOWN = .01f;
// How close the pilot needs to be to the goal tile to stop moving.
constexpr float GROUP_GOAL_ALLOWANCE = 1.0f;
// If the current flow instruction is more than this many radians in direction difference, recalculate slot positions.
constexpr float GROUP_ROTATION_UPDATE_TRIGGER = .05f;

// Sample flowfield and update group position based on it.
void PathPackage::tick() {
	//if ( singleUnitMode || atDestination ) 
	//	return;

	//// Impulse in flowfield's direction.
	//float newDirection = flowTick(this, formationPosition, groupSpeed, &velocity);

	//// Rate limit: If we want to rotate too much from our current rotation, then recalculate slot positions.
	//if ( abs(newDirection - groupRotation) > GROUP_ROTATION_UPDATE_TRIGGER ) 
	//	setRotation(newDirection);

	//// FRICTION.
	//velocity *= pow(.0001f, FIXED_TIMESTEP);

	//// Actually update position.
	//formationPosition += velocity;

	//// If at end, halt.
	//if ( getVectorDistanceSquare(formationPosition, goal + .5f) < GROUP_GOAL_ALLOWANCE )
	//	atDestination = true;
}

// Update slot positions based on new group heading.
void PathPackage::setRotation(float newRotation) {
	groupRotation = newRotation;

	for ( int i = 0; i < slots.size(); i++ )
		slots[i]->offset = rotateVector(slots[i]->gridSpot, groupRotation);
}

void PathPackage::assignSlots() {


	std::vector<Mob*> unitsToAssign = units;


	for ( int slot = 0; slot < slots.size(); slot++ ) {
		Mob* closestMob = NULL;
		float closestDistance = INFINITY;

		for ( Mob* unit : unitsToAssign ) {
			float distance = abs(unit->position.x - slots[slot]->offset.x) + abs(unit->position.y - slots[slot]->offset.y);

			if ( distance < closestDistance ) {
				closestDistance = distance;
				closestMob = unit;
			}
		}

		closestMob->groupOffset = slots[slot];
		slots[slot]->unit = closestMob;
		closestMob->movementMode = MOVEMENT_MODE_GROUP;
		vErase(unitsToAssign, closestMob);

		// No more units to add!
		if ( !unitsToAssign.size() )
			return;
	}
}

void PathPackage::updateFormation() {
	// Yuck, square root! Can avoid? Lookup table for any number lower than 100. Above 100 uses sqrt. Or just have set widths for value thresholds.
	// Please double check the height ceil makes sense.
	// Change to be wider rather than square.
	int idealWidth = (int) ceil(sqrt(units.size()));
	//int idealHeight = (int) ceil(numOfUsers / idealWidth);

	idealWidth = 5; // Hardcoded hehehe.

	// Which column is the middle (odd values only).
	int centreColumn = (int) ceil(idealWidth / 2.0f);

	// How many columns per side. E.g. 7 columns gives 3.
	//int centreOffset = centreColumn - 1;

	IntPair currentGridPosition = worldToGrid(formationPosition);
	//groupRotation = getFlow(currentGridPosition);
	slotsNeeded = (int) units.size();

	//PRINTIP("Formaa", currentGridPosition);
	//PRINT("Rotation" << formationRotation);

	int row = 1;
	int leftClearance = -centreColumn;
	int rightClearance = centreColumn;

	slots.clear();
	// Needed?
	//memset(formationSlots, NULL, sizeof(formationSlots[0][0]) * 11 * 100);

	while ( true ) {
		// Check middle column.
		// If blocked, then we don't want to continue at all.
		if ( checkSlotCollision(0, row, groupRotation, currentGridPosition) ) {
			PRINT("EXIT");
			break;
		}

		if ( !slotsNeeded )
			goto exitloop;

		// Check left columns.
		for ( int x = -1; x > leftClearance; x-- ) {
			if ( checkSlotCollision(x, row, groupRotation, currentGridPosition) ) {
				leftClearance = x;
				break;
			}

			if ( !slotsNeeded )
				goto exitloop;
		}

		// Check right columns.
		for ( int x = 1; x < rightClearance; x++ ) {
			if ( checkSlotCollision(x, row, groupRotation, currentGridPosition) ) {
				rightClearance = x;
				break;
			}

			if ( !slotsNeeded ) {
				goto exitloop;
			}
		}

		row++;
	}

exitloop:
	return;
}

bool PathPackage::checkSlotCollision(int x, int y, float rotation, IntPair currentGridPos) {
	//PRINT("----------");
	//PRINT("Checking for collision: " << x << " " << y);

	// Rotate the offset around the origin (0, 0) which is the top middle.
	glm::vec2 rotatedLocalCoordinates = rotateVector({ (float) x, (float) y }, rotation);

	// Round to nearest local grid coords, roundf is needed otherwise -0.99999.. rounds to 0 instead.
	IntPair roundedLocalCoords = { (int) roundf(rotatedLocalCoordinates.x), (int) roundf(rotatedLocalCoordinates.y) };

	// Convert local grid coordinates to world grid coordinates.
	IntPair worldGrid = roundedLocalCoords + currentGridPos;

	//PRINT("Checking world: " << worldGrid.x << " " << worldGrid.y);

	// Out of bounds check.
	if ( worldGrid.x < 0 || worldGrid.x >= leader->world->width || worldGrid.y < 0 || worldGrid.y >= leader->world->height )
		return true;

	//PRINT("Checking: " << x << " " << y);

	if ( leader->world->turfs[worldGrid.x][worldGrid.y]->isOpen ) {
		//PRINT("Approved offset: " << rotatedLocalCoordinates.x << " " << rotatedLocalCoordinates.y);

		PathPackageSlot* slot = new PathPackageSlot;
		slot->offset = { (rotatedLocalCoordinates.x) * UNITS_PER_TILE, rotatedLocalCoordinates.y * UNITS_PER_TILE };
		slot->gridSpot = { x, y };
		//glm::vec2 offset = { (rotatedLocalCoordinates.x) * UNITS_PER_TILE, rotatedLocalCoordinates.y * UNITS_PER_TILE };
		slots.push_back(slot);
		//PRINTV("Offset", offset);

		//formationSlots[x + centreOffset][y] = new PathPackageSlot { offset };
		slotsNeeded--;

		//PRINT("Slot found: " << x << " " << y);

		return false;
	}

	return true;
}

void PathPackage::checkCollision() {
	World* world = units[0]->world;

	for ( PathPackageSlot* slot : slots ) {
		if ( !slot->valid )
			continue;

		glm::vec2 slotWorldLocation = formationPosition + slot->offset;
		IntPair slotGridLocation = worldToGrid(slotWorldLocation);
		Turf* slotTurf = world->turfs[slotGridLocation.x][slotGridLocation.y];

		if ( !slotTurf->isOpen ) {
			slot->valid = false;
			slot->unit->movementMode = MOVEMENT_MODE_FLOWFIELD;
			//slot->unit->startNextPathSegment();
		}
	}
}

void PathPackage::calculateGroupSpeed() {
	if ( units.size() == 1 ) {
		groupSpeed = units[0]->speed;
		return;
	}

	float slowestSpeed = INFINITY;

	for ( Mob* unit : units ) {
		if ( unit->speed < slowestSpeed ) {
			slowestSpeed = unit->speed;
		}
	}

	groupSpeed = slowestSpeed - GROUP_SPEED_SLOWDOWN;

	//PRINT("Speed group: " << groupSpeed);
}

//float PathPackage::getFlow(IntPair vec) const {
//	return flowfield[vec.x][vec.y] - 1;
//}

void PathPackage::removeUnit(Mob* unit) {
	// Switch to fast erase?
	//Common::eraseFast(units, unit);
	vErase(units, unit);
	calculateGroupSpeed();

	if ( units.size() > 1 ) {


		//vErase(slots, unit->groupOffset);
		//delete(unit->groupOffset);
		//unit->groupOffset = NULL;

		// One unit left. Switch it to single entity movement mode.
		//if ( units.size() == 1 ) {
			//singleEntityMode = true;
			//PRINT("Single mode");
			//units[0]->movementMode = MOVEMENT_MODE_FLOWFIELD;
			//units[0]->startNextPathSegment();
		//}
	} else if ( units.size() == 1 ) {
		singleUnitMode = true;
	}


	//PRINT("User removed.");

	if ( !units.size() )
		delete(this);
}

PathPackage::~PathPackage() {

	//PRINT("Path expend, deleting it.");
	//Common::eraseFast(Master::SSPathing->activePaths, this);
	vErase(Master::SSPathing->activePaths, this);
}