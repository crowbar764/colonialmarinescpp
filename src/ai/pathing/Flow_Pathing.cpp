#include "Flow_Pathing.h"
#include "Flow_AStar.h"
#include "../../subsystems/Master.h"
#include <chrono>
#include "../../common/Directions.h"
#include "../../common/Math.h"
#include "../../atom/turf/Turf.h"
#include "../../atom/entity/mob/Mob.h"
#include "../../global/World.h"
#include "PathPackage.h"
#include "../../Main.h"
#include "../../subsystems/SSPathing.h"

// (optimisation) Only create flowfields for chunks in the macro ASTAR path.
//#define PATH_CHUNKS_ONLY

// When generating flow directions, any tile this many spaces away from the goal will ignore walls trying to push away.
static constexpr int GOAL_SPACING = 3;

/**
 * Adds a request to the queue that will eventually notify mobs via signal when their PathPackage is ready.
 *
 * @param mobs
 *            - The mob(s) that want the path.
 * @param goal
 *            - The grid coordinates of the destination.
 */
F_PathingRequest* Flow_Pathing::requestPath(std::vector<Mob*> mobs, IntPair goal) {
	F_PathingRequest* request = new F_PathingRequest { mobs, goal };
	request->delay = Main::currentTime + .25f;
	requests.push_back(request);

	return request;
}

void Flow_Pathing::loadRequest() {
	currentRequest = requests.front();
	requests.erase(requests.begin());
}

void F_PathingRequest::removeMob(Mob* mob) {
	// If this is the last mob, just delete the request instead.
	if ( mobs.size() == 1 ) {
		if ( Flow_Pathing::currentRequest == this )
			Flow_Pathing::currentRequest = NULL; // TODO, cleanup half-complete requests.
		else
			vErase(Flow_Pathing::requests, this);

		delete(this);
		return;
	}

	vErase(mobs, mob);
}

/**
 * [PRIVATE] Acts upon the current request and broadcasts signals to mobs with a completed pathPackage.
 */
void Flow_Pathing::findPath() {
	//if ( Main::deltaTimeTotal < currentRequest->delay )
		//return;

	//PRINT("Calculating path...");

	/*
	* TODO:
	* Try disabling path merging. Performance might be untouched, while allowing better mass-flows.
	* Cleanup API.
	* AStar cost value updating to save better route.
	* Good mob path collision.
	* Test for mapping edge cases.
	* Add island IDs to networks to identify impossible paths straight away. (Should this ever happen? Maps should not be segregated. Also quite expensive to update).
	* Research if we can 'merge' a chunk's windows that lead directly to the same network.
	* Research if we can switch to one window per window instead of two.
	* Add a second pass for 'large' mobs. Clearance could be a pre-calculated bool for every tile.
	* Handle pathing failure.
	* Handle mapping updates mid-run.
	* Handle package cleanup if it finishes / runs out of users.
	* What if some mobs fail to path?
	*/

	auto startTime = std::chrono::system_clock::now();
	
	world = currentRequest->mobs[0]->world;

	Flow_AStar::prepareRequest(currentRequest->goal, world);

	// Only proceed with mobs that can use the path.
	std::vector<Mob*> successfulMobs;
	for ( Mob* mob : currentRequest->mobs )
		if ( Flow_AStar::prepareMob(mob) )
			successfulMobs.push_back(mob);

	if ( !successfulMobs.size() ) {
		PRINT("No successful mobs");
		reset();
		return;
	}

	package = new PathPackage;
	package->goal = currentRequest->goal;
	package->units = successfulMobs; // what if this is no longer correct?
	//package->numOfUsers = (short) successfulMobs.size(); // what if this is no longer correct?

	// Loop through all chunks and create the flowfield objects for chunks that are part of the path.
	//for ( int chunkY = 0; chunkY < CHUNKS; chunkY++ ) {
	//	for ( int chunkX = 0; chunkX < CHUNKS; chunkX++ ) {
	//		if ( !Flow_Pathing::pathChunks[chunkX][chunkY] )
	//			continue;

	//		// Create flowfield object.
	//		//FlowfieldWrapper* flowfield = new FlowfieldWrapper;
	//		//package->chunks[chunkX][chunkY] = flowfield;

	//		// Initialise all tile directions from 0 (north) to NONE.
	//		memset(package->flowfield, DIR_SMPL_NONE, sizeof(package->flowfield[0][0]) * MAX_MAP_DIMENSION * MAX_MAP_DIMENSION);
	//	}
	//}

	// Initialise all tile directions from 0 (north) to 8 (NONE).
	// CAN'T MEMSET FLOATS.
	memset(package->flowfield, FLOW_NONE, sizeof(package->flowfield[0][0]) * MAX_MAP_DIMENSION * MAX_MAP_DIMENSION);


	// Set starting tile.

	package = package;
	//world = world;
	goal = currentRequest->goal;

	//Flow_FloodFillNode* thing = new Flow_FloodFillNode();

	//std::vector<Flow_FloodFillNode*> arr1, arr2;

	//arr1.emplace_back(thing);

	//arr2 = arr1;

	//arr1.clear();

	//PRINT_CYAN("Array 1 size: " << arr1.size());
	//PRINT_CYAN("Array 2 size: " << arr2.size());

	//DebugLine::clearLines();
	//losPass(package, world);


	

	//LOOP(1000) {
		firstWaveFront.emplace_back(currentRequest->goal.x, currentRequest->goal.y, FLOW_NONE);
		package->flowfield[currentRequest->goal.x][currentRequest->goal.y] = FLOWFIELD_LOS;

		flood(true);

		//firstWaveFront = secondWaveFront;
		//secondWaveFront.clear();

		memset(floodCache, 0, sizeof(floodCache[0][0]) * MAX_MAP_DIMENSION * MAX_MAP_DIMENSION);
		firstWaveFront.emplace_back(currentRequest->goal.x, currentRequest->goal.y, FLOW_NONE);

		// Direct all pathable tiles.
		flood(false);



	

	package->singleUnitMode = true;


	for ( Mob* mob : successfulMobs ) {
		mob->movementMode = MOVEMENT_MODE_FLOWFIELD;
		package->singleUnitMode = true;
	}

	Master::SSPathing->activePaths.push_back(package);

	for ( Mob* mob : successfulMobs )
		mob->onPathFound(package);

	reset();

	//PRINT(package->flowfield[5][6]);

	//for ( int y = 0; y < MAX_MAP_DIMENSION; y++ ) {
	//	for ( int x = 0; x < MAX_MAP_DIMENSION; x++ ) {
	//		PRINT(package->flowfield[x][y]);
	//	}
	//}

	//auto endTime = std::chrono::system_clock::now();
	//auto elapsedTime = endTime - startTime;

	//PRINT("Path took: " << elapsedTime.count() / 1000);
}

// With pathChunks populated, we now know every chunk in the path.
// Now we do a queue-based flood fill outward from the goal until every pathable tile has a direction set.
// We cannot use recursive flood fill here as the order would be chaos and will not set directions correctly.
// Result is the PathPackage has its flowfield directions set.
// TODO: What if goal is next to a wall.
void Flow_Pathing::flood(bool losPass) {
	while ( firstWaveFront.size() ) {
		Flow_FloodFillNode currentTile = firstWaveFront.front();
		firstWaveFront.erase(firstWaveFront.begin());



#ifdef PATH_CHUNKS_ONLY
		int chunkX = (int) floor(currentTile.x / CHUNK_SIZE);
		int chunkY = (int) floor(currentTile.y / CHUNK_SIZE);
		//// This tile is in a chunk we don't want to process.
		if ( !pathChunks[chunkX][chunkY] )
			continue;
#endif

		if ( losPass && !checkLOS(currentTile.x, currentTile.y, currentTile.parentDirection) )
			continue;

		unsigned int clearance = 0;

		if ( world->turfs[currentTile.x][currentTile.y - 1]->isOpen )
			BIT_1(clearance, DIR_BIT_N);

		if ( world->turfs[currentTile.x + 1][currentTile.y]->isOpen )
			BIT_1(clearance, DIR_BIT_E);

		if ( world->turfs[currentTile.x][currentTile.y + 1]->isOpen )
			BIT_1(clearance, DIR_BIT_S);

		if ( world->turfs[currentTile.x - 1][currentTile.y]->isOpen )
			BIT_1(clearance, DIR_BIT_W);


		// N
		if (
			!floodCache[currentTile.x][currentTile.y - 1] &&
			BIT_CHECK(clearance, DIR_BIT_N))
		{
			firstWaveFront.emplace_back(currentTile.x, currentTile.y - 1, DIR_SMPL_S);
			floodCache[currentTile.x][currentTile.y - 1] = FLOODCACHE_FILLED;
		}

		// E
		if ( 
			!floodCache[currentTile.x + 1][currentTile.y] &&
			BIT_CHECK(clearance, DIR_BIT_E) )
		{
			firstWaveFront.emplace_back(currentTile.x + 1, currentTile.y, DIR_SMPL_W);
			floodCache[currentTile.x + 1][currentTile.y] = FLOODCACHE_FILLED;
		}

		// S
		if ( 
			!floodCache[currentTile.x][currentTile.y + 1] &&
			BIT_CHECK(clearance, DIR_BIT_S) )
		{
			firstWaveFront.emplace_back(currentTile.x, currentTile.y + 1, DIR_SMPL_N);
			floodCache[currentTile.x][currentTile.y + 1] = FLOODCACHE_FILLED;
		}

		// W
		if ( 
			!floodCache[currentTile.x - 1][currentTile.y] &&
			BIT_CHECK(clearance, DIR_BIT_W) )
		{
			firstWaveFront.emplace_back(currentTile.x - 1, currentTile.y, DIR_SMPL_E);
			floodCache[currentTile.x - 1][currentTile.y] = FLOODCACHE_FILLED;
		}

		// NE
		if (
			!floodCache[currentTile.x + 1][currentTile.y - 1] &&
			BIT_CHECK(clearance, DIR_BIT_N) && BIT_CHECK(clearance, DIR_BIT_E) &&
			world->turfs[currentTile.x + 1][currentTile.y - 1]->isOpen )
		{
			firstWaveFront.emplace_back(currentTile.x + 1, currentTile.y - 1, DIR_SMPL_SW);
			floodCache[currentTile.x + 1][currentTile.y - 1] = FLOODCACHE_FILLED;
		}

		// SE
		if (
			!floodCache[currentTile.x + 1][currentTile.y + 1] &&
			BIT_CHECK(clearance, DIR_BIT_S) && BIT_CHECK(clearance, DIR_BIT_E) &&
			world->turfs[currentTile.x + 1][currentTile.y + 1]->isOpen )
		{
			firstWaveFront.emplace_back(currentTile.x + 1, currentTile.y + 1, DIR_SMPL_NW);
			floodCache[currentTile.x + 1][currentTile.y + 1] = FLOODCACHE_FILLED;
		}

		// SW
		if (
			!floodCache[currentTile.x - 1][currentTile.y + 1] &&
			BIT_CHECK(clearance, DIR_BIT_S) && BIT_CHECK(clearance, DIR_BIT_W) &&
			world->turfs[currentTile.x - 1][currentTile.y + 1]->isOpen )
		{
			firstWaveFront.emplace_back(currentTile.x - 1, currentTile.y + 1, DIR_SMPL_NE);
			floodCache[currentTile.x - 1][currentTile.y + 1] = FLOODCACHE_FILLED;
		}

		// NW
		if (
			!floodCache[currentTile.x - 1][currentTile.y - 1] &&
			BIT_CHECK(clearance, DIR_BIT_N) && BIT_CHECK(clearance, DIR_BIT_W) &&
			world->turfs[currentTile.x - 1][currentTile.y - 1]->isOpen )
		{
			firstWaveFront.emplace_back(currentTile.x - 1, currentTile.y - 1, DIR_SMPL_SE);
			floodCache[currentTile.x - 1][currentTile.y - 1] = FLOODCACHE_FILLED;
		}

		if ( !losPass && package->flowfield[currentTile.x][currentTile.y] != FLOWFIELD_LOS )
			package->flowfield[currentTile.x][currentTile.y] = currentTile.parentDirection;
		
	}
}

bool Flow_Pathing::checkLOS(int x, int y, uint8_t parentDirection) {
	int xDif = goal.x - x;
	int yDif = goal.y - y;

	int xDifAbs = abs(xDif);
	int yDifAbs = abs(yDif);
	int xDifOne = sign(xDif);
	int yDifOne = sign(yDif);

	bool hasLos = false;

	//Check the direction we are furtherest from the destination on (or both if equal)
	// If it has LOS then we might

	//Check in the x direction
	if ( xDifAbs >= yDifAbs ) {
		if ( package->flowfield[x + xDifOne][y] == FLOWFIELD_LOS ) {
			hasLos = true;
		}
	}

	//Check in the y direction
	if ( yDifAbs >= xDifAbs ) {
		if ( package->flowfield[x][y + yDifOne] == FLOWFIELD_LOS ) {
			hasLos = true;
		}
	}

	// A more strict version of the diagonal-checking version above.
	if ( yDifAbs > 0 && xDifAbs > 0 ) {
		if (package->flowfield[x + xDifOne][y + yDifOne] != FLOWFIELD_LOS ||
			!world->turfs[x + xDifOne][y]->isOpen ||
			!world->turfs[x][y + yDifOne]->isOpen) {
			hasLos = false;
		}
	}

	if ( hasLos ) {
		package->flowfield[x][y] = FLOWFIELD_LOS;
	} else {
		//secondWaveFront.emplace_back(x, y, parentDirection, true);
	}

	return hasLos;
}

void Flow_Pathing::reset() {
	delete(currentRequest);
	currentRequest = NULL;

	// Reset.
	memset(pathChunks, false, sizeof(pathChunks[0][0]) * CHUNKS * CHUNKS);
	memset(floodCache, 0, sizeof(floodCache[0][0]) * MAX_MAP_DIMENSION * MAX_MAP_DIMENSION);
}