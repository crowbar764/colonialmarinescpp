#pragma once
#include "../../common/Directions.h"
#include "../../components/Comp_FlowFollower.h"
#include "../../atom/entity/mob/Mob.h"
#include "../../common/IntPair.h"

struct PathPackageSlot {
	glm::vec2 offset, gridSpot;
	float size = UNITS_PER_TILE;
	bool valid; // Is the slot not in a wall?
	Mob* unit;
};

class PathPackage : public FlowFollower {
public:
	// A finalised 2D array with the path chunks filled with direction instructions.
	uint8_t flowfield[MAX_MAP_DIMENSION][MAX_MAP_DIMENSION];

	glm::vec2 formationPosition;
	//glm::vec2 pilotPosition; // Because the pilot can adjust seperately from the group.
	float groupSpeed;
	glm::vec2 velocity;
	// The currently used rotation of the slots. Is NOT the current flow tile rotation.
	float groupRotation = FLOW_NONE;

	//int formationWidth, formationHeight;
	Mob* leader; // The formation abstract connected mob.

	// Each slot is a vector worldPos offset from the pilot.
	//PathPackageSlot* formationSlots[11][100]; // magic
	//std::vector<glm::vec2> slots; // debug to render.
	std::vector<PathPackageSlot*> slots;

	IntPair goal;
	std::vector<Mob*> units;
	//unsigned short numOfUsers;

	//bool singleEntityMode = false;
	bool atDestination = false;
	bool singleUnitMode = false;

	int slotsNeeded;

	void tick();

	void removeUnit(Mob* unit);

	void assignSlots();

	void updateFormation();

	void setRotation(float newRotation);

	bool checkSlotCollision(int x, int y, float rotation, IntPair currentGridPos);

	void checkCollision();

	void calculateGroupSpeed();

	//float getFlow(IntPair vec) const;

	~PathPackage();
};