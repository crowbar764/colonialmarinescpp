#pragma once

struct Flow_Window;

class Flow_Heap {
public:
	static Flow_Window* nodes[];
	static int currentNodeCount;
	
	static void add(Flow_Window*);
	static Flow_Window* removeFirst();
	static void sortDown();
	static void sortUp(Flow_Window*);
	static void swap(Flow_Window*, Flow_Window*);
	static void reset();
};