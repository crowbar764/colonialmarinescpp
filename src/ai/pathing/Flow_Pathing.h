#pragma once
#include "../../common/IntPair.h"
#include "../../common/Common.h"

constexpr int FLOODCACHE_NULL = 0;
constexpr int FLOODCACHE_FILLED = 1;
constexpr int FLOODCACHE_NEAR_WALL = 2;
constexpr int FLOODCACHE_WAVE_FRONT_BLOCKED = 4;

//struct IntPair;
struct Flow_Network;
//struct Flow_Window;
class PathPackage;
struct Chunk;
class Mob;
class World;

struct Flow_Window {
	int id;
	Chunk* chunk; // The chunk this window is in.
	std::vector<IntPair> tiles; // The LOCAL TILE COORDS this window occupies.
	std::vector<Flow_Window*> connections; // Other windows this one connects to.
	bool linked; // Has this window calculated its local chunk links yet? (Used in the generation process).
	Flow_Network* network;

	//int chunkX, chunkY;
	bool inOpen; //, isOpen, inClosed;
	int g, h, f;
	int heapIndex;

	int CompareTo(Flow_Window* nodeToCompare) {
		int compare = intCompare(f, nodeToCompare->f);
		if ( compare == 0 )
			compare = intCompare(h, nodeToCompare->h);

		return -compare;
	}

	int intCompare(int a, int b) {
		if ( a < b )
			return -1;
		else if ( a > b )
			return 1;

		return 0;
	}
};

// A section of a chunk that is pathable.
struct Flow_Network {
	std::vector<Flow_Window*> windows;
	unsigned int flowPathRunID = 0; // The ID of the current group pathing request. Used to combine paths between mobs.
	unsigned int mobPathRunID = 0; // The ID of the current mob pathing request. Used to keep track of what networks have been checked.

	// The window that breached into this chunk. (The window that isn't in this chunk but the neighbouring chunk).
	// Is this changable to a better parent?
	// Used to backtrack and create the finished path links.
	Flow_Network* parent; 
};

struct F_PathingRequest {
	std::vector<Mob*> mobs; // The mob(s) which requested the path.
	IntPair goal;
	float delay;

	void removeMob(Mob* mob);
};

struct Flow_FloodFillNode {
	int x, y;
	uint8_t parentDirection;
	bool wasLOSBorder;
};

class Flow_Pathing {
public:
	inline static std::vector<F_PathingRequest*> requests;
	inline static F_PathingRequest* currentRequest;
	inline static bool pathChunks[CHUNKS][CHUNKS];
	inline static std::vector<Flow_FloodFillNode> firstWaveFront;
	inline static std::vector<Flow_FloodFillNode> secondWaveFront;
	inline static short floodCache[MAX_MAP_DIMENSION][MAX_MAP_DIMENSION];

	inline static PathPackage* package;
	inline static IntPair goal;
	inline static World* world;

	static F_PathingRequest* requestPath(std::vector<Mob*> mobs, IntPair goal);
	static void loadRequest();
	static void findPath();
	static void flood(bool losPass);

	static bool checkLOS(int x, int y, uint8_t parentDirection);
	static void reset();
};