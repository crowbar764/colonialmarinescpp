#include "../../subsystems/SSPathing.h"
#include "../../atom/turf/Turf.h"
#include "Flow_Pathing.h"
#include "../../global/World.h"

void SSPathing::generatePrePathing() {
	//int numChunks = 10; // HARDCODED.

	//generateCostField();
	//generateIntegrationField();

	// Generate windows.
	for ( int chunkY = 0; chunkY < NUMBER_OF_CHUNKS; chunkY++ ) {
		for ( int chunkX = 0; chunkX < NUMBER_OF_CHUNKS; chunkX++ ) {
			generateWindowsForChunk(chunkX, chunkY);
		}
	}

	// Generate edges.
	for ( int chunkY = 0; chunkY < NUMBER_OF_CHUNKS; chunkY++ ) {
		for ( int chunkX = 0; chunkX < NUMBER_OF_CHUNKS; chunkX++ ) {
			generateEdgesForChunk(chunkX, chunkY);
		}
	}

	//LOOP(1000) {
		//resetChunk(1, 1);
	//}
}

// Create two windows; One for the chunk, one for the chunk it touches.
// Connect these two chunks together with an edge connection.
void SSPathing::createWindow(int chunkX, int chunkY, int xOffset, int yOffset, std::vector<IntPair> windowTiles, std::vector<IntPair> neighbourWindowTiles) {
	Chunk* chunk = World::activeWorld->chunks[chunkX][chunkY];
	Chunk* neighbourChunk = World::activeWorld->chunks[chunkX + xOffset][chunkY + yOffset];

	// Create window objects.
	Flow_Window* window = new Flow_Window { ++lastWindowID, chunk, windowTiles };
	Flow_Window* neighbourWindow = new Flow_Window { ++lastWindowID, neighbourChunk, neighbourWindowTiles };

	// Make connections.
	window->connections.push_back(neighbourWindow);
	neighbourWindow->connections.push_back(window);

	// Save to chunk.
	chunk->windows.insert({ window->id, window });
	neighbourChunk->windows.insert({ neighbourWindow->id, neighbourWindow });
}

// Should run on each chunk (won't run on out of bounds edges)
// for EAST then SOUTH edges of chunk
// step along each tile and check if passable to neighbouring chunk
// when we reach end of chunk, or hit impassable tile, end the window and save it to current and neighbouring chunk.
void SSPathing::generateWindowsForChunk(int chunkX, int chunkY, bool allDirections) {
	//int numChunks = 10; // HARDCODED.
	int windowStart = -1;

	// East.
	if ( chunkX != NUMBER_OF_CHUNKS - 1 ) {
		int turfX = (chunkX * CHUNK_SIZE) + CHUNK_SIZE - 1;
		for ( int localOffset = 0; localOffset < CHUNK_SIZE; localOffset++ ) { // For every tile along.
			int turfY = (chunkY * CHUNK_SIZE) + localOffset;

			// If tile is open on both sides.
			if ( World::activeWorld->turfs[turfX][turfY]->isOpen && World::activeWorld->turfs[turfX + 1][turfY]->isOpen ) {

				// Start new window
				if ( windowStart == -1 ) {
					windowStart = localOffset;
				}

				// previous tile was window closing.
			} else if ( windowStart != -1 ) {
				std::vector<IntPair> windowTiles, neighbourWindowTiles;
				for ( int i = windowStart; i < localOffset; i++ ) {
					windowTiles.emplace_back(CHUNK_SIZE - 1, i);
					neighbourWindowTiles.emplace_back(0, i);
				}

				createWindow(chunkX, chunkY, 1, 0, windowTiles, neighbourWindowTiles);
				windowStart = -1;
			}
		}

		// Close any open windows.
		if ( windowStart != -1 ) {
			// Generate local coords
			std::vector<IntPair> windowTiles, neighbourWindowTiles;
			for ( int i = windowStart; i < CHUNK_SIZE; i++ ) {
				windowTiles.emplace_back(CHUNK_SIZE - 1, i);
				neighbourWindowTiles.emplace_back(0, i);
			}

			createWindow(chunkX, chunkY, 1, 0, windowTiles, neighbourWindowTiles);
			windowStart = -1;
		}
	}

	// South.
	if ( chunkY != NUMBER_OF_CHUNKS - 1 ) {
		int turfY = (chunkY * CHUNK_SIZE) + CHUNK_SIZE - 1;
		for ( int localOffset = 0; localOffset < CHUNK_SIZE; localOffset++ ) { // For every tile along.
			int turfX = (chunkX * CHUNK_SIZE) + localOffset;

			// If tile is open on both sides.
			if ( World::activeWorld->turfs[turfX][turfY]->isOpen && World::activeWorld->turfs[turfX][turfY + 1]->isOpen ) {

				// Start new window
				if ( windowStart == -1 ) {
					windowStart = localOffset;
				}

				// previous tile was window closing.
			} else if ( windowStart != -1 ) {
				std::vector<IntPair> windowTiles, neighbourWindowTiles;
				for ( int i = windowStart; i < localOffset; i++ ) {
					windowTiles.emplace_back(i, CHUNK_SIZE - 1);
					neighbourWindowTiles.emplace_back(i, 0);
				}

				createWindow(chunkX, chunkY, 0, 1, windowTiles, neighbourWindowTiles);
				windowStart = -1;
			}
		}

		// Close any open windows.
		if ( windowStart != -1 ) {
			// Generate local coords
			std::vector<IntPair> windowTiles, neighbourWindowTiles;
			for ( int i = windowStart; i < CHUNK_SIZE; i++ ) {
				windowTiles.emplace_back(i, CHUNK_SIZE - 1);
				neighbourWindowTiles.emplace_back(i, 0);
			}

			createWindow(chunkX, chunkY, 0, 1, windowTiles, neighbourWindowTiles);
			windowStart = -1;
		}
	}

	if ( !allDirections )
		return;

	// West.
	if ( chunkX ) {
		int turfX = chunkX * CHUNK_SIZE;
		for ( int localOffset = 0; localOffset < CHUNK_SIZE; localOffset++ ) { // For every tile along.
			int turfY = (chunkY * CHUNK_SIZE) + localOffset;

			// If tile is open on both sides.
			if ( World::activeWorld->turfs[turfX][turfY]->isOpen && World::activeWorld->turfs[turfX - 1][turfY]->isOpen ) {

				// Start new window
				if ( windowStart == -1 ) {
					windowStart = localOffset;
				}

				// previous tile was window closing.
			} else if ( windowStart != -1 ) {
				std::vector<IntPair> windowTiles, neighbourWindowTiles;
				for ( int i = windowStart; i < localOffset; i++ ) {
					windowTiles.emplace_back(0, i);
					neighbourWindowTiles.emplace_back(CHUNK_SIZE - 1, i);
				}

				createWindow(chunkX, chunkY, -1, 0, windowTiles, neighbourWindowTiles);
				windowStart = -1;
			}
		}

		// Close any open windows.
		if ( windowStart != -1 ) {
			// Generate local coords
			std::vector<IntPair> windowTiles, neighbourWindowTiles;
			for ( int i = windowStart; i < CHUNK_SIZE; i++ ) {
				windowTiles.emplace_back(0, i);
				neighbourWindowTiles.emplace_back(CHUNK_SIZE - 1, i);
			}

			createWindow(chunkX, chunkY, -1, 0, windowTiles, neighbourWindowTiles);
			windowStart = -1;
		}
	}

	// North.
	if ( chunkY ) {
		int turfY = chunkY * CHUNK_SIZE;
		for ( int localOffset = 0; localOffset < CHUNK_SIZE; localOffset++ ) { // For every tile along.
			int turfX = (chunkX * CHUNK_SIZE) + localOffset;

			// If tile is open on both sides.
			if ( World::activeWorld->turfs[turfX][turfY]->isOpen && World::activeWorld->turfs[turfX][turfY - 1]->isOpen ) {

				// Start new window
				if ( windowStart == -1 ) {
					windowStart = localOffset;
				}

				// previous tile was window closing.
			} else if ( windowStart != -1 ) {
				std::vector<IntPair> windowTiles, neighbourWindowTiles;
				for ( int i = windowStart; i < localOffset; i++ ) {
					windowTiles.emplace_back(i, 0);
					neighbourWindowTiles.emplace_back(i, CHUNK_SIZE - 1);
				}

				createWindow(chunkX, chunkY, 0, -1, windowTiles, neighbourWindowTiles);
				windowStart = -1;
			}
		}

		// Close any open windows.
		if ( windowStart != -1 ) {
			// Generate local coords
			std::vector<IntPair> windowTiles, neighbourWindowTiles;
			for ( int i = windowStart; i < CHUNK_SIZE; i++ ) {
				windowTiles.emplace_back(i, 0);
				neighbourWindowTiles.emplace_back(i, CHUNK_SIZE - 1);
			}

			createWindow(chunkX, chunkY, 0, -1, windowTiles, neighbourWindowTiles);
			windowStart = -1;
		}
	}
}

// Calculates and saves which windows can 'path' to eachother.
// Local paths ONLY. Chunk to chunk connections are handled at the window stage.
void SSPathing::generateEdgesForChunk(int chunkX, int chunkY) {
	Chunk* chunk = World::activeWorld->chunks[chunkX][chunkY];
	lastNetworkID = 0;

	//PRINT("Chunk: " << chunk->position.x << " " << chunk->position.y);

	// Populate the local 'cache' map with window IDs.
	for ( std::pair<int, Flow_Window*> window : chunk->windows )
		for ( IntPair tile : window.second->tiles )
			windowCache[tile.x][tile.y].insert(window.second->id);

	Flow_Network* network = NULL;

	// Flood fill each window to find which can path together.
	// Networks are also established here.
	// Redundant (linked) windows are skipped as they have already been touched.
	for ( std::pair<int, Flow_Window*> window : chunk->windows ) {
		// This window was linked in the last run.
		if ( window.second->linked )
			continue;

		network = new Flow_Network;
		chunk->networks.emplace(++lastNetworkID, network);

		// Initiate the flood fill.
		IntPair start = window.second->tiles[0];
		flood(start.x, start.y, chunkX, chunkY);

		// Link touched windows together and add network.
		for ( int touchedWindowID : currentWindows ) {
			Flow_Window* touchedWindow = chunk->windows.at(touchedWindowID);
			touchedWindow->linked = true;
			touchedWindow->network = network;

			network->windows.push_back(touchedWindow);

			for ( int otherWindowID : currentWindows ) {
				// Dont connect window to itself.
				if ( otherWindowID == touchedWindowID )
					continue;

				Flow_Window* otherWindow = chunk->windows.at(otherWindowID);
				otherWindow->connections.push_back(touchedWindow);
			}
		}

		// Reset list of touched windows.
		currentWindows.clear();
	}

	// Reset caches.
	resetCache();
}

// Recursive flood-fill method of finding what windows touch.
// Result is currentWindows is populated with window IDs.
void SSPathing::flood(int x, int y, int chunkX, int chunkY) {
	// If we already flooded this tile.
	if ( floodCache[x][y] )
		return;
	floodCache[x][y] = true;

	IntPair worldPos = {
		(chunkX * CHUNK_SIZE) + x,
		(chunkY * CHUNK_SIZE) + y
	};

	Turf* turf = World::activeWorld->turfs[worldPos.x][worldPos.y];

	// Closed tile, skip it.
	if ( !turf->isOpen )
		return;

	turf->pathNetworkID = lastNetworkID;

	// Add tile windows to list of touched windows.
	currentWindows.insert(windowCache[x][y].begin(), windowCache[x][y].end());

	// N
	if ( y )
		flood(x, y - 1, chunkX, chunkY);

	// E
	if ( x != CHUNK_SIZE - 1 )
		flood(x + 1, y, chunkX, chunkY);

	// S
	if ( y != CHUNK_SIZE - 1 )
		flood(x, y + 1, chunkX, chunkY);

	// W
	if ( x )
		flood(x - 1, y, chunkX, chunkY);
}

// RESET FLOOD CACHE TO 0s. RESET WINDOW CACHE TO EMPTY VECs.
// Make this a common func? Doesn't work on anything but 0?
// TODO: Can this be made into local vars instead?
// https://stackoverflow.com/questions/2516096/fastest-way-to-zero-out-a-2d-array-in-c
void SSPathing::resetCache() {
	memset(floodCache, false, sizeof(floodCache[0][0]) * CHUNK_SIZE * CHUNK_SIZE);

	for ( int y = 0; y < CHUNK_SIZE; y++ )
		for ( int x = 0; x < CHUNK_SIZE; x++ )
			windowCache[x][y].clear();
}

// Recalculates a single chunk's window data.
// (This involves also recalculating direct neighbour chunks).
// -----OUTDATED------
void SSPathing::resetChunk(int chunkX, int chunkY) {
	//int numChunks = 10; // HARDCODED.

	std::unordered_map<int, Flow_Window*>* invalidChunkWindows = &World::activeWorld->chunks[chunkX][chunkY]->windows;

	// For every window in the invalid chunk...
	for ( std::pair<int, Flow_Window*> windowPair : *invalidChunkWindows ) {
		Flow_Window* window = windowPair.second;
		Flow_Window* sisterWindow = window->connections.front();

		// Remove neighbour chunk's reference to sister window.
		sisterWindow->chunk->windows.erase(sisterWindow->id);

		// Delete invalid windows.
		delete(window);
		delete(sisterWindow);
	}

	invalidChunkWindows->clear();

	// North
	if ( chunkY )
		resetChunkInternalConnections(World::activeWorld->chunks[chunkX][chunkY - 1]);
	// East
	if ( chunkX != NUMBER_OF_CHUNKS - 1 )
		resetChunkInternalConnections(World::activeWorld->chunks[chunkX + 1][chunkY]);
	// South
	if ( chunkY != NUMBER_OF_CHUNKS - 1 )
		resetChunkInternalConnections(World::activeWorld->chunks[chunkX][chunkY + 1]);
	// West
	if ( chunkX )
		resetChunkInternalConnections(World::activeWorld->chunks[chunkX - 1][chunkY]);

	// Regenerate windows.
	generateWindowsForChunk(chunkX, chunkY, true);

	// Regenerate edges.
	generateEdgesForChunk(chunkX, chunkY);

	// North
	if ( chunkY )
		generateEdgesForChunk(chunkX, chunkY - 1);
	// East
	if ( chunkX != NUMBER_OF_CHUNKS - 1 )
		generateEdgesForChunk(chunkX + 1, chunkY);
	// South
	if ( chunkY != NUMBER_OF_CHUNKS - 1 )
		generateEdgesForChunk(chunkX, chunkY + 1);
	// West
	if ( chunkX )
		generateEdgesForChunk(chunkX - 1, chunkY);
}

// Clear all chunk window connects except for those pointing outside.
void SSPathing::resetChunkInternalConnections(Chunk* chunk) {
	for ( std::pair<int, Flow_Window*> window : chunk->windows )
		window.second->connections.resize(1);
}

//void SSPathing::reset() {
////	int numChunks = 10; // HARDCODED.
//
//	for ( int chunkY = 0; chunkY < NUMBER_OF_CHUNKS; chunkY++ ) {
//		for ( int chunkX = 0; chunkX < NUMBER_OF_CHUNKS; chunkX++ ) {
//			Chunk* chunk = World::activeWorld->chunks[chunkX][chunkY];
//
//			for ( std::pair<int, Flow_Window*> window : chunk->windows )
//				delete(window.second);
//
//			chunk->windows.clear();
//
//		}
//	}
//}

//void SSPathing::generateCostField() {
//	for ( int y = 0; y < World::activeWorld->height; y++ ) {
//		for ( int x = 0; x < World::activeWorld->width; x++ ) {
//			if ( !World::activeWorld->turfs[x][y]->isOpen ) {
//				costField[x][y] = 255;
//			}
//		}
//	}
//}

//void SSPathing::generateIntegrationField() {
//	for ( int y = 0; y < World::activeWorld->height; y++ ) {
//		for ( int x = 0; x < World::activeWorld->width; x++ ) {
//			integrationField[x][y] = 0;// 65535;
//		}
//	}
//}