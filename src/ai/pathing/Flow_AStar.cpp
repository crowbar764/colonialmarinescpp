#include "Flow_AStar.h"
#include "Flow_Pathing.h"
#include "../../atom/turf/Turf.h"
#include "../../atom/entity/mob/Mob.h"
#include "../../global/World.h"

// TESTING: 
// Disable path merging for more organic paths.
// #define PATH_MERGING

// Done once and at the start of a path request. Sets up goal data.
void Flow_AStar::prepareRequest(IntPair goalGrid, World* world) {
	goalChunk = { goalGrid.x / CHUNK_SIZE, goalGrid.y / CHUNK_SIZE };
	short networkID = world->turfs[goalGrid.x][goalGrid.y]->pathNetworkID;
	endNetwork = world->chunks[goalGrid.x / CHUNK_SIZE][goalGrid.y / CHUNK_SIZE]->networks.at(networkID);

	#ifdef PATH_MERGING
	lastFlowPathRunID++;
	#endif
}

// Done for every mob in the request. Sets up start data and initiates the AStar.
bool Flow_AStar::prepareMob(Mob* mob) {
	IntPair startingGrid = { (int) mob->position.x / UNITS_PER_TILE, (int) mob->position.y / UNITS_PER_TILE };
	IntPair startingChunkPos = startingGrid / CHUNK_SIZE;
	Chunk* startingChunk = mob->world->chunks[startingChunkPos.x][startingChunkPos.y];

	// First, check if this mob is already in a path.
	if ( Flow_Pathing::pathChunks[startingChunk->position.x][startingChunk->position.y] )
		return true;

	startChunk = startingChunk->position;
	startNetwork = startingChunk->networks.at(mob->world->turfs[startingGrid.x][startingGrid.y]->pathNetworkID);

	// If there are no windows, and we are not at the destination, the path is impossible.
	if ( !startNetwork->windows.size() && (startNetwork != endNetwork) )
		return false;

	// Add starting network's windows to open list. (Add the sister window so when the loop starts, it moves into the real starting network).
	open.add(startNetwork->windows[0]->connections[0]);

	lastMobPathRunID++;
	return FindMobPath();
}

// AStar but NOT in a grid.
// We traverse through Flow_Windows based on what other windows connect to the current window.
// Backtracking (parent) is the Flow_Network, not a window, as we care about the fastest way to each network, not to each window.
// Secondary runs (2nd mob and beyond) will stop their run if they traverse into a network that is part of the established path and just add the networks to that join point.
// The result is that the 2D pathChunks array is set to true if that chunk is part of the path.
bool Flow_AStar::FindMobPath() {
	while ( open.currentNodeCount ) {
		// Remove current window from open list.
		currentWindow = open.removeFirst()->connections[0];
		//PRINT(currentWindow->connections[0]->chunk->position.x << " " << currentWindow->connections[0]->chunk->position.y << " >> " << currentWindow->chunk->position.x << " " << currentWindow->chunk->position.y);

		if ( currentWindow->network->mobPathRunID == lastMobPathRunID )
			continue;

		IntPair windowChunk = currentWindow->chunk->position;

		// We have reached the goal network, or joined with another mob's path.
		#ifdef PATH_MERGING
		if ( currentWindow->network->flowPathRunID == lastFlowPathRunID || currentWindow->network == endNetwork ) {
		#else
		if ( currentWindow->network == endNetwork ) {
		#endif
			currentWindow->network->parent = currentWindow->connections[0]->network;
			retracePath();
			open.reset();
			return true;
		}

		// Add every window in the newly breached network to the open list (except the first, since that is from the previous chunk).
		for ( int i = 1; i < currentWindow->connections.size(); i++ ) {
			Flow_Window* window = currentWindow->connections[i];

			// This window leads to a processed network, skip it.
			if ( window->connections[0]->network->mobPathRunID == lastMobPathRunID )
				continue;

			// Add distance to start.
			window->g = getDistance(
				windowChunk.x,
				windowChunk.y,
				startChunk.x,
				startChunk.y); 

			// Add distance to goal.
			window->h = getDistance(
				windowChunk.x,
				windowChunk.y,
				goalChunk.x,
				goalChunk.y);

			// TODO: Add a check to see if we have found a faster way to the network.
			window->f = window->g + window->h;

			open.add(window);
		}

		// Set network as processed.
		currentWindow->network->mobPathRunID = lastMobPathRunID;
			
		// Set parent.
		currentWindow->network->parent = currentWindow->connections[0]->network;
	}

	PRINT("Path not found.");
	return false;
}


// Backtrack via the 'parent' trail to find our way back to the start.
// Result is pathChunks is updated. Network 'flowPathRunID' is also set so other mobs can join the path.
void Flow_AStar::retracePath() {
	Flow_Network* currentNetwork = currentWindow->network;

	//PRINT("Final path:");

	while ( true ) {
		Chunk* chunk = currentNetwork->windows[0]->chunk;

		#ifdef PATH_MERGING
		currentNetwork->flowPathRunID = lastFlowPathRunID;
		#endif

		Flow_Pathing::pathChunks[chunk->position.x][chunk->position.y] = true;
		//PRINT("Adding " << chunk->position.x << " " << chunk->position.y << " to the path.");

		if ( currentNetwork == startNetwork )
			break;

		
		currentNetwork = currentNetwork->parent;
	}
}

int Flow_AStar::getDistance(int x, int y, int x2, int y2) {
	int dstX = abs(x - x2);
	int dstY = abs(y - y2);

	if ( dstX > dstY )
		return 14 * dstY + 10 * (dstX - dstY);

	return 14 * dstX + 10 * (dstY - dstX);
}