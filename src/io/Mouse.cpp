#include "Mouse.h"
#include "../global/Camera.h"
#include "../rendering/Renderer.h"
#include "../global/World.h"
#include "SSSelect.h"
#include "../other/Gamemode_Editor.h"
#include "ClickMask.h"
#include "../ai/AStar.h"
#include "../gui/SSUI.h"
#include "../atom/turf/Hull.h"
#include "../rendering/TurfRenderer.h"
#include "../Test.h"
#include "../atom/entity/mob/Mob.h"
#include "../atom/entity/mob/flying/Flying.h"
#include "../subsystems/Master.h"
#include "../atom/effect/MarkerBig.h"
#include "../ai/pathing/Flow_Pathing.h"
#include "../common/IntPair.h"
#include "../common/Conversions.h"
#include "../gui/GUIElement.h"
//#include "../common/Math.h"

bool Mouse::isDragging;
glm::vec2 Mouse::startDrag, Mouse::endDrag;

void Mouse::onLeftClick(IntPair screenPos, glm::vec2 worldPos, IntPair gridPos, int action, int mods) {
	unsigned int entityID = 0;

	//BresenhamLine(10, 10, gridPos.x, gridPos.y);


	// If no GUI hit, check clickmask.
	if (!SSUI::hoveredElement)
		entityID = ClickMask::getEntity(screenPos.x, screenPos.y);

	if (action == GLFW_PRESS) {
		// Clicked GUI.
		if (SSUI::hoveredElement) {
			SSUI::hoveredElement->onMouseDown();
			return;
		}

		// Click canvas somewhere.
		M1Pressed = true;
		startDrag = worldPos;

	} else if (action == GLFW_RELEASE) {
		M1Pressed = false;
		bool wasDragging = isDragging;
		isDragging = false;

		// Clicked GUI.
		if (SSUI::onMouseUp())
			return;

		//Clicked canvas.
		if (!wasDragging ) {

			// Ghost mode.
			if ( BIT_CHECK(Gamemode_Editor::constructionFlags, CONSTR_GHOST_ENABLED) ) {
				Gamemode_Editor::attemptBuild();
				return;
			}

			// Clicked entity.
			if ( entityID ) {
				Entity* entity = World::getEntity(entityID).get();

				if ( entity->isSelectable ) {
					SSSelect::deselect(); // Deselect any previously selected entities.
					SSSelect::addEntities(entity); // Select clicked one.
				}
			} else { // Clicked turf.
				SSSelect::deselect(); // Clear selection.
			}
		}

		//isDragging = false;
	}
}

void Mouse::onRightClick(IntPair screenPos, glm::vec2 worldPos, IntPair gridPos, int action, int mods) {
	// If no GUI hit, check clickmask.
	if (SSUI::hoveredElement)
		return;

	if (action == GLFW_PRESS) {

		//M2Pressed = true;

	} else if (action == GLFW_RELEASE) {
		if (BIT_CHECK(Gamemode_Editor::mode, MODE_EDIT)) {
			delete(World::activeWorld->turfs[gridPos.x][gridPos.y]);
			World::activeWorld->turfs[gridPos.x][gridPos.y] = World::activeWorld->newTurf(
				Gamemode_Editor::currentTurf,
				(float)gridPos.x * UNITS_PER_TILE,
				(float)gridPos.y * UNITS_PER_TILE
			);
		} else {
			std::vector<Mob*> mobs;

			for (Atom* selectedEntity : SSSelect::selectedEntities) {
				Mob* mob = dynamic_cast<Mob*>(selectedEntity);

				if (mob != nullptr)
					mobs.push_back(mob);
			}

			if ( mobs.size() ) {
				Mob::moveTo(mobs, gridPos.x, gridPos.y);
				new MarkerBig(World::activeWorld, { gridPos.x * UNITS_PER_TILE, gridPos.y * UNITS_PER_TILE });
			}
		}

		//M2Pressed = false;
	}
}

void Mouse::onMiddleClick(IntPair screenPos, glm::vec2 worldPos, IntPair gridPos, int action, int mods) {
	if ( action == GLFW_RELEASE ) {
		World::activeWorld->turfs[gridPos.x][gridPos.y]->isOpen = false;
		new MarkerBig(World::activeWorld, {gridPos.x * UNITS_PER_TILE, gridPos.y * UNITS_PER_TILE });
	}
}

// When any mouse button is pressed or released.
// window:	The window that received the event.
// button:	The mouse button that was pressed or released.
// action:	One of GLFW_PRESS or GLFW_RELEASE. Future releases may add more actions.
// mods:	Bit field describing which modifier keys were held down.
void Mouse::onMouseButton(GLFWwindow* window, int button, int action, int mods)
{
	IntPair screenPos = mousePos;
	glm::vec2 worldPos = getMousePosWorld();
	IntPair gridPos = getMousePosGrid();
	
	switch (button) {
		case GLFW_MOUSE_BUTTON_LEFT:
			onLeftClick(screenPos, worldPos, gridPos, action, mods);
			break;
		case GLFW_MOUSE_BUTTON_RIGHT:
			onRightClick(screenPos, worldPos, gridPos, action, mods);
			break;
		case GLFW_MOUSE_BUTTON_MIDDLE:
			onMiddleClick(screenPos, worldPos, gridPos, action, mods);
			break;
	}
}

void Mouse::onMouseMove(GLFWwindow* window, double xpos, double ypos)
{
	if (M1Pressed && (Master::gameState == GAMESTATE_IN_WORLD)) {
		SSSelect::onDrag();
	}
}

void Mouse::handleScroll(GLFWwindow* window, double xoffset, double yoffset) {
	if (yoffset == 1) { //Scroll forward
		Camera::zoom(1);
	}
	else {				//Scroll back
		Camera::zoom(-1);
	}
}

void Mouse::calculateMousePos() {
	double x, y;
	glfwGetCursorPos(Main::window, &x, &y);

	mousePos = { (int)x, (int)y };
}

glm::vec2 Mouse::getMousePosWorld() {
	return screenToWorld(mousePos);
}

IntPair Mouse::getMousePosGrid() {
	return screenToGrid(mousePos);
}

void Mouse::init() {
	isDragging = false;

	glfwSetMouseButtonCallback(Main::window, onMouseButton);
	glfwSetCursorPosCallback(Main::window, onMouseMove);
	glfwSetScrollCallback(Main::window, handleScroll);
}