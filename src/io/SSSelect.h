#pragma once

#include "../common/Common.h"

class Entity;

class SSSelect
{
public:
	inline static glm::vec4 selectedArea;
	inline static std::vector<Entity*> selectedEntities;

	static void addEntities(Entity*);
	static void onDrag();
	static void selectArea();
	static void deselect();
	static void deselectSingle(Entity* deselectedEntity);
	static void drawSelectBox();
};