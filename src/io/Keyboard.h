#pragma once

struct GLFWwindow;

class Keyboard {
public:
	static int keys;

	static void handleEvent(GLFWwindow* window, int key, int scancode, int action, int mods);
	static void process();
	static void init();
};