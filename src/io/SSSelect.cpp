#include "SSSelect.h"
#include "Mouse.h"
#include "../global/World.h"
//#include "ClickMask.h"
#include "../rendering/Renderer.h"
#include "../global/Camera.h"
#include "../common/IntPair.h"
#include "../common/VectorHelpers.h"
#include "../common/Conversions.h"
#include "../atom/turf/Turf.h"
#include "../atom/entity/Entity.h"

constexpr auto BOX_SELECT_THRESHOLD = .2f; // The number of world units that must be dragged before a click is considered a drag.
constexpr glm::vec3 BOX_SELECT_COLOUR = glm::vec3(0, .4, .8);
constexpr auto BOX_SELECT_BORDER_WIDTH = 1;

void SSSelect::addEntities(Entity* entity)
{
	entity->isSelected = true;
	selectedEntities.push_back(entity);
}

void SSSelect::onDrag() {
	deselect();

	Mouse::endDrag = Mouse::getMousePosWorld();

	glm::vec2 useStart = { BOX_SELECT_THRESHOLD - 1, BOX_SELECT_THRESHOLD - 1 };
	glm::vec2 useEnd = { BOX_SELECT_THRESHOLD - 1, BOX_SELECT_THRESHOLD - 1 };

	if (Mouse::endDrag.x < Mouse::startDrag.x) {
		useStart.x = Mouse::endDrag.x;
		useEnd.x = Mouse::startDrag.x;
	}
	else {
		useStart.x = Mouse::startDrag.x;
		useEnd.x = Mouse::endDrag.x;
	}

	if (Mouse::endDrag.y < Mouse::startDrag.y) {
		useStart.y = Mouse::endDrag.y;
		useEnd.y = Mouse::startDrag.y;
	}
	else {
		useStart.y = Mouse::startDrag.y;
		useEnd.y = Mouse::endDrag.y;
	}

	glm::vec4 newSelection = glm::vec4{
		(float)useStart.x,
		(float)useStart.y,
		(float)useEnd.x,
		(float)useEnd.y
	};

	// If selection size too small, have no select area.
	if ( newSelection.z - newSelection.x < BOX_SELECT_THRESHOLD && newSelection.w - newSelection.y < BOX_SELECT_THRESHOLD) {
		selectedArea = glm::vec4{};
		Mouse::isDragging = false;
	} else {
		selectedArea = newSelection;
		Mouse::isDragging = true;
		selectArea();
	}
}

void SSSelect::selectArea() {
	const glm::vec4 selectedGrid = {
		floor(selectedArea.x / UNITS_PER_TILE),
		floor(selectedArea.y / UNITS_PER_TILE),
		ceil(selectedArea.z / UNITS_PER_TILE),
		ceil(selectedArea.w / UNITS_PER_TILE),
	};

	for (int y = (int)selectedGrid.y; y < selectedGrid.w; y++) {
		for ( int x = (int) selectedGrid.x; x < selectedGrid.z; x++ ) {

			if (!World::activeWorld->gridIsInBounds(x, y))
				continue;

			Turf* turf = World::activeWorld->turfs[x][y];

			//turf->debugMarked = true;

			

			for ( Entity* occupant : turf->occupants ) {
				if ( occupant->isSelectable && !occupant->isSelected ) { // Second check can be removed if locs is changed to loc.

					// Get world positions of top left and bottom right corners of the entity.
					const float halfRes = occupant->spriteResolution * 0.5f;
					const glm::vec4 entityCorners = {
						occupant->position.x - halfRes,
						occupant->position.y - halfRes,
						occupant->position.x + halfRes,
						occupant->position.y + halfRes
					};

					//PRINT("entity " << entityCorners.x << " " << entityCorners.w);


					//if (
						//entityCorners.w >= selectedArea.x &&
						//selectedArea.w >= entityCorners.x
						//entityCorners.z >= selectedArea.y &&
						//selectedArea.z >= entityCorners.y
						//) {
						addEntities(occupant);
					//}

				}
			}
		}
	}
}

void SSSelect::deselect()
{
	for ( Entity* entity : selectedEntities)
		entity->isSelected = false;

	selectedEntities.clear();
}

void SSSelect::deselectSingle(Entity* deselectedEntity)
{
	std::vector<Entity*>::iterator it = find(selectedEntities.begin(), selectedEntities.end(), deselectedEntity);

	if (it != selectedEntities.end())
		eraseFast(selectedEntities, it);
}

void SSSelect::drawSelectBox() {
	if (!Mouse::isDragging) {
		return;
	}

	Renderer::shapeShader.use();
	glBindVertexArray(Renderer::shapeVAO);
	glBindBuffer(GL_ARRAY_BUFFER, Renderer::shapeVBO);

	glm::vec4 colour = glm::vec4(BOX_SELECT_COLOUR, 1);

	// X, Y, WIDTH, HEIGHT.
	const glm::vec4 worldCoords = {
		(selectedArea.x * Renderer::tileRes) - Camera::X,
		(selectedArea.y * Renderer::tileRes) - Camera::Y,
		(selectedArea.z - selectedArea.x) * Renderer::tileRes,
		(selectedArea.w - selectedArea.y) * Renderer::tileRes
	};

	// Box fill
	Renderer::drawShape(
		worldCoords.x,
		worldCoords.y,
		worldCoords.z,
		worldCoords.w,
		glm::vec4(BOX_SELECT_COLOUR, 0.2)
	);
#
	// Borders:
	// Top
	Renderer::drawShape(
		worldCoords.x,
		worldCoords.y,
		worldCoords.z,
		BOX_SELECT_BORDER_WIDTH,
		colour
	);

	// Bottom
	Renderer::drawShape(
		worldCoords.x,
		worldCoords.y + worldCoords.w - BOX_SELECT_BORDER_WIDTH,
		worldCoords.z,
		BOX_SELECT_BORDER_WIDTH,
		colour
	);

	// Left
	Renderer::drawShape(
		worldCoords.x,
		worldCoords.y,
		BOX_SELECT_BORDER_WIDTH,
		worldCoords.w,
		colour
	);

	// Right
	Renderer::drawShape(
		worldCoords.x + worldCoords.z - BOX_SELECT_BORDER_WIDTH,
		worldCoords.y,
		BOX_SELECT_BORDER_WIDTH,
		worldCoords.w,
		colour
	);

	Renderer::drawToBuffer(NULL, VERTEX_PAYLOAD_SHAPE);

	Renderer::ourShader.use();
	glBindVertexArray(Renderer::VAO);
	glBindBuffer(GL_ARRAY_BUFFER, Renderer::VBO);
}

//void SSSelect::init() {
//	selectedEntities.pop_back(); // Delete first empty element
//}