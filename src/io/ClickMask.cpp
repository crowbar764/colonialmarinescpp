#include "ClickMask.h"
#include "../rendering/FrameBuffer.h"
#include "../rendering/Renderer.h"
#include "../global/World.h"
#include "../global/Camera.h"
#include "../gui/SSUI.h"
#include "../Test.h"
#include "../common/Math.h"

unsigned int ClickMask::getEntity(int x, int y) {
	// Need both of these?
	maskFBO->setAsTarget();
	maskFBO->setAsSource();

	// Only click-check pixels inside mask.
	x = clamp(x, 0, Main::width - 1);
	y = clamp(y, 1, Main::height);

	unsigned char pixel[4];
	glReadPixels(x, Main::height - y, 1, 1, GL_RGBA, GL_UNSIGNED_BYTE, pixel); // scans from bottom left.

	unsigned int entityColorID = RBGAToInt(glm::vec4(
		static_cast<int>(pixel[0]),
		static_cast<int>(pixel[1]),
		static_cast<int>(pixel[2]),
		static_cast<int>(pixel[3])
	));

	//std::cout << "Clicked entityID: " << entityColorID << "\n";
	//std::cout << x << " " << y << " " << entityColorID << "\n";
	return entityColorID;
}

// NOTE: We DON'T need to create mask for turfs. Turfs can be selected the cheat way via math.
void ClickMask::createMask() {
	if (!active)
		return;

	maskFBO->setAsTarget();

	glDisable(GL_BLEND); // Do not blend and lose RGBA data
	Renderer::shapeShader.use();
	glBindVertexArray(Renderer::shapeVAO);
	glBindBuffer(GL_ARRAY_BUFFER, Renderer::shapeVBO);

	glClearColor(0.0f, 0.0f, 0.0f, 0.0f);
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
	Renderer::activeDrawBuffer = Renderer::drawBufferClickmask;
	Renderer::drawBufferCounterActive = &Renderer::drawBufferCounterClickmask;

	// This also draws the vertex data stored from the Render loop earlier.
	Renderer::drawToBuffer(NULL, VERTEX_PAYLOAD_SHAPE);

	// Set back to regular.
	FrameBuffer::switchToMain();
	glEnable(GL_BLEND);
	Renderer::ourShader.use();
	glBindVertexArray(Renderer::VAO);
	glBindBuffer(GL_ARRAY_BUFFER, Renderer::VBO);
	Renderer::activeDrawBuffer = Renderer::drawBufferMain;
	Renderer::drawBufferCounterActive = &Renderer::drawBufferCounterMain;
}

// OUT: e.g. (.5, .2, .1, .6)
glm::vec4 ClickMask::intToRBGA(unsigned int RGBAInt) {
	return glm::vec4(
		normalise((float) ((RGBAInt >> 24) & 255), 255),
		normalise((float) ((RGBAInt >> 16) & 255), 255),
		normalise((float) ((RGBAInt >> 8) & 255), 255),
		normalise((float) (RGBAInt & 255), 255)
	);
}

// IN: e.g: (100, 255, 123, 20)
unsigned int ClickMask::RBGAToInt(glm::vec4 rgba) {
	return
		(static_cast<int>(rgba.r) << 24) +
		(static_cast<int>(rgba.g) << 16) +
		(static_cast<int>(rgba.b) << 8) +
		static_cast<int>(rgba.a);
}

void ClickMask::init() {
	maskFBO = new FrameBuffer();
	FrameBuffer::createShader(&Renderer::shapeShader, "shaderShape");
}