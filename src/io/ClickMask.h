#pragma once

#include "../common/Common.h"

class FrameBuffer;

class ClickMask {
public:
    inline static bool active;
    inline static FrameBuffer* maskFBO;

    static unsigned int getEntity(int, int);
    static glm::vec4 intToRBGA(unsigned int);
    static unsigned int RBGAToInt(glm::vec4);
    static void createMask();
    static void init();
};