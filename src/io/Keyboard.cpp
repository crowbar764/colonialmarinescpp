#include "Keyboard.h"
#include "../Main.h"
#include "../global/Camera.h"
#include "../common/Common.h"
#include "../global/World.h"

int Keyboard::keys;

void Keyboard::handleEvent(GLFWwindow* window, int key, int scancode, int action, int mods) {
	// Only trigger on initial press.
	if (action != GLFW_PRESS)
		return;
		
	switch (key) {
	case GLFW_KEY_E:
		std::cout << "E\n";
		break;
	case GLFW_KEY_SPACE:
		if ( World::activeWorld->ID == WORLD_SHIP )
			World::switchWorld(WORLD_PLANET);
		else
			World::switchWorld(WORLD_SHIP);
		break;
	//case GLFW_KEY_W:
	//	std::cout << "W\n";
	//	break;
	//	Main::running = false;
	//	break;
	//case SDLK_p:
	//	Gamemode_Editor::cycleEditMode();
	//	break;
	//case SDLK_o:
	//	Gamemode_Editor::toggleEditMode();
	//	break;
	//case SDLK_m:
	//	World::saveWorld();
	//	break;
	}
}


void Keyboard::process() {
	keys = 0;

	int stateW = glfwGetKey(Main::window, GLFW_KEY_W);
	if (stateW == GLFW_PRESS)
		BIT_1(keys, IO_KEY_W);

	int stateS = glfwGetKey(Main::window, GLFW_KEY_S);
	if (stateS == GLFW_PRESS)
		BIT_1(keys, IO_KEY_S);

	int stateA = glfwGetKey(Main::window, GLFW_KEY_A);
	if (stateA == GLFW_PRESS)
		BIT_1(keys, IO_KEY_A);

	int stateD = glfwGetKey(Main::window, GLFW_KEY_D);
	if (stateD == GLFW_PRESS)
		BIT_1(keys, IO_KEY_D);

	//int stateE = glfwGetKey(Main::window, GLFW_KEY_E);
	//if ( stateE == GLFW_PRESS )
		//Gamemo

	//int stateR = glfwGetKey(Main::window, GLFW_KEY_R);
	//if ( stateR == GLFW_PRESS )
	//	World::saveWorld(World::activePath, true);

	//int stateR = glfwGetKey(Main::window, GLFW_KEY_R);
	//if (stateR == GLFW_PRESS)
	//	Main::prepareResize(1920, 1080);

	//int stateL = glfwGetKey(Main::window, GLFW_KEY_L);
	//if (stateL == GLFW_PRESS)
	//	BIT_TOGGLE(Renderer::renderFlags, RENDER_CLICKMASK);

	//const Uint8* keystates = SDL_GetKeyboardState(NULL);

	//if (keystates[SDL_SCANCODE_W]) {
	//	Camera::move(0, -Camera::panSpeed);
	//}
	//if (keystates[SDL_SCANCODE_S]) {
	//	Camera::move(0, Camera::panSpeed);
	//}
	//if (keystates[SDL_SCANCODE_A]) {
	//	Camera::move(-Camera::panSpeed, 0);
	//}
	//if (keystates[SDL_SCANCODE_D]) {
	//	Camera::move(Camera::panSpeed, 0);
	//}
}

void Keyboard::init() {
	glfwSetKeyCallback(Main::window, handleEvent);
}