#pragma once

#include <glad/glad.h>
#include <GLFW/glfw3.h>

#include "../common/IntPair.h"

class Mouse
{
public:
	inline static IntPair mousePos;
	static glm::vec2 startDrag, endDrag;

	static void calculateMousePos();
	static glm::vec2 getMousePosWorld();
	static IntPair getMousePosGrid();
	//static IntPair screenToWorld(int, int);
	//static IntPair worldToGrid(int, int);
	//static IntPair screenToGrid(int, int);
	static void onLeftClick(IntPair, glm::vec2, IntPair, int, int);
	static void onRightClick(IntPair, glm::vec2, IntPair, int, int);
	static void onMiddleClick(IntPair, glm::vec2, IntPair, int, int);
	static void handleScroll(GLFWwindow* window, double xoffset, double yoffset);
	static void init();

	static bool isDragging;

private:
	inline static bool M1Pressed, M2Pressed;

	static void onMouseButton(GLFWwindow*, int, int, int);
	static void onMouseMove(GLFWwindow*, double, double);
};