#pragma once

#include "../GUIElement.h"

class Resizer : public GUIElement
{
public:

	Resizer(GUIElement* parent) : GUIElement(parent) {
		positionType = GUIPosition::ABSOLUTE;
		width = 64;
		height = 64;
		xAlignment = GUIAlignment::END;
		yAlignment = GUIAlignment::END;
		backgroundColour = glm::vec4(255, 0, 0, 1.0f);
	}

	void onMouseUp() {
		SSUI::resizer = NULL;
	}

	void onMouseDown(bool isPropagation) {
		GUIElement::onMouseDown(true);

		if (isPropagation)
			return;

		SSUI::resizer = this;
		
	}

	void process() {
		//IntPair mousePos = Mouse::getMousePos();

		//// Offset so cursor is in middle of resize button.
		//float offsetX = (calculatedW / SSUI::main->calculatedW) / 2.0f;
		//float offsetY = (calculatedH / SSUI::main->calculatedH) / 2.0f;

		//float newW = ((float)mousePos.x / SSUI::main->calculatedW) - parent->x + offsetX;
		//float newH = ((float)mousePos.y / SSUI::main->calculatedH) - parent->y + offsetY;

		//// Keep inside screen, and bigger than 20%.
		//newW = Common::clamp(newW, .2f, 1.0f - parent->x);
		//newH = Common::clamp(newH, .2f, 1.0f - parent->y);

		//parent->width = newW;
		//parent->height = newH;
	}
};