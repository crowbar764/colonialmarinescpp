#pragma once

#include "../GUIElement.h"
#include "../font/Text.h"

class Label : public GUIElement
{
	Text* text;
public:
	Label(GUIElement* parent) : GUIElement(parent) {

        width = 100;
        height = 24;

        text = new Text{
            "Hello World!",
            0,
            0,
            .25,
            glm::vec4(1.0f, 1.0f, 1.0f, 1.0f),
            true
        };

        text->regenerateMesh();
	}

    void draw() {
        Renderer::activeDrawBuffer = Renderer::drawBufferText;
        Renderer::drawBufferCounterActive = &Renderer::drawBufferCounterText;

        SSText::drawText(text, x, y);
    }
};