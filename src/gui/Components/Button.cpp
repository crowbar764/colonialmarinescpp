#include "Button.h"
#include "../../rendering/Renderer.h"
#include "../../global/World.h"
#include "../../other/Gamemode_Editor.h"
#include "../../global/Atlas.h"
#include "../../common/DefinesSprites.h"

#include <GLFW/glfw3.h>

Button::Button(GUIElement* parent, std::function<void()> callback) : GUIElement(parent) {
	this->callback = callback;
	hasHoverEffect = true;
	cursorShape = GLFW_HAND_CURSOR;
	background = Atlas::mainAtlas->getSprite9(SPRITE_GUI_SLICE_OUT);
}

void Button::onMouseUp() {
	GUIElement::onMouseUp();
	//state = !state;
	PRINT("button");

	if(callback)
		callback();

	//switch (callback) {
	//case EVENT_BUTTON_SAVE_WORLD:
	//	//World::saveWorld(World::activePath, true);
	//	World::saveWorld(World::activePath);
	//	break;
	//case EVENT_BUTTON_CYCLE_EDITMODE:
	//	Gamemode_Editor::currentTurf++;
	//	if (Gamemode_Editor::currentTurf == (int)TurfType::END)
	//		Gamemode_Editor::currentTurf = 0;
	//	break;
	//}
}

void Button::draw() {

	glm::vec4 colour = glm::vec4(1.0f, 0, 0, .2);

	//if (isHovering) {
	//	colour = glm::vec4(0, 0, 0, .4);
	//}

	Renderer::activeDrawBuffer = Renderer::drawBufferMain;
	Renderer::drawBufferCounterActive = &Renderer::drawBufferCounterMain;

	Renderer::nslice(background, x, y, width_, height_);
}