#pragma once

#include "../GUIElement.h"
#include "../../common/Math.h"
#include "../../common/DefinesSprites.h"
#include "../../global/Atlas.h"

class Draggable : public GUIElement {
public:
	bool active;
	GUIElement* dragTarget;
	IntPair lastMousePos;
	TexCoords* background;

	Draggable(GUIElement* parent, GUIElement* dragTarget) : GUIElement(parent) {
		width = 300;
		height = 32;
		this->dragTarget = dragTarget;
		cursorShape = GLFW_HAND_CURSOR;
		stretchableX = GUIStretch::FILL;
		background = Atlas::mainAtlas->getSprite(SPRITE_DEBUG)->frames[0];
	}

	void process() {
		IntPair mousePos = Mouse::mousePos;

		// Clamp mouse to inside viewport.
		mousePos.x = (int)clamp((float)mousePos.x, .0f, SSUI::main->width_);
		mousePos.y = (int)clamp((float)mousePos.y, .0f, SSUI::main->height_);

		// If mouse hasn't moved, do nothing.
		if (mousePos.x == lastMousePos.x && mousePos.y == lastMousePos.y)
			return;
		
		// How much has the mouse moved since the last frame?
		float offsetX = (float)mousePos.x - lastMousePos.x;
		float offsetY = (float)mousePos.y - lastMousePos.y;

		// Calculate new target position.
		float newX = dragTarget->x + offsetX;
		float newY = dragTarget->y + offsetY;

		// Keep inside viewport.
		newX = clamp(newX, .0f, SSUI::main->width_ - dragTarget->width_);
		newY = clamp(newY, .0f, SSUI::main->height_ - height_);

		dragTarget->x = newX;
		dragTarget->y = newY;

		BIT_1(Renderer::renderFlags, RENDER_RECALC_GUI);
		BIT_1(Renderer::renderFlags, RENDER_REDRAW_GUI);

		lastMousePos = mousePos;
	}

	void onHover() {
		GUIElement::onHover();
		
		//GLFWcursor* cursor = glfwCreateStandardCursor(GLFW_CROSSHAIR_CURSOR);
		//cursor->
		//glfwSetCursor(Main::window, cursor);
		//glfwSetInputMode(Main::window, GLFW_CURSOR, GLFW_CURSOR_NORMAL);
	}

	void onUnhover() {
		GUIElement::onUnhover();
		//Main::setCursor();
	}

	void onMouseDown(bool isPropagation) {
		GUIElement::onMouseDown(true);

		if (isPropagation)
			return;

		SSUI::draggable = this;
		active = true;
		lastMousePos = Mouse::mousePos;
		dragTarget->xAlignment = GUIAlignment::NONE;
		dragTarget->yAlignment = GUIAlignment::NONE;
	}

	void onMouseUp() {
		if (!active)
			return;

		SSUI::draggable = NULL;
		active = false;
	}

	void draw() {

		Renderer::activeDrawBuffer = Renderer::drawBufferMain;
		Renderer::drawBufferCounterActive = &Renderer::drawBufferCounterMain;


		//Renderer::drawShape(x, y, width_, height_, glm::vec4(1.0, 0.0, 0.0, 1.0));
		Renderer::drawSprite(x, y, width_, height_, background, 0, true);

		//Renderer::nslice(background, x, y, width_, height_);
	}
};