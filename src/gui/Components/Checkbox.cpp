#include "Checkbox.h"
#include "../../rendering/Renderer.h"
#include <iostream>
#include "../../other/Gamemode_Editor.h"
#include "../../ai/AStar.h"
//#include "SSUI.h"

constexpr int GUI_CHECKBOX_BORDER = 4;
// https://www.youtube.com/watch?v=uK-2jzB41c4

Checkbox::Checkbox(GUIElement* parent, int callback, bool isActive) : GUIElement(parent) {
//	active = isActive;
//	this->callback = callback;
//	backgroundColour = glm::vec4(0, 255.0f, 0, .5f);
}

void Checkbox::onClicked() {
	active = !active;
	BIT_1(Renderer::renderFlags, RENDER_REDRAW_GUI);

	//switch (callback) {
	//case EVENT_CHECKBOX_TOGGLE_ASTAR:
	//	BIT_SET(Renderer::renderFlags, RENDER_ASTAR, active);

	//	if (!BIT_CHECK(Renderer::renderFlags, RENDER_ASTAR))
	//		AStar::paths.clear();
	//	break;
	//case EVENT_CHECKBOX_TOGGLE_CLICKMASK:
	//	BIT_SET(Renderer::renderFlags, RENDER_CLICKMASK, active);
	//	break;
	//case EVENT_CHECKBOX_TOGGLE_LIGHTING:
	//	BIT_SET(Renderer::renderFlags, RENDER_LIGHTING, active);
	//	break;
	//case EVENT_CHECKBOX_TOGGLE_EDITMODE:
	//	BIT_SET(Gamemode_Editor::mode, MODE_EDIT, active);
	//	break;
	//case EVENT_CHECKBOX_TOGGLE_GHOST:
	//	BIT_SET(Gamemode_Editor::constructionFlags, CONSTR_GHOST_ENABLED, active);
	//	break;
	//case EVENT_CHECKBOX_TOGGLE_ENTITIES:
	//	BIT_SET(Renderer::renderFlags, RENDER_ENTITIES, active);
	//	break;
	//}
}

void Checkbox::draw() {
	GUIElement::draw();

	if (active) {
		Renderer::activeDrawBuffer = Renderer::drawBufferShapes;
		Renderer::drawBufferCounterActive = &Renderer::drawBufferCounterShapes;

		Renderer::drawShape(
			x + GUI_CHECKBOX_BORDER,
			y + GUI_CHECKBOX_BORDER,
			width_ - GUI_CHECKBOX_BORDER * 2,
			height_ - GUI_CHECKBOX_BORDER * 2,
			glm::vec4(0, 1, 0, 1)
		);
	}
}