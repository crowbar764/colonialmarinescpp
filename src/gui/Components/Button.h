#pragma once

#include "../GUIElement.h"
#include <functional>

struct Sprite9;

class Button : public GUIElement {
public:
	std::function<void()> callback;
	Sprite9* background;

	Button(GUIElement* parent, std::function<void()> callback);

	void onMouseUp();
	void draw();
};