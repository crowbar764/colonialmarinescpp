#pragma once

#include "../GUIElement.h"
#include "../../common/DefinesSprites.h"
#include "../../rendering/Renderer.h"
#include "../../global/Atlas.h"
#include "../SSUI.h"
#include "Frame.h"
#include <algorithm>

class Panel : public GUIElement
{
public:
	Sprite9* background;

	Panel() : GUIElement(SSUI::main) {
		background = Atlas::mainAtlas->getSprite9(SPRITE_GUI_SLICE_OUT);
		SSUI::panels.emplace_back(this);
	}

	void onMouseDown(bool isPropagation) {
		GUIElement::onMouseDown(true);

		// Move this panel to the front of the draw order.
		std::vector<GUIElement*>::iterator it = std::find(SSUI::main->children.begin(), SSUI::main->children.end(), this);
		std::rotate(it, it + 1, SSUI::main->children.end());

		BIT_1(Renderer::renderFlags, RENDER_REDRAW_GUI);
	}

	void draw() {

		Renderer::activeDrawBuffer = Renderer::drawBufferMain;
		Renderer::drawBufferCounterActive = &Renderer::drawBufferCounterMain;

		Renderer::nslice(background, x, y, width_, height_);
	}

	~Panel() {
		vErase(SSUI::panels, this);
	}
};