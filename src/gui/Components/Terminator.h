#pragma once

#include "../GUIElement.h"
#include "../../common/DefinesSprites.h"
#include "../../global/Atlas.h"

class Terminator : public Button
{
public:
	GUIElement* closeTarget;
	Sprite9* background;

	Terminator(GUIElement* parent, GUIElement* closeTarget) : Button(parent, 0) {
		width = 32;
		height = 32;
		background = Atlas::mainAtlas->getSprite9(SPRITE_GUI_SLICE_DEBUG);
		this->closeTarget = closeTarget;
	}

	void onClicked() {
		GUIElement::onClicked();

		delete(closeTarget);
		BIT_1(Renderer::renderFlags, RENDER_RECALC_GUI);
		BIT_1(Renderer::renderFlags, RENDER_REDRAW_GUI);
		SSUI::hoveredElement = NULL;
	}
};