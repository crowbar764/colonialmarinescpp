#pragma once

#include "../GUIElement.h"

class HorizontalFlow : public GUIElement
{
public:
	HorizontalFlow(GUIElement* parent) : GUIElement(parent) {
	}

	void fitChild(GUIElement* child) {
		// If not first child, add gap spacing.
		if (width_ > marginLeft)
			width_ += gap;

		// Fit child.
		width_ += child->width_;

		// Does other axis need streching to fit this child?
		if (child->height_ > height_)
			height_ = child->height_;
	}

	void addOffset(float x, float y) {
		cursorX += x + gap;
	}
};