#pragma once
#include "../GUIElement.h"
#include <functional>

class Checkbox : public GUIElement
{
public:
	bool active;

	Checkbox(GUIElement* parent, int callback, bool isActive);

	void onClicked();
	void draw();
};