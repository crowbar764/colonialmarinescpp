#pragma once

#include "../../Main.h"
#include "../GUIElement.h"

class Frame : public GUIElement
{
public:
	Frame() : GUIElement(NULL) {
		resize();
	}

	void resize() {
		width_ = (float)Main::width;
		height_ = (float)Main::height;
	}

	void onMouseDown(bool isPropagation) {
	}
};