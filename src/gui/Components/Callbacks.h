#pragma once
#include "../../common/Common.h"

void helloWorld() {
	PRINT("Hello World!");
}

void print(std::string text) {
	PRINT(text);
}

void toggleEditMode() {
	BIT_TOGGLE(Gamemode_Editor::constructionFlags, CONSTR_GHOST_ENABLED);
}

