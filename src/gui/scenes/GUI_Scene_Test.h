#pragma once
#include "../SSUI.h"
#include "../components/Panel.h"
#include "../components/Button.h"
#include "../components/HorizontalFlow.h"
#include "../components/Terminator.h"
#include "../components/Draggable.h"


// Debug panel.
class GUI_Scene_Test : public Panel {
public:
	GUI_Scene_Test() : Panel() {
		positionType = GUIPosition::ABSOLUTE;
		xAlignment = GUIAlignment::MIDDLE;
		yAlignment = GUIAlignment::MIDDLE;

		HorizontalFlow* topBar = new HorizontalFlow(this);
		topBar->stretchableX = GUIStretch::FULL;

		new Draggable(topBar, this);
		new Terminator(topBar, this);

		for ( int y = 0; y < 3; y++ ) {
			HorizontalFlow* horizontal = new HorizontalFlow(this);

			for ( int x = 0; x < 4; x++ ) {
				Button* cell = new Button(horizontal, nothing);
				cell->width = 100;
				cell->height = 100;
			}
		}
	}

	static void nothing() {
		PRINT("Button press.");
	}
};