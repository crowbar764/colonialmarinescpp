#pragma once
#include "../SSUI.h"
#include "../components/Panel.h"
#include "../components/Button.h"
#include "../../global/World.h"
#include "../../other/Gamemode_Editor.h"



// Debug panel.
class GUI_Scene_Debug : public Panel {
public:
	GUI_Scene_Debug() : Panel() {
		positionType = GUIPosition::ABSOLUTE;
		xAlignment = GUIAlignment::END;
		yAlignment = GUIAlignment::MIDDLE;

		Button* cell;

		cell = new Button(this, toggleEditMode);
		cell->width = 100;
		cell->height = 50;

		cell = new Button(this, cycleBuildItem);
		cell->width = 100;
		cell->height = 50;

		cell = new Button(this, save);
		cell->width = 100;
		cell->height = 50;
	}

	static void toggleEditMode() {
		Gamemode_Editor::mode = !Gamemode_Editor::mode;

		PRINT(Gamemode_Editor::mode);
	}

	static void cycleBuildItem() {
		Gamemode_Editor::currentTurf++;
		if ( Gamemode_Editor::currentTurf > NUM_OF_TURF_TYPES )
			Gamemode_Editor::currentTurf = 0;

		PRINT(Gamemode_Editor::currentTurf);
	}

	static void swapWorld() {
		World* world;

		if ( World::activeWorld->ID ) {
			world = World::worlds[0];
		} else {
			world = World::worlds[1];
		}
		World::switchWorld(world);
	}

	static void worldLoadStressTest() {
		LOOP(100) {
			World::clear();

			World* world = new World(WORLD_SHIP, "maps/ships/ship01");
			World* world2 = new World(WORLD_PLANET, "maps/rooms/room02");
			World::switchWorld(world);
			Master::gameState = GAMESTATE_IN_WORLD;
			SSUI::clearScene();
			new GUI_Scene_Debug;


			PRINT(i);
		}
	}

	static void save() {
		//World::saveWorld("ships/ship01");
		World::saveWorld("ships/ship02");
	}
};