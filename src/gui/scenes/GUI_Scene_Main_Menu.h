#pragma once
#include "../SSUI.h"
#include "../components/Panel.h"
#include "../components/Button.h"
#include "../../global/World.h"

// Debug panel.
class GUI_Scene_Main_Menu : public Panel {
public:
	GUI_Scene_Main_Menu() : Panel() {
		positionType = GUIPosition::ABSOLUTE;
		xAlignment = GUIAlignment::MIDDLE;
		yAlignment = GUIAlignment::MIDDLE;

		//Button* cell;

		//cell = new Button(this, std::bind(World::loadWorld, "maps/ships/ship01"));
		//cell->width = 100;
		//cell->height = 50;

		//cell = new Button(this, std::bind(World::loadWorld, "maps/rooms/room02"));
		//cell->width = 100;
		//cell->height = 50;
	}
};