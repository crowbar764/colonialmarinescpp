#pragma once
#include <vector>

enum class GUIPosition
{
	RELATIVE,
	ABSOLUTE
};

enum class GUIAlignment
{
	NONE,
	START,
	MIDDLE,
	END
};

enum class GUIStretch
{
	NONE,
	FILL, // Take up remaining space in HorizontalFlow.
	FULL // Set to be size of parent.
};

class GUIElement {
public:
	unsigned int ID;
	inline static int lastID = 1;
	int cursorShape;

	// Layout.
	float x, y;
	float width, width_;
	float height, height_;
	float cursorX, cursorY;
	GUIStretch stretchableX;

	// Tree.
	//GUIElement* panel;
	GUIElement* parent;
	std::vector<GUIElement*> children;

	// Layout styles.
	GUIPosition positionType;
	float gap = 16.0f;
	float marginTop, marginLeft, marginRight, marginBottom;
	GUIAlignment xAlignment, yAlignment;

	// Visual styles.
	//Sprite9* background;
	//glm::vec4 backgroundColour;
	//texCoordData* backgroundImage;

	// States.
	bool hasHoverEffect, hovered;
	bool pressed;

	GUIElement(GUIElement* parent);

	// IO.
	virtual void onClicked();
	virtual void onMouseDown(bool isPropagation = false);
	virtual void onMouseUp();
	virtual void onHover();
	virtual void onUnhover();
	
	// Overloadables.
	virtual void process();
	virtual void draw();
	//virtual void drawBackgroundImage();
	virtual void fitChild(GUIElement* child);

	// Layout.
	void layoutChildren();
	void stretchChildren();
	void positionChildren();
	void drawChildren();
	void layout();
	void stretch();
	void position();
	void appendTo(GUIElement* parent);

	virtual void addOffset(float x, float y);

	GUIElement* getHoveredElement(int mouseX, int mouseY);
	virtual ~GUIElement();
};