#pragma once
#include "../subsystems/Subsystem.h"

class Frame;
class FrameBuffer;
class GUIElement;

class SSUI : public Subsystem {
public:
	inline static Frame* main;
	inline static std::vector<GUIElement*> panels;
	inline static GUIElement* hoveredElement;
	inline static GUIElement* pressedElement;
	inline static GUIElement* draggable;
	inline static FrameBuffer* GUIFBO;

	SSUI() {
		name = "SSUI";
		runlevels |= GAMESTATE_MAIN_MENU;
		init();
	}

	void fire(bool resumed);

	void init();

	static void draw();
	static void updateHover();
	static bool onMouseUp();
	static void clearScene();
};