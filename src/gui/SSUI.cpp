#include "SSUI.h"
#include "../rendering/Renderer.h"
#include <iostream>
#include "../io/Mouse.h"
#include "components/Checkbox.h"
#include "components/Button.h"
#include <functional>
#include "font/SSText.h"
#include "../other/Gamemode_Editor.h"
#include "components/Panel.h"
#include "components/HorizontalFlow.h"
#include "components/Terminator.h"
#include "components/Label.h"
#include "components/Draggable.h"
#include "font/SSText.h"
#include "components/Callbacks.h"
#include "../global/World.h"
#include "../rendering/FrameBuffer.h"

void SSUI::fire(bool resumed) {
	Ease::update();

	if ( draggable ) {
		draggable->process();
	}
}

void SSUI::draw() {
	const bool recalculate = BIT_CHECK(Renderer::renderFlags, RENDER_RECALC_GUI);
	const bool redraw = BIT_CHECK(Renderer::renderFlags, RENDER_REDRAW_GUI);

	if (recalculate) {
		//PRINT("RECALCULATE GUI");

		// Calculate sizes.
		main->layoutChildren();
		// Stretch out stretchables.
		main->stretchChildren();
		// Calculate final screen positions.
		main->positionChildren();
	}

	// If redrawing, clear canvas and set up for painting.
	if (redraw) {
		//Renderer::drawCalls = 0;
		//PRINT("REDRAW GUI");

		GUIFBO->setAsTarget();
		glClearColor(0.0f, 0.0f, 0.0f, 0.0f);
		glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

		// These are absolute and might overlap so fully draw one at a time.
		for (GUIElement* child : main->children) {
			child->draw();
			child->drawChildren();

			// Draw shapes first.
			Renderer::activeDrawBuffer = Renderer::drawBufferShapes;
			Renderer::drawBufferCounterActive = &Renderer::drawBufferCounterShapes;

			Renderer::shapeShader.use();
			glBindVertexArray(Renderer::shapeVAO);
			glBindBuffer(GL_ARRAY_BUFFER, Renderer::shapeVBO);

			Renderer::drawToBuffer(NULL, VERTEX_PAYLOAD_SHAPE);

			// Draw sprites next.
			Renderer::activeDrawBuffer = Renderer::drawBufferMain;
			Renderer::drawBufferCounterActive = &Renderer::drawBufferCounterMain;

			Renderer::ourShader.use();
			glBindVertexArray(Renderer::VAO);
			glBindBuffer(GL_ARRAY_BUFFER, Renderer::VBO);

			Renderer::drawToBuffer(Atlas::mainAtlas->atlas);

			// Draw text next.
			Renderer::activeDrawBuffer = Renderer::drawBufferText;
			Renderer::drawBufferCounterActive = &Renderer::drawBufferCounterText;

			SSText::fontShader.use();
			glBindVertexArray(SSText::fontVAO);
			glBindBuffer(GL_ARRAY_BUFFER, SSText::fontVBO);

			Renderer::drawToBuffer(SSText::atlas);
		}

		// Reset.
		Renderer::ourShader.use();
		glBindVertexArray(Renderer::VAO);
		glBindBuffer(GL_ARRAY_BUFFER, Renderer::VBO);
		Renderer::activeDrawBuffer = Renderer::drawBufferMain;
		Renderer::drawBufferCounterActive = &Renderer::drawBufferCounterMain;


		BIT_0(Renderer::renderFlags, RENDER_RECALC_GUI);
		BIT_0(Renderer::renderFlags, RENDER_REDRAW_GUI);
		//PRINT("Draws: " << Renderer::drawCalls);
	}

	// Draw GUI FBO to screen.
	FrameBuffer::switchToMain();
	GUIFBO->setAsSource();
	Renderer::RenderToScreen(glm::vec4 {
		0,
		0,
		Main::width,
		Main::height
	});

	//glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
	
}

// Uses math to check if we are hovering over a GUI element.
// May ask for redraw.
void SSUI::updateHover() {
	GUIElement* newHoveredElement = NULL;

	// Check tree for hovered element. (Reversed foreach loop for correct ordering).
	for (int i = (int)main->children.size(); i != 0; i--) {
		GUIElement* child = main->children[i - 1];

		newHoveredElement = child->getHoveredElement(Mouse::mousePos.x, Mouse::mousePos.y);

		if (newHoveredElement) {

			// Same element? Do nothing.
			if (newHoveredElement == hoveredElement)
				return;

			break;
		}
	}

	// If new hover element.
	if (newHoveredElement) {
		if (hoveredElement)
			hoveredElement->onUnhover();

		// Only trigger redraw if before / after elements have hover effects.
		if (newHoveredElement->hasHoverEffect || (hoveredElement && hoveredElement->hasHoverEffect))
			BIT_1(Renderer::renderFlags, RENDER_REDRAW_GUI);

		hoveredElement = newHoveredElement;
		hoveredElement->onHover();

	// If no hover element, but had one.
	} else if (hoveredElement) {
		hoveredElement->onUnhover();

		if (hoveredElement->hasHoverEffect)
			BIT_1(Renderer::renderFlags, RENDER_REDRAW_GUI);

		hoveredElement = NULL;
	}
}

// Return true to stop further mouse propagation.
bool SSUI::onMouseUp() {
	bool clickedOnGUI = false;
	if (hoveredElement)
		clickedOnGUI = true;

	// If dragging an element, release it.
	if (draggable)
		draggable->onMouseUp();

	if (clickedOnGUI) {
		hoveredElement->onMouseUp();

		// If release element == current element, it is clicked.
		if (pressedElement && pressedElement == hoveredElement)
			pressedElement->onClicked();
	}

	// Reset.
	if (pressedElement) {
		pressedElement->pressed = false;
		pressedElement = NULL;
	}

	return clickedOnGUI;
}

void SSUI::init() {
	Subsystem::init();

	GUIFBO = new FrameBuffer();
	main = new Frame();

	if (false) {
		//new Checkbox(Main::app_width - 256.0f, 192.0f, 32.0f, 32.0f, EVENT_CHECKBOX_TOGGLE_ASTAR, BIT_CHECK(Renderer::renderFlags, RENDER_ASTAR));
		//SSText::newText("AStar", Main::app_width - 220.0f, 195.0f, .3f, glm::vec4(1, 1, 1, 1), true);

		//new Checkbox(Main::app_width - 256.0f, 232.0f, 32.0f, 32.0f, EVENT_CHECKBOX_TOGGLE_CLICKMASK, BIT_CHECK(Renderer::renderFlags, RENDER_CLICKMASK));
		//SSText::newText("Clickmask", Main::app_width - 220.0f, 235.0f, .3f, glm::vec4(1, 1, 1, 1), true);

		//new Button(Main::app_width - 256.0f, 272.0f, 32.0f, 32.0f, EVENT_BUTTON_SAVE_WORLD);
		//SSText::newText("Save world", Main::app_width - 220.0f, 275.0f, .3f, glm::vec4(1, 1, 1, 1), true);

		//new Checkbox(Main::app_width - 256.0f, 312.0f, 32.0f, 32.0f, EVENT_CHECKBOX_TOGGLE_LIGHTING, BIT_CHECK(Renderer::renderFlags, RENDER_LIGHTING));
		//SSText::newText("Lighting", Main::app_width - 220.0f, 315.0f, .3f, glm::vec4(1, 1, 1, 1), true);

		//new Checkbox(Main::app_width - 256.0f, 352.0f, 32.0f, 32.0f, EVENT_CHECKBOX_TOGGLE_EDITMODE, BIT_CHECK(Gamemode_Editor::mode, MODE_EDIT));
		//SSText::newText("Edit Mode", Main::app_width - 220.0f, 355.0f, .3f, glm::vec4(1, 1, 1, 1), true);

		//new Button(Main::app_width - 256.0f, 392.0f, 32.0f, 32.0f, EVENT_BUTTON_CYCLE_EDITMODE);
		//SSText::newText("Cycle edit", Main::app_width - 220.0f, 395.0f, .3f, glm::vec4(1, 1, 1, 1), true);

		//new Checkbox(Main::app_width - 256.0f, 432.0f, 32.0f, 32.0f, EVENT_CHECKBOX_TOGGLE_GHOST, BIT_CHECK(Gamemode_Editor::constructionFlags, CONSTR_GHOST_ENABLED));
		//SSText::newText("Ghost :o", Main::app_width - 220.0f, 435.0f, .3f, glm::vec4(1, 1, 1, 1), true);

		//new Checkbox(Main::app_width - 256.0f, 472.0f, 32.0f, 32.0f, EVENT_CHECKBOX_TOGGLE_ENTITIES, BIT_CHECK(Renderer::renderFlags, RENDER_ENTITIES));
		//SSText::newText("Entities", Main::app_width - 220.0f, 475.0f, .3f, glm::vec4(1, 1, 1, 1), true);
	}

	if ( false ) {
		for ( int y = 0; y < 3; y++ ) {
			Panel* panel = new Panel();
			panel->positionType = GUIPosition::ABSOLUTE;
			panel->xAlignment = GUIAlignment::MIDDLE;
			panel->yAlignment = GUIAlignment::END;

			HorizontalFlow* panel2 = new HorizontalFlow(panel);
			panel2->stretchableX = GUIStretch::FULL;

			new Draggable(panel2, panel);
			new Terminator(panel2, panel);

			for ( int y = 0; y < 3; y++ ) {
				HorizontalFlow* horizontal = new HorizontalFlow(panel);

				for ( int x = 0; x < 4; x++ ) {
					Button* cell = new Button(horizontal, 0);
					cell->width = 100;
					cell->height = 100;
				}
			}

			//new Label(panel);
		}
	}

	//new GUI_Scene_Debug;




	

	//new Checkbox(panel2, 0, 0, .5f, .5f, EVENT_CHECKBOX_TOGGLE_CLICKMASK, BIT_CHECK(Renderer::renderFlags, RENDER_CLICKMASK));
	//SSText::newText("Clickmask", Main::app_width - 220.0f, 235.0f, .3f, glm::vec4(1, 1, 1, 1), true);
}

void SSUI::clearScene() {
	for ( GUIElement* panel : panels )
		delete(panel);

	panels.clear();
	hoveredElement = NULL;
	pressedElement = NULL;
	draggable = NULL;

	Renderer::renderFlags |= RENDER_RECALC_GUI;
	Renderer::renderFlags |= RENDER_REDRAW_GUI;
}