#pragma once

/**
 * @param id
 *            - the ASCII value of the character.
 * @param xTextureCoord
 *            - the x texture coordinate for the top left corner of the
 *            character in the texture atlas.
 * @param yTextureCoord
 *            - the y texture coordinate for the top left corner of the
 *            character in the texture atlas.
 * @param xTexSize
 *            - the width of the character in the texture atlas.
 * @param yTexSize
 *            - the height of the character in the texture atlas.
 * @param xOffset
 *            - the x distance from the curser to the left edge of the
 *            character's quad.
 * @param yOffset
 *            - the y distance from the curser to the top edge of the
 *            character's quad.
 * @param sizeX
 *            - the width of the character's quad in screen space.
 * @param sizeY
 *            - the height of the character's quad in screen space.
 * @param xAdvance
 *            - how far in pixels the cursor should advance after adding
 *            this character.
 */

struct Character
{
	int id;
	float UVx;
	float UVy;
	float UVx2;
	float UVy2;
	float xOffset;
	float yOffset;
	float sizeX;
	float sizeY;
	float xAdvance;
	float width;
	float height;

	glm::vec4 getUV() {
		return(glm::vec4{
			UVx,
			UVy,
			UVx2,
			UVy2
		});
	}
};

//glm::vec4 getCharUV