#include "SSText.h"

#include "../../rendering/FrameBuffer.h"
#include "../../global/ResourceManager.h"
#include "../../global/Camera.h"
#include "../../io/Mouse.h"
#include "../../rendering/Renderer.h"
#include "../../global/Atlas.h"

#define FLOATER_SPEED 50;

unsigned int SSText::fontVBO, SSText::fontVAO;
Shader SSText::fontShader;
MetaFile SSText::metaFile;

float SSText::cursorX;
std::vector<Text*> SSText::texts;
std::vector<Text*> SSText::floatingTexts;

void SSText::draw() {
    fontShader.use();
    glBindVertexArray(fontVAO);
    glBindBuffer(GL_ARRAY_BUFFER, fontVBO);

    for (Text* text : texts) {
        if (text->isGUI) {
            drawText(
                text,
                text->x,
                text->y
            );
        } else {
            drawText(
                text,
                (text->x * Renderer::tileRes) - Camera::X,
                (text->y * Renderer::tileRes) - Camera::Y
            );
        }
    }

    Renderer::drawToBuffer(atlas);

    // Reset.
    Renderer::ourShader.use();
    glBindVertexArray(Renderer::VAO);
    glBindBuffer(GL_ARRAY_BUFFER, Renderer::VBO);
}

void SSText::drawText(Text* text, float x, float y) {
    for (CharacterMesh* mesh : text->meshes) {

        float posX = x + mesh->scaledXOffset;
        float posY = y + mesh->scaledYOffset;
        float posX2 = posX + mesh->scaledWidth;
        float posY2 = posY + mesh->scaledHeight;
        glm::vec4 uv = mesh->UV;
        glm::vec4 colour = text->colour;

        float values[] = {
            posX, posY, uv.x, uv.y, colour.r, colour.g, colour.b, colour.a,
            posX, posY2, uv.x, uv.w, colour.r, colour.g, colour.b, colour.a,
            posX2, posY, uv.z, uv.y, colour.r, colour.g, colour.b, colour.a,
            posX2, posY, uv.z, uv.y, colour.r, colour.g, colour.b, colour.a,
            posX, posY2, uv.x, uv.w, colour.r, colour.g, colour.b, colour.a,
            posX2, posY2, uv.z, uv.w, colour.r, colour.g, colour.b, colour.a
        };

        Renderer::addToDrawQueue(values, 48);
    }
}

void SSText::newCursorText(std::string content) {
    glm::vec2 worldPos = Mouse::getMousePosWorld();
    SSText::newText(content, worldPos.x, worldPos.y, .25, glm::vec4(1, 1, 1, 1), false, true);
}

Text* SSText::newText(std::string content, float x, float y, float scale, glm::vec4 colour, bool isGUI, bool isFloater) {
    Text* newText = new Text{
        content,
        x,
        y,
        scale,
        colour,
        isGUI
    };

    newText->regenerateMesh();

    texts.push_back(newText);

    if (isFloater) {
        newText->ease = new EaseFloat(&newText->y, newText->y - 100, 1.75f, false);
        floatingTexts.push_back(newText);
    }

    return newText;
}

void SSText::deleteText(Text* text) {
    //std::vector<Text*>::iterator it = std::find(texts.begin(), texts.end(), text);
    //texts.erase(it); // try changing to fasterase
    vErase(texts, text);
    delete text;
}

void SSText::regenerateMeshes() {
    for (Text* text : texts) {
        if (!text->isGUI)
            text->regenerateMesh();
    }
}

void SSText::update(float deltaTime) {
    for (Text* text : floatingTexts) {
        if (text->ease->tick()) {
            std::vector<Text*>::iterator it = std::find(SSText::floatingTexts.begin(), SSText::floatingTexts.end(), text);
            SSText::floatingTexts.erase(it); // try changing to fasterase
            deleteText(text);
        }
    }
}

void SSText::init() {
    int size = 512;

    // Load font atlas image.
    RawImageData* image = ResourceManager::loadImageFromFile("assets/fonts/first.png");
    Atlas atlasTemp;
    atlasTemp.addToLoadBuffer(image, "first.png");
    atlasTemp.createAtlas(size);
    Atlas::clearLoadBuffer(true); // Don't need TexCoord data, delete it.
    
    atlas = atlasTemp.atlas;

    // Load font atlas data.
    metaFile.init("assets/fonts/first.fnt");

    // Create font shader.
    FrameBuffer::createShader(&fontShader, "shaderFont");
    glGenVertexArrays(1, &fontVAO);
    glGenBuffers(1, &fontVBO);
    glBindVertexArray(fontVAO);
    glBindBuffer(GL_ARRAY_BUFFER, fontVBO);
    glVertexAttribPointer(0, 4, GL_FLOAT, GL_FALSE, 8 * sizeof(float), (void*)0);
    glEnableVertexAttribArray(0);
    glVertexAttribPointer(1, 4, GL_FLOAT, GL_FALSE, 8 * sizeof(float), (void*)(4 * sizeof(float)));
    glEnableVertexAttribArray(1);

    // Set back to regular.
    Renderer::ourShader.use();
    FrameBuffer::switchToMain();
    glBindVertexArray(Renderer::VAO);
    glBindBuffer(GL_ARRAY_BUFFER, Renderer::VBO);
}