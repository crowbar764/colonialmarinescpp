#pragma once

#include "TextMeshCreator.h"
#include "TextMeshData.h"
#include "GUIText.h"

class FontType {
public:

	int textureAtlas;
	TextMeshCreator loader;

	/**
	 * Creates a new font and loads up the data about each character from the
	 * font file.
	 *
	 * @param textureAtlas
	 *            - the ID of the font atlas texture.
	 * @param fontFile
	 *            - the font file containing information about each character in
	 *            the texture atlas.
	 */
	void init(int textureAtlas, std::string fontFile) {
		this->textureAtlas = textureAtlas;
		this->loader.init(fontFile);
	}

	/**
	 * Takes in an unloaded text and calculate all of the vertices for the quads
	 * on which this text will be rendered. The vertex positions and texture
	 * coords and calculated based on the information from the font file.
	 *
	 * @param text
	 *            - the unloaded text.
	 * @return Information about the vertices of all the quads.
	 */
	 TextMeshData loadText(GUIText text) {
	 	return loader.createTextMesh(text);
	 }

};
