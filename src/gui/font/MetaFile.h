#pragma once

//#include <vector>
//#include <string>
#include <unordered_map>

#include "../../Main.h"
#include "Character.h"
//#include "TextMeshCreator.h"
#include "../../common/StringHelpers.h"
#include "../../common/Common.h"

#include <fstream>

/**
 * Provides functionality for getting the values from a font file.
 *
 * @author Karl
 *
 */
class MetaFile {
public:

	static constexpr int PAD_TOP = 0;
	static constexpr int PAD_LEFT = 1;
	static constexpr int PAD_BOTTOM = 2;
	static constexpr int PAD_RIGHT = 3;

	static constexpr int DESIRED_PADDING = 3;

	//static constexpr std::string SPLITTER = " ";
	//static constexpr std::string NUMBER_SEPARATOR = ",";

	float aspectRatio;

	float verticalPerPixelSize;
	float horizontalPerPixelSize;
	float spaceWidth;
	std::vector<int> padding;
	int paddingWidth;
	int paddingHeight;

	std::unordered_map<int, Character> metaData = std::unordered_map<int, Character>();

	std::ifstream fileStream;
	std::unordered_map<std::string, std::string> values = std::unordered_map<std::string, std::string>();

	/**
	 * Opens a font file in preparation for reading.
	 *
	 * @param file
	 *            - the font file.
	 */
	void init(std::string fileName) {
		aspectRatio = (float)Main::width / (float)Main::height;
		openFile(fileName);
		loadPaddingData();
		loadLineSizes();
		int imageWidth = getValueOfVariable("scaleW");
		loadCharacterData(imageWidth);
		padding.clear();
		fileStream.close();

		//std::cout << getCharacter(34).yOffset;
	}

	Character getCharacter(int ascii) {
		return metaData.at(ascii);
	}

	/**
	 * Read in the next line and store the variable values.
	 *
	 * @return {@code true} if the end of the file hasn't been reached.
	 */
	bool processNextLine() {
		values.clear();
		std::string line;
		try {
			getline(fileStream, line);
		}
		catch (const std::exception& e) {
			PRINT_ERROR(e.what());
		}

		if (line.empty() || !line.rfind("kerning", 0)) {
			return false;
		}

		for (std::string part : splitString(line, ' ')) {
			std::vector<std::string> valuePairs = splitString(part, '=');

			if (valuePairs.size() == 2) {
				std::pair<std::string, std::string> param (valuePairs[0], valuePairs[1]);
				//std::cout << "Param part 0: " << valuePairs[0] << " 1: " << valuePairs[1] << "\n";
				values.insert(param);
			}
		}

		//for (std::string part : values) {
			//std::cout << part << "\n";
		//}

		/*for (auto& it : values) {
			std::cout << it.first << " " << it.second << "\n";
		}*/

		return true;
	}

	/**
	 * Gets the {@code int} value of the variable with a certain name on the
	 * current line.
	 *
	 * @param variable
	 *            - the name of the variable.
	 * @return The value of the variable.
	 */
	int getValueOfVariable(std::string variable) {
		return std::stoi(values.at(variable));
	}

	/**
	 * Gets the array of ints associated with a variable on the current line.
	 *
	 * @param variable
	 *            - the name of the variable.
	 * @return The int array of values associated with the variable.
	 */
	std::vector<int> getValuesOfVariable(std::string variable) {
		std::vector<std::string> numbers = splitString(values.at(variable), ',');
		std::vector<int> actualValues;
		for (int i = 0; i < numbers.size(); i++) {
			actualValues.push_back(std::stoi(numbers[i]));
		}
		return actualValues;
	}

	/**
	 * Opens the font file, ready for reading.
	 *
	 * @param file
	 *            - the font file.
	 */
	void openFile(std::string fileName) {
		try {
			fileStream.open(fileName);
		}
		catch (const std::exception& e) {
			PRINT_ERROR(e.what());
		}
	}

	/**
	 * Loads the data about how much padding is used around each character in
	 * the texture atlas.
	 */
	void loadPaddingData() {
		processNextLine();
		padding = getValuesOfVariable("padding");
		paddingWidth = padding[PAD_LEFT] + padding[PAD_RIGHT];
		paddingHeight = padding[PAD_TOP] + padding[PAD_BOTTOM];
	}

	/**
	 * Loads information about the line height for this font in pixels, and uses
	 * this as a way to find the conversion rate between pixels in the texture
	 * atlas and screen-space.
	 */
	void loadLineSizes() {
		processNextLine();
		int lineHeightPixels = getValueOfVariable("lineHeight") - paddingHeight;
		verticalPerPixelSize = LINE_HEIGHT / (float)lineHeightPixels;
		horizontalPerPixelSize = verticalPerPixelSize / aspectRatio;
	}

	/**
	 * Loads in data about each character and stores the data in the
	 * {@link Character} class.
	 *
	 * @param imageWidth
	 *            - the width of the texture atlas in pixels.
	 */
	void loadCharacterData(int imageWidth) {
		processNextLine();
		processNextLine();

		while (processNextLine()) {
			loadCharacter(imageWidth);
		}
	}

	/**
	 * Loads all the data about one character in the texture atlas and converts
	 * it all from 'pixels' to 'screen-space' before storing. The effects of
	 * padding are also removed from the data.
	 *
	 * @param imageSize
	 *            - the size of the texture atlas in pixels.
	 * @return The data about the character.
	 */
	void loadCharacter(int imageSize) {
		int id = getValueOfVariable("id");
		if (id == SPACE_ASCII) {
			spaceWidth = (float)(getValueOfVariable("xadvance") - paddingWidth);
			return;
		}
		float xTex = ((float)getValueOfVariable("x") + (padding[PAD_LEFT] - DESIRED_PADDING));
		float yTex = 1 - ((float)getValueOfVariable("y") + (padding[PAD_TOP] - DESIRED_PADDING));
		int width = getValueOfVariable("width") - (paddingWidth - (2 * DESIRED_PADDING));
		int height = getValueOfVariable("height") - ((paddingHeight)-(2 * DESIRED_PADDING));
		float quadWidth = width * horizontalPerPixelSize;
		float quadHeight = height * verticalPerPixelSize;
		float xTexSize = (float)width;
		float yTexSize = (float)height;
		float xOff = (float)(getValueOfVariable("xoffset") + padding[PAD_LEFT] - DESIRED_PADDING);
		float yOff = (float)(getValueOfVariable("yoffset") + (padding[PAD_TOP] - DESIRED_PADDING));
		float xAdvance = (float)(getValueOfVariable("xadvance") - paddingWidth);

		Character myChar = Character(
			id,
			xTex / imageSize,
			yTex / imageSize,
			(xTexSize + xTex) / imageSize,
			(yTex - yTexSize) / imageSize,
			xOff,
			yOff,
			quadWidth,
			quadHeight,
			xAdvance,
			(float)width,
			(float)height
		);
		std::pair<int, Character> newCharacter (id, myChar);
		
		metaData.insert(newCharacter);
	}
};