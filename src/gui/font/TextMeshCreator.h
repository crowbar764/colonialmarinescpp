#pragma once

#include "MetaFile.h"
#include "Word.h"
#include "Line.h"
#include <string>

class TextMeshCreator {
public:


	MetaFile metaData;

	void init(std::string metaFile) {
		metaData.init(metaFile);// = MetaFile(metaFile);

		//std::cout
	}

	TextMeshData createTextMesh(GUIText text) {
		std::vector<Line> lines = createStructure(text);
		TextMeshData data = createQuadVertices(text, lines);
		return data;
	}

	std::vector<Line> createStructure(GUIText text) {
		std::vector<char> chars;
		std::copy(text.textString.begin(), text.textString.end(), std::back_inserter(chars));
		std::vector<Line> lines;
		Line currentLine = Line(metaData.spaceWidth, text.fontSize, text.lineMaxSize);
		Word currentWord = Word(text.fontSize);
		for (char c : chars) {
			int ascii = (int)c;
			if (ascii == SPACE_ASCII) {
				bool added = currentLine.attemptToAddWord(currentWord);
				if (!added) {
					lines.push_back(currentLine);
					currentLine = Line(metaData.spaceWidth, text.fontSize, text.lineMaxSize);
					currentLine.attemptToAddWord(currentWord);
				}
				currentWord = Word(text.fontSize);
				continue;
			}
			Character character = metaData.getCharacter(ascii);
			currentWord.addCharacter(character);
		}
		completeStructure(lines, currentLine, currentWord, text);
		return lines;
	}

	void completeStructure(std::vector<Line> lines, Line currentLine, Word currentWord, GUIText text) {
		bool added = currentLine.attemptToAddWord(currentWord);
		if (!added) {
			lines.push_back(currentLine);
			currentLine = Line(metaData.spaceWidth, text.fontSize, text.lineMaxSize);
			currentLine.attemptToAddWord(currentWord);
		}
		lines.push_back(currentLine);
	}

	TextMeshData createQuadVertices(GUIText text, std::vector<Line> lines) {
		text.numberOfLines = lines.size();
		double curserX = 0.0f;
		double curserY = 0.0f;
		std::vector<float> vertices;
		std::vector<float> textureCoords;
		for (Line line : lines) {
			if (text.centerText) {
				curserX = (line.maxLength - line.currentLineLength) / 2;
			}
			for (Word word : line.words) {
				for (Character letter : word.characters) {
					addVerticesForCharacter(curserX, curserY, letter, text.fontSize, vertices);
					addTexCoords(textureCoords, letter.x, letter.y,
						letter.UVx2, letter.UVy2);
					curserX += letter.xAdvance * text.fontSize;
				}
				curserX += metaData.spaceWidth * text.fontSize;
			}
			curserX = 0;
			curserY += LINE_HEIGHT * text.fontSize;
		}
		return TextMeshData(vertices, textureCoords);
	}

	void addVerticesForCharacter(double curserX, double curserY, Character character, double fontSize,
		std::vector<float> vertices) {
		double x = curserX + (character.xOffset * fontSize);
		double y = curserY + (character.yOffset * fontSize);
		double maxX = x + (character.sizeX * fontSize);
		double maxY = y + (character.sizeY * fontSize);
		double properX = (2 * x) - 1;
		double properY = (-2 * y) + 1;
		double properMaxX = (2 * maxX) - 1;
		double properMaxY = (-2 * maxY) + 1;
		addVertices(vertices, properX, properY, properMaxX, properMaxY);
	}

	static void addVertices(std::vector<float> vertices, double x, double y, double maxX, double maxY) {
		vertices.push_back((float)x);
		vertices.push_back((float)y);
		vertices.push_back((float)x);
		vertices.push_back((float)maxY);
		vertices.push_back((float)maxX);
		vertices.push_back((float)maxY);
		vertices.push_back((float)maxX);
		vertices.push_back((float)maxY);
		vertices.push_back((float)maxX);
		vertices.push_back((float)y);
		vertices.push_back((float)x);
		vertices.push_back((float)y);
	}

	static void addTexCoords(std::vector<float> texCoords, double x, double y, double maxX, double maxY) {
		texCoords.push_back((float)x);
		texCoords.push_back((float)y);
		texCoords.push_back((float)x);
		texCoords.push_back((float)maxY);
		texCoords.push_back((float)maxX);
		texCoords.push_back((float)maxY);
		texCoords.push_back((float)maxX);
		texCoords.push_back((float)maxY);
		texCoords.push_back((float)maxX);
		texCoords.push_back((float)y);
		texCoords.push_back((float)x);
		texCoords.push_back((float)y);
	}


	//static float[] listToArray(List<Float> listOfFloats) {
	//	float[] array = new float[listOfFloats.size()];
	//	for (int i = 0; i < array.length; i++) {
	//		array[i] = listOfFloats.get(i);
	//	}
	//	return array;
	//}

};