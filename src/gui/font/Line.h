#pragma once
#include <vector>

#include "Word.h"

class Line {
public:
	double maxLength;
	double spaceSize;

	std::vector<Word> words;
	double currentLineLength = 0;

	Line(double spaceWidth, double fontSize, double maxLength) {
		spaceSize = spaceWidth * fontSize;
		this->maxLength = maxLength;
	}

	bool attemptToAddWord(Word word) {
		double additionalLength = word.width;
		additionalLength += words.empty() ? spaceSize : 0;

		if (currentLineLength + additionalLength <= maxLength) {
			words.push_back(word);
			currentLineLength += additionalLength;
			return true;
		}

		return false;
	}
};