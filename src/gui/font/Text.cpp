#include "Text.h"
#include "Character.h"
#include "SSText.h"
#include "../../rendering/Renderer.h"

void Text::regenerateMesh() {

    // Remove old values
    for (CharacterMesh* mesh : meshes)
        delete mesh;
    meshes.clear();

    float cursorX = 0;
    float properScale;

    if (isGUI)
        properScale = scale;
    else
        properScale = scale * Renderer::tileRes;

    for (char characterCode : content) {
        if (characterCode == ' ') {
            cursorX += SSText::metaFile.spaceWidth * properScale;
            continue;
        }

        Character character = SSText::metaFile.metaData.at(characterCode);

        meshes.push_back(new CharacterMesh{
            character.width * properScale,
            character.height * properScale,
            (character.xOffset * properScale) + cursorX,
            character.yOffset * properScale,
            character.xAdvance * properScale,
            character.getUV()
        });

        cursorX += character.xAdvance * properScale;
    }
}

Text::~Text() {
    for (CharacterMesh* mesh : meshes)
        delete mesh;

    if (ease)
        delete(ease);
}