#pragma once
#include <vector>

class TextMeshData {
public:

	std::vector<float> vertexPositions;
	std::vector<float> textureCoords;

	TextMeshData(std::vector<float> vertexPositions, std::vector<float> textureCoords) {
		this->vertexPositions = vertexPositions;
		this->textureCoords = textureCoords;
	}

	int getVertexCount() {
		return vertexPositions.size() / 2;
	}

};