#pragma once
#include <map>
#include <unordered_map>

#include "Text.h"
//#include "../../rendering/Renderer.h"
//#include "../../global/Atlas.h"
#include "MetaFile.h"
//#include "../../rendering/shader.h"

class Shader;

//struct Character {
//    unsigned int TextureID;  // ID handle of the glyph texture
//    glm::ivec2   Size;       // Size of glyph
//    glm::ivec2   Bearing;    // Offset from baseline to left/top of glyph
//    unsigned int Advance;    // Offset to advance to next glyph
//};

class SSText {
public:
    static unsigned int fontVAO, fontVBO;
    static Shader fontShader;
    //static Atlas fontAtlas;
    inline static unsigned int atlas;
    static float cursorX;
    static MetaFile metaFile;
    static std::vector<Text*> texts;
    static std::vector<Text*> floatingTexts;

    static void drawText(Text*, float, float);
    static void newCursorText(std::string content);
    static Text* newText(std::string content, float x, float y, float scale, glm::vec4 colour, bool isGUI, bool isFloater = false);
    static void deleteText(Text*);
    static void regenerateMeshes();
    static void init();
    static void draw();
    static void update(float deltaTime);
    //static void drawText(std::string, float, float, float, glm::vec4);
    //static void drawWord(std::string_view, float, float, float);
    //static void drawCharacter(char, float, float, float);
private:
};