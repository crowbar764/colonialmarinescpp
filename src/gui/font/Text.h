#pragma once
#include <string>
#include <glm/glm.hpp>
#include "../../other/Easing.h"

class CharacterMesh {
public:
    float scaledWidth, scaledHeight, scaledXOffset, scaledYOffset, xAdvance;
    glm::vec4 UV;
};

class Text {
public:
    std::string content;
    float x, y, scale;
    glm::vec4 colour;
    bool isGUI;
    std::vector<CharacterMesh*> meshes;
    EaseFloat* ease;

    void regenerateMesh();
    ~Text();
};