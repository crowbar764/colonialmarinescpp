#pragma once
#include <vector>

#include "Character.h"

class Word {
public:
	std::vector<Character> characters;
	double width = 0;
	double fontSize;

	Word(double fontSize) {
		this->fontSize = fontSize;
	}

	void addCharacter(Character character) {
		characters.push_back(character);
		width += character.xAdvance * fontSize;
	}
};