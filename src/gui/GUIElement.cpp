#include "GUIElement.h"
#include "../rendering/Renderer.h"
#include "../io/ClickMask.h"
#include "SSUI.h"
#include <ctype.h>
#include "font/SSText.h"

GUIElement::GUIElement(GUIElement* parent) {
	ID = lastID++;

	if (parent)
		appendTo(parent);
}

void GUIElement::layout() {
	width_ = marginLeft;
	height_ = marginTop;

	// Calculate size / offsets of children first.
	layoutChildren();

	// If hard sizes set, use them instead.
	if (width)
		width_ = width;
	if (height)
		height_ = height;

	// Update parent's size to fit this new element.
	if (positionType != GUIPosition::ABSOLUTE) {
		parent->fitChild(this);
	}
}

void GUIElement::stretch() {
	if (stretchableX == GUIStretch::FULL) {
		width_ = parent->width_;
	} else if (stretchableX == GUIStretch::FILL) {
		float availableSpace = parent->width_;

		// Calculate how much space of parent is unfilled.
		for (GUIElement* child : parent->children)
			availableSpace -= child->width_;

		// Don't fill gap space.
		availableSpace -= ((parent->children.size() - 1) * parent->gap);

		width_ += availableSpace;
	}
}

void GUIElement::fitChild(GUIElement* child) {
	// If not first child, add gap spacing.
	if (height_ > marginTop)
		height_ += gap;

	// Fit child.
	height_ += child->height_;

	// Does other axis need streching to fit this child?
	if (child->width_ > width_)
		width_ = child->width_;
}

void GUIElement::position() {
	cursorX = .0f;
	cursorY = .0f;

	if (positionType == GUIPosition::ABSOLUTE) {

		if (xAlignment == GUIAlignment::MIDDLE) {
			x = std::floor(parent->x + ((parent->width_ - width_) / 2.0f));
		} else if (xAlignment == GUIAlignment::END) {
			x = std::floor(parent->x + (parent->width_ - width_));
		} else if (xAlignment == GUIAlignment::START) {
			x = std::floor(parent->x);
		}


		if (yAlignment == GUIAlignment::MIDDLE) {
			y = std::floor(parent->y + ((parent->height_ - height_) / 2.0f));
		} else if (yAlignment == GUIAlignment::END) {
			y = std::floor(parent->y + (parent->height_ - height_));
		} else if (yAlignment == GUIAlignment::START) {
			y = std::floor(parent->y);
		}

	} else {
		
		//PRINT("Parent cursor Y: " << parent->cursorY);
		x = parent->x + parent->cursorX;
		y = parent->y + parent->cursorY;

		parent->addOffset(width_, height_);
	}

	positionChildren();
}

void GUIElement::addOffset(float x, float y) {
	cursorY += y + gap;
}

void GUIElement::draw() {
}

void GUIElement::drawChildren() {
	for (GUIElement* child : children) {
		child->draw();
		child->drawChildren();
	}
}

void GUIElement::positionChildren() {
	for (GUIElement* child : children)
		child->position();
}

void GUIElement::layoutChildren() {
	for (GUIElement* child : children)
		child->layout();
}

void GUIElement::stretchChildren() {
	for (GUIElement* child : children) {
		child->stretch();
		child->stretchChildren();
	}
}

void GUIElement::process() {

}

void GUIElement::appendTo(GUIElement* parent) {
	this->parent = parent;
	parent->children.push_back(this);
}

GUIElement* GUIElement::getHoveredElement(int mouseX, int mouseY) {
	// Check if mouse is within this element's bounds.
	if (mouseX > x &&
		mouseX < x + width_ &&
		mouseY > y &&
		mouseY < y + height_) {	

		// Check if a child was real target.
		for (GUIElement* child : children) {
			GUIElement* hoveredChild = child->getHoveredElement(mouseX, mouseY);

			if (hoveredChild)
				return hoveredChild;
		}

		return this;
	}

	return NULL;
}

void GUIElement::onMouseDown(bool isPropagation) {
	parent->onMouseDown(true);

	if (isPropagation)
		return;

	pressed = true;
	SSUI::pressedElement = this;
}

void GUIElement::onMouseUp() {
}

void GUIElement::onHover() {
	hovered = true;

	if (cursorShape)
		Main::setCursor(cursorShape);
}

void GUIElement::onUnhover() {
	hovered = false;

	if (cursorShape)
		Main::setCursor();
}

void GUIElement::onClicked() {
}

GUIElement::~GUIElement() {
	PRINT("delete");

	// Delete all children.
	std::vector<GUIElement*> copy = children;
	for (GUIElement* child : copy)
		delete(child);

	// Remove this from parent's children.
	vErase(parent->children, this);
}