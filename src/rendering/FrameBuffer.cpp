#include "FrameBuffer.h"
#include "Renderer.h"
#include <glm/gtc/matrix_transform.hpp>
#include "../Main.h"


// Atlas FBO's don't delete the texture when deleted.
FrameBuffer::FrameBuffer(unsigned int _width, unsigned int _height, bool _isAtlas) {

    if (!_width || !_height) {
        width = Main::width;
        height = Main::height;
    } else {
        width = _width;
        height = _height;
    }

    isAtlas = _isAtlas;

    glGenFramebuffers(1, &ID); // bad
    glBindFramebuffer(GL_FRAMEBUFFER, ID);
    glGenTextures(1, &texture);
    glBindTexture(GL_TEXTURE_2D, texture);

    glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, width, height, 0, GL_RGBA, GL_UNSIGNED_BYTE, NULL);

    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);

    glFramebufferTexture2D(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, GL_TEXTURE_2D, texture, 0); // This cannot be deleted? Causing memory leak on repeat creations.

    if (glCheckFramebufferStatus(GL_FRAMEBUFFER) != GL_FRAMEBUFFER_COMPLETE)
        PRINT_ERROR("Framebuffer is not complete!");

    glViewport(0, 0, width, height);

    // Needed?
    glDisable(GL_DEPTH_TEST);
    glClearColor(0.0f, 0.0f, 0.0f, 0.0f);
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
}

void FrameBuffer::setAsTarget() { //unsigned int* fbo, unsigned int* texture, unsigned int width, unsigned int height) {
    glBindFramebuffer(GL_FRAMEBUFFER, ID);
}

void FrameBuffer::setAsSource() { //unsigned int* fbo, unsigned int* texture, unsigned int width, unsigned int height) {
    glActiveTexture(GL_TEXTURE0);
    glBindTexture(GL_TEXTURE_2D, texture);
}

// Similar to above.
void FrameBuffer::switchToMain() {
    glViewport(0, 0, Main::width, Main::height); // Needs setting for non-standard sizes and then back again after.
    glBindFramebuffer(GL_FRAMEBUFFER, 0);
}

FrameBuffer::~FrameBuffer() {
    glBindFramebuffer(GL_FRAMEBUFFER, ID);  // Does anything?

    glBindTexture(GL_TEXTURE_2D, texture);  // Does anything?
    glFramebufferTexture2D(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, GL_TEXTURE_2D, 0, 0);

    if (!isAtlas) {
        glDeleteTextures(1, &texture);  // Does anything?
    }

    glDeleteFramebuffers(1, &ID); // Does anything?
}

void FrameBuffer::createShader(Shader* shader, std::string shaderName, unsigned int width, unsigned int height) {
    std::string vertexShader = "assets/shaders/" + shaderName + ".vs";
    std::string fragmentShader = "assets/shaders/" + shaderName + ".fs";

    if (!width || !height) {
        width = Main::width;
        height = Main::height;
    }

    // Set up FBO shader
    (*shader).generate(vertexShader.c_str(), fragmentShader.c_str());
    (*shader).use();
    glm::mat4 projection2 = glm::ortho(0.0f, (float)width, (float)height, 0.0f, -500.0f, 1000.0f);
    glm::mat4 view = glm::lookAt(glm::vec3(), glm::vec3(0.0f, 0.0f, -1.0f), glm::vec3(0.0f, 1.0f, 0.0f));
    (*shader).setMat4("projection", projection2);
    (*shader).setMat4("view", view);
}

void FrameBuffer::init() {
    glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
    glEnable(GL_BLEND);
}