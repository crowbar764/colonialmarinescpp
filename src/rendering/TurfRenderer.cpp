#include "TurfRenderer.h"
#include "Renderer.h"
#include "../global/Camera.h"
#include "../global/World.h"
#include "FrameBuffer.h"
#include "../common/Directions.h"
#include "../Main.h"
#include "../global/Atlas.h"
#include "../atom/turf/Turf.h"

//#include <bitset>
//#include <glm/gtx/string_cast.hpp>

int pixelOfScreenBottom, pixelOfScreenTop, pixelOfScreenRight, pixelOfScreenLeft;

int turfsRedrawn;

std::vector<Turf*> TurfRenderer::invalidTurfs;

void TurfRenderer::draw() {
	// If nothing changed, don't redraw.
	
	if (!Camera::directionsMoved && !BIT_CHECK(Renderer::renderFlags, RENDER_REDRAW_TURF) && !invalidTurfs.size()) {
		return;
	}

	int subGridOffsetX = Camera::X - ((Camera::X / (int)Renderer::tileRes) * (int) Renderer::tileRes);
	int subGridOffsetY = Camera::Y - ((Camera::Y / (int)Renderer::tileRes) * (int) Renderer::tileRes);

	// Do we need to render an extra tile for sub-grid movements?
	bool xBufferTileNeeded = calculateBuffer(Main::width, Camera::X, Camera::xLimit, subGridOffsetX);
	bool yBufferTileNeeded = calculateBuffer(Main::height, Camera::Y, Camera::yLimit, subGridOffsetY);

	turfsRedrawn = 0;

	Renderer::visibleXTiles = (int)std::ceil(Main::width / Renderer::tileRes) + xBufferTileNeeded;
	Renderer::visibleYTiles = (int)std::ceil(Main::height / Renderer::tileRes) + yBufferTileNeeded;
	pixelOfScreenBottom = Camera::Y + Main::height;
	pixelOfScreenTop = Camera::Y;
	pixelOfScreenRight = Camera::X + Main::width;
	pixelOfScreenLeft = Camera::X;
	
	if (BIT_CHECK(Renderer::renderFlags, RENDER_USING_STATIC_DRAW_A)) {
		Renderer::staticDrawFBOA->setAsTarget();
		Renderer::staticDrawFBOB->setAsSource();
	} else {
		Renderer::staticDrawFBOB->setAsTarget();
		Renderer::staticDrawFBOA->setAsSource();
	}

	// Editor worlds might contain BLANK (transparent) turfs so we must clear the background.
	//if ( World::flags & WORLD_EDITOR ) {
		glClearColor(0.0f, 1.0f, 0.0f, 1.0f);
		glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
	//}

	// --- Draw to buffers ---
	if (BIT_CHECK(Renderer::renderFlags, RENDER_REDRAW_TURF)) {
		fullDraw();
		BIT_0(Renderer::renderFlags, RENDER_REDRAW_TURF);
	} else if (Camera::directionsMoved || invalidTurfs.size()) {
		copyBuffer();
		smartDraw();
	}

	invalidTurfs.clear();

	Renderer::drawToBuffer(Atlas::mainAtlas->atlas);

	//std::cout << "Total turfs redrawn: " << turfsRedrawn << "\n";
	BIT_TOGGLE(Renderer::renderFlags, RENDER_USING_STATIC_DRAW_A);
}

void TurfRenderer::smartDraw() {
	//glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);



	//std::cout << "Total tiles: " << visibleXTiles * visibleYTiles << "\n";

	std::vector<int> rowsToCheck;
	std::vector<Turf*> turfToDraw;
	int lastRow = -1;
	
	if (BIT_CHECK(Camera::directionsMoved, DIR_S)) {
		for (int p = Camera::pixelsMovedY; p != 0; --p) {
			int currentRow = (pixelOfScreenBottom - p) / (int) Renderer::tileRes; // This is the pixel of the bottom of the turf

			if (currentRow != lastRow) {
				rowsToCheck.push_back(currentRow);
				lastRow = currentRow;
			}
		}
	} else if (BIT_CHECK(Camera::directionsMoved, DIR_N)) {
		for (int p = Camera::pixelsMovedY; p != -1; --p) {
			int currentRow = (pixelOfScreenTop + p) / (int) Renderer::tileRes; // This is the pixel of the bottom of the turf

			if (currentRow != lastRow) {
				rowsToCheck.push_back(currentRow);
				lastRow = currentRow;
			}
		}
	}

	for (int y : rowsToCheck) {
		for (int x = 0; x < Renderer::visibleXTiles; ++x) {
			turfToDraw.push_back(World::activeWorld->turfs[Camera::X / (int)Renderer::tileRes + x][y]);
		}
	}

	rowsToCheck.clear();
	lastRow = -1;

	if (BIT_CHECK(Camera::directionsMoved, DIR_E)) {
		for (int p = Camera::pixelsMovedX; p != 0; --p) {
			int currentRow = (pixelOfScreenRight - p) / (int) Renderer::tileRes; // This is the pixel of the bottom of the turf

			if (currentRow != lastRow) {
				rowsToCheck.push_back(currentRow);
				lastRow = currentRow;
			}
		}
	}
	else if (BIT_CHECK(Camera::directionsMoved, DIR_W)) {
		for (int p = Camera::pixelsMovedX; p != -1; --p) {
			int currentRow = (pixelOfScreenLeft + p) / (int) Renderer::tileRes; // This is the pixel of the right of the turf

			if (currentRow != lastRow) {
				rowsToCheck.push_back(currentRow);
				lastRow = currentRow;
			}
		}
	}

	for (int x : rowsToCheck) {
		for (int y = 0; y < Renderer::visibleYTiles; ++y) {
			turfToDraw.push_back(World::activeWorld->turfs[x][Camera::Y / (int)Renderer::tileRes + y]);
		}
	}

	turfToDraw.insert(turfToDraw.end(), invalidTurfs.begin(), invalidTurfs.end());

	for (Turf* turf : turfToDraw) {
		
		Renderer::drawSprite(
			turf->position.x * Renderer::tileRes,
			turf->position.y * Renderer::tileRes,
			Renderer::tileRes, Renderer::tileRes,
			turf->sprite->frames[turf->currentFrame]
		);

		++turfsRedrawn;

		
	}
}

////////////////////////////////////////////////////
//              REDRAW ALL TILES                  //
////////////////////////////////////////////////////
void TurfRenderer::fullDraw() {
	for (int y{ 0 }; y < Renderer::visibleYTiles; ++y) {
		for (int x{ 0 }; x < Renderer::visibleXTiles; ++x) {
			Turf* turf = Renderer::turfExists(x, y); // WHY AM I STILL DOING THIS??????? DO I NEED TO????

			Renderer::drawSprite(
				turf->position.x * Renderer::tileRes,
				turf->position.y * Renderer::tileRes,
				Renderer::tileRes, Renderer::tileRes,
				turf->sprite->frames[turf->currentFrame]
			);

			++turfsRedrawn;
		}
	}
}

void TurfRenderer::copyBuffer() {
	// Copy static buffer to other static buffer
	float values[] = {
		(float)-Camera::pixelsMovedXneg,
		(float)-Camera::pixelsMovedYneg,
		0,
		1,

		(float)-Camera::pixelsMovedXneg,
		(float)Main::height - Camera::pixelsMovedYneg,
		0,
		0,

		(float)Main::width - Camera::pixelsMovedXneg,
		(float)-Camera::pixelsMovedYneg,
		1,
		1,

		(float)Main::width - Camera::pixelsMovedXneg,
		(float)-Camera::pixelsMovedYneg,
		1,
		1,

		(float)-Camera::pixelsMovedXneg,
		(float)Main::height - Camera::pixelsMovedYneg,
		0,
		0,

		(float)Main::width - Camera::pixelsMovedXneg,
		(float)Main::height - Camera::pixelsMovedYneg,
		1,
		0,
	};

	Renderer::addToDrawQueue(values, 24);

	Renderer::drawToBuffer();
}

/// <summary>Sets subGridOffsetX or Y based on if an extra tile needs to be drawn</summary>
/// <param name="screenDimension">The screen resolution, e.g. 1920</param>
/// <param name="cameraPos">Location of camera</param>
/// <param name="cameraLimit">How many pixels the camera is allowed to move</param>
/// <param name="subGridOffset">Render pixel offset</param>
/// <returns>bool: Whether an extra tile draw is needed</returns> 
bool TurfRenderer::calculateBuffer(int screenDimension, int cameraPos, int cameraLimit, int subGridOffset) {
	if (!subGridOffset) {
		return false;
	}

	float fullTilesCanFit = screenDimension / Renderer::tileRes; // How many full tiles fit on screen from current tileRes
	bool isNeatDivision = floor(fullTilesCanFit) == fullTilesCanFit; // Check if a half-tile is drawn with current tileRes

	if (!isNeatDivision) {
		int fullTilesCanRender = (int)std::floor(screenDimension / Renderer::tileRes); // How many full tiles can fit in the screen width?
		int halfTilePixels = (int)Renderer::tileRes - (screenDimension - ((int) Renderer::tileRes * fullTilesCanRender)); // How many pixels are left to reach the end of screen?
		int distanceToEnd = cameraLimit - cameraPos; // Distance from Camera end to the end of world

		if (distanceToEnd < halfTilePixels) {
			return false;
		}
	}

	return true;
}