#pragma once

#include <vector>

class Turf;

class TurfRenderer
{
public:
	static std::vector<Turf*> invalidTurfs;

	static void draw();
	
private:
	static bool calculateBuffer(int, int, int, int);
	static void smartDraw();
	static void fullDraw();
	static void copyBuffer();
};