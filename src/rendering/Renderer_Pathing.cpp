#pragma once
#include "Renderer.h"
#include "../subsystems/Master.h"
#include "../global/Camera.h"
#include "../global/World.h"
#include "../ai/pathing/Flow_Pathing.h"
#include "../io/SSSelect.h"
#include "../atom/entity/mob/Mob.h"
#include "../common/IntPair.h"
#include "../common/Directions.h"
#include "../gui/font/SSText.h"
#include "../atom/turf/Turf.h"
#include "../global/Atlas.h"
#include "../ai/pathing/PathPackage.h"

void Renderer::renderPathing() {
	// CLEAR PREVIOUS TEXTS
	for ( Text* text : pathingTexts )
		SSText::deleteText(text);
	pathingTexts.clear();

	//renderPathing_Windows();
	renderPathing_ChunkOutlines();
	renderPathing_Flowfields();

	drawToBuffer(Atlas::mainAtlas->atlas);

	//renderPathing_Networks();
}

void Renderer::renderPathing_ChunkOutlines() {
	TexCoords* tex = Atlas::mainAtlas->getSprite(SPRITE_SELECT_THIN)->frames[0];
	for ( IntPair visibleChunk : Camera::visibleChunks ) {
		//if ( !Flow_Pathing::pathChunks[visibleChunk.x][visibleChunk.y] )
		//	continue;

		drawSprite(
			visibleChunk.x * tileRes * CHUNK_SIZE,
			visibleChunk.y * tileRes * CHUNK_SIZE,
			CHUNK_SIZE * tileRes, CHUNK_SIZE * tileRes,
			tex
		);
	}
}

void Renderer::renderPathing_Windows() {
	TexCoords* tex = Atlas::mainAtlas->getSprite(SPRITE_DEBUG)->frames[0];
	for ( IntPair visibleChunk : Camera::visibleChunks ) {
		Chunk* chunkObj = World::activeWorld->chunks[visibleChunk.x][visibleChunk.y];
		std::unordered_map<int, Flow_Window*> windows = chunkObj->windows;

		for ( std::pair<int, Flow_Window*> windowPair : windows ) {
			Flow_Window* window = windowPair.second;

			glm::vec4 windowDimensions = {
				window->tiles.front().x,
				window->tiles.front().y,
				window->tiles.back().x - window->tiles.front().x + 1,
				window->tiles.back().y - window->tiles.front().y + 1
			};

			// Windows
			drawSprite(
				((windowDimensions.x + (visibleChunk.x * CHUNK_SIZE)) * tileRes),
				((windowDimensions.y + (visibleChunk.y * CHUNK_SIZE)) * tileRes),
				windowDimensions.z * tileRes,
				windowDimensions.w * tileRes,
				tex
			);

			// Number of window connections.
			std::string label = std::to_string(window->connections.size());
			Text* text = SSText::newText(
				label,
				windowDimensions.x + ((windowDimensions.z - .3f) / 2) + (visibleChunk.x * CHUNK_SIZE),
				windowDimensions.y + ((windowDimensions.w - .5f) / 2) + (visibleChunk.y * CHUNK_SIZE),
				.01f,
				glm::vec4(1, 1, 1, 1),
				false
			);
			pathingTexts.push_back(text);
		}
	}
}

void Renderer::renderPathing_Flowfields() {
	PathPackage* package = NULL;

	for ( Entity* selectedEntity : SSSelect::selectedEntities ) {
		Mob* mob = dynamic_cast<Mob*>(selectedEntity);
		if ( mob != nullptr ) {
			if ( mob->movePath ) {
				package = mob->movePath;
				break;
			}
		}
	}

	if ( !package )
		return;

	TexCoords* arrowTexture = Atlas::mainAtlas->getSprite(SPRITE_ARROW)->frames[0];
	TexCoords* losTexture = Atlas::mainAtlas->getSprite(SPRITE_LOS)->frames[0];
	TexCoords* blockedTexture = Atlas::mainAtlas->getSprite(SPRITE_BLANK)->frames[0];
	TexCoords* tropheyTexture = Atlas::mainAtlas->getSprite(SPRITE_TROPHEY)->frames[0];

	for ( int y = 0; y < visibleYTiles; y++ ) {
		for ( int x = 0; x < visibleXTiles; x++ ) {
			IntPair worldGrid = gridToWorldGrid(x, y);

			uint8_t direction = package->flowfield[worldGrid.x][worldGrid.y];
			if ( direction == FLOW_NONE )
				continue;

			TexCoords* sprite;
			float rotation = 0.0f;

			switch ( direction ) {
			case FLOWFIELD_LOS:
				sprite = losTexture;
				break;
			case FLOW_BLOCKED:
				sprite = blockedTexture;
				break;
			case FLOW_GOAL:
				sprite = tropheyTexture;
				break;
			default:
				sprite = arrowTexture;
				rotation = DIR_TABLE_RADIANS[direction];
			}

			drawSprite(
				worldGrid.x * tileRes,
				worldGrid.y * tileRes,
				tileRes, tileRes,
				sprite,
				rotation
			);
		}
	}
}

void Renderer::renderPathing_Networks() {
	Renderer::shapeShader.use();
	glBindVertexArray(Renderer::shapeVAO);
	glBindBuffer(GL_ARRAY_BUFFER, Renderer::shapeVBO);

	glm::vec4 const DEBUG_COLOUR_TABLE[8] = {
		{ 0, 0, 0, 0 },
		{ .75, 0, .25, .15 },
		{ 0, .5, 0, .15 },
		{ 0, .25, .5, .15 },
		{ 1, 0, 0, .15 },
		{ 0, 1, 0, .15 },
		{ 0, 0, 1, .15 },
		{ 1, 1, 1, .15 },
	};

	for ( int y { 0 }; y < Renderer::visibleYTiles; ++y ) {
		for ( int x { 0 }; x < Renderer::visibleXTiles; ++x ) {
			Turf* turf = Renderer::turfExists(x, y); // WHY AM I STILL DOING THIS??????? DO I NEED TO????
			if ( !turf->pathNetworkID )
				continue;

			Renderer::drawShape(
				(turf->position.x * tileRes) - Camera::X,
				(turf->position.y * tileRes) - Camera::Y,
				tileRes, tileRes,
				DEBUG_COLOUR_TABLE[turf->pathNetworkID]
			);
		}
	}

	drawToBuffer(NULL, VERTEX_PAYLOAD_SHAPE);

	// Set back to regular.
	ourShader.use();
	glBindVertexArray(VAO);
	glBindBuffer(GL_ARRAY_BUFFER, VBO);
}