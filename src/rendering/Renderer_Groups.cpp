#pragma once
#include "Renderer.h"
#include "../subsystems/Master.h"
#include "../global/Camera.h"
#include "../ai/pathing/Flow_Pathing.h"
#include "../global/Atlas.h"
#include "../common/DefinesSprites.h"
#include "../ai/pathing/PathPackage.h"
#include "../subsystems/SSPathing.h"

void Renderer::renderPathingGroups() {
	TexCoords* tex;
	TexCoords* white = Atlas::mainAtlas->getSprite(SPRITE_DEBUG_OUTLINE_WHITE)->frames[0];
	TexCoords* red = Atlas::mainAtlas->getSprite(SPRITE_DEBUG_OUTLINE_RED)->frames[0];

	for ( PathPackage* package : Master::SSPathing->activePaths ) {

		// Don't render group shit for single unit movements.
		if ( package->singleUnitMode )
			continue;

		// Draw individual slots.
		for ( PathPackageSlot* slot : package->slots ) {
			if ( slot->valid )
				tex = white;
			else
				tex = red;

			drawSprite(
				(package->formationPosition.x + slot->offset.x - (slot->size / 2.0f)) * tileRes,
				(package->formationPosition.y + slot->offset.y - (slot->size / 2.0f)) * tileRes,
				tileRes, tileRes,
				tex,
				package->groupRotation
			);
		}

		// Draw pilot.
		tex = Atlas::mainAtlas->getSprite(SPRITE_DEBUG_OUTLINE_PINK)->frames[0];
		drawSprite(
			(package->formationPosition.x - UNITS_PER_TILE_HALF) * tileRes,
			(package->formationPosition.y - UNITS_PER_TILE_HALF) * tileRes,
			tileRes, tileRes,
			tex,
			package->groupRotation
		);

		// Draw pilot direction (doesn't work rn).
		//tex = Atlas::mainAtlas->getSprite(SPRITE_DEBUG_LINE)->frames[0];
		//drawSprite(
		//	(package->formationPosition.x * tileRes) - Camera::X,
		//	(package->formationPosition.y * tileRes) - Camera::Y,
		//	tileRes, tileRes,
		//	tex
		//);
	}

	drawToBuffer(Atlas::mainAtlas->atlas);
}