#pragma once

#include "../common/Common.h"
#include "shader.h"
#include "../common/DefinesColours.h"

struct Flow_Window;
class PathPackage;
class Marker;
class Text;
struct IntPair;
class Turf;
class FrameBuffer;

class Renderer
{
	public:
		inline static float tileRes; // How many pixels each sprite should use. e.g. 64. Useful for converting world locations to screen locations.
		inline static Shader ourShader;
		inline static Shader shapeShader;
		inline static unsigned int VBO, VAO;
		inline static unsigned int shapeVBO, shapeVAO;

		inline static float drawBufferMain[RENDERER_MAX_VALUES], drawBufferClickmask[RENDERER_MAX_VALUES], drawBufferShapes[RENDERER_MAX_VALUES], drawBufferText[RENDERER_MAX_VALUES], drawBufferLines[RENDERER_MAX_VALUES];
		inline static float* activeDrawBuffer;
		inline static unsigned int drawBufferCounterMain, drawBufferCounterClickmask, drawBufferCounterShapes, drawBufferCounterText, drawBufferCounterLines;
		inline static unsigned int* drawBufferCounterActive;
		inline static unsigned int mostValues;

		inline static int visibleXTiles, visibleYTiles;
		inline static int renderFlags;

		inline static int drawCalls;
		inline static int aNumber;
		inline static std::vector<Text*> debugTexts;
		inline static std::vector<Marker*> frameMarkers; // Debug markers that last one frame before being deleted.
		inline static std::unordered_multimap<std::string, DebugLine*> lines;

		static void drawToBuffer(unsigned int = 0, int = VERTEX_PAYLOAD_SPRITE, int drawMode = GL_TRIANGLES);
		static void addToDrawQueue(float[], int);
		static void drawWorld();
		static void drawSprite(float, float, float, float, TexCoords*, float = 0.0f, bool isGUI = false);
		static void drawShape(float, float, float, float, glm::vec4);
		static void drawLine(glm::vec2 pointA, glm::vec2 pointB, glm::vec4 colour = COLOUR_RED, bool worldSpace = true);
		static void draw();
		static void RenderToScreen(glm::vec4, unsigned int = 0);
		static IntPair gridToWorldGrid(int, int);
		static Turf* turfExists(int, int);
		static void nslice(Sprite9* sprite, float x, float y, float w, float h);
		static void idk(float x, float y, float w, float h, float tx, float ty, float tx2, float ty2);
		static void setTileRes(float);
		static void toggle();
		static void init();

		inline static FrameBuffer* staticDrawFBOA;
		inline static FrameBuffer* staticDrawFBOB;
		inline static FrameBuffer* worldFBO;

	private:

		inline static std::vector<Text*> pathingTexts; // Temp hack.
		inline static float spriteWidth, spriteHeight; // Because rotating non-standard sizes requires the width and height supplied, and this isn't easily available through params.
		static void drawVertex(float, float, float, float, float, float, float);
		static glm::vec2 rotateVertex(glm::vec2, float);
		static void drawLines();
		static void renderPathing();
		static void renderPathing_ChunkOutlines();
		static void renderPathing_Windows();
		static void renderPathing_Networks();
		static void renderPathing_Flowfields();

		static void renderPathingGroups();

		static void renderMisc();
};