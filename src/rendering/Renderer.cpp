#include "Renderer.h"
#include "FrameBuffer.h"
#include "TurfRenderer.h"
#include "../ai/AStar.h"
#include "../global/Camera.h"
#include "../global/World.h"
#include "../lighting/SSLighting.h"
#include "../io/SSSelect.h"
#include "../io/ClickMask.h"
#include "../gui/SSUI.h"
#include "../Main.h"
#include "../other/Gamemode_Editor.h"
#include "../subsystems/Master.h"
#include "../subsystems/SSEffect.h"
#include "../global/Atlas.h"
#include "../gui/font/SSText.h"
#include "../atom/entity/Entity.h"
#include "../atom/turf/Turf.h"
//#include "../common/DefinesColours.h"

/*
	How to draw:
 
	Step 1: Add lots of vertex data to queue
		Make sure correct vertex queues are active, e.g:
			Renderer::activeDrawBuffer = Renderer::drawBufferLines;
			Renderer::drawBufferCounterActive = &Renderer::drawBufferCounterLines;

		Add vertex data, in this case x, y, r, g, b, a
			float values[] = {
				10.0f * tileRes - Camera::X, 10.0f * tileRes - Camera::Y, 1.0f, 0, 0, 1.0f,
				20.0f * tileRes - Camera::X, 20.0f * tileRes - Camera::Y, 1.0f, 0, 0, 1.0f
			};
			addToDrawQueue(values, 12);

		Reset after (maybe)
			Renderer::activeDrawBuffer = Renderer::drawBufferMain;
			Renderer::drawBufferCounterActive = &Renderer::drawBufferCounterMain;

	Step 2: When all batch data is added, prepare shader
		Renderer::shapeShader.use();
		glBindVertexArray(Renderer::shapeVAO);
		glBindBuffer(GL_ARRAY_BUFFER, Renderer::shapeVBO);

		(if queue was reset earlier, make sure it is correct one)
			Renderer::activeDrawBuffer = Renderer::drawBufferLines;
			Renderer::drawBufferCounterActive = &Renderer::drawBufferCounterLines;

	Step 3: Draw
		Renderer::drawToBuffer(NULL, VERTEX_PAYLOAD_SHAPE, GL_LINES);

	Step 4: Reset shader
		ourShader.use();
		glBindVertexArray(VAO);
		glBindBuffer(GL_ARRAY_BUFFER, VBO);
*/

bool doOnce = false;
int number;

//#define DEBUG_LOCS_CHUNK
//#define DEBUG_LOCS_TURF

void Renderer::draw() {
	if (!BIT_CHECK(renderFlags, RENDER_MASTER))
		return;



	//drawCalls = 0;

	//if (BIT_CHECK(renderFlags, RENDER_ASTAR))
		//AStar::findPath(IntPair{ 5, 5 }, targetPoint);

	if (Master::gameState == GAMESTATE_IN_WORLD)
		drawWorld();

	renderMisc();
	drawLines();	

	SSUI::draw();
	SSText::draw();

	//PRINT("Draws: " << drawCalls);
}

void Renderer::drawWorld() {
	TurfRenderer::draw();

	// DRAW WORLD
	// ----------

	// Switch target to World FBO.
	worldFBO->setAsTarget();

	// Decide which turf FBO to use and draw turfs to World FBO.
	unsigned int textureToDraw = staticDrawFBOA->texture;
	if ( BIT_CHECK(renderFlags, RENDER_USING_STATIC_DRAW_A) )
		textureToDraw = staticDrawFBOB->texture;
	RenderToScreen(glm::vec4 { 0, 0, Main::width, Main::height }, textureToDraw);

	// Chunks test
	Sprite* chunksprite = Atlas::mainAtlas->getSprite(SPRITE_DEBUG);
	int chunkScreenSize = (int) (CHUNK_SIZE * tileRes);

	// Draw entities to World FBO.
	for ( IntPair chunk : Camera::visibleChunks ) {

		// Chunk visualisation.
		#ifdef DEBUG_LOCS_CHUNK
			if ( World::activeWorld->chunks[chunk.x][chunk.y]->occupants.size() ) {
				//Sprite* chunksprite = Atlas::mainAtlas->getSprite(SPRITE_DEBUG);
				int chunkScreenSize = (int)(CHUNK_SIZE * tileRes);
				drawSprite(
					(float) chunk.x * chunkScreenSize,
					(float) chunk.y * chunkScreenSize,
					tileRes * CHUNK_SIZE, tileRes * CHUNK_SIZE,
					chunksprite->frames[0]
				);
			}
		#endif // DEBUG_LOCS_CHUNK

		for ( Entity* occupant : World::activeWorld->chunks[chunk.x][chunk.y]->occupants ) {
			// Entity already drawn via another loc.
			if ( occupant->lastDrawn == Main::currentTime )
				continue;
			occupant->lastDrawn = Main::currentTime;

			float spriteRes = occupant->spriteResolution * tileRes;

			glm::vec2 interpolatedPos = occupant->position * Main::magic + occupant->positionOld * (1.0f - Main::magic);

			// Draw sprite.
			drawSprite(
				(interpolatedPos.x - (occupant->spriteResolution * .5f)) * tileRes,
				(interpolatedPos.y - (occupant->spriteResolution * .5f)) * tileRes,
				spriteRes, spriteRes,
				occupant->sprite->frames[occupant->currentFrame],
				occupant->rotation
			);

			//Sprite* interp = Atlas::mainAtlas->getSprite(SPRITE_DEBUG_POINT);

			//drawSprite(
			//	(occupant->position.x - (occupant->spriteResolution * .5f)) * tileRes,
			//	(occupant->position.y - (occupant->spriteResolution * .5f)) * tileRes,
			//	spriteRes, spriteRes,
			//	interp->frames[0],
			//	PI
			//);

			for ( std::pair<short, Sprite*> overlay : occupant->overlays ) {
				drawSprite(
					(occupant->position.x - (occupant->spriteResolution * .5f)) * tileRes,
					(occupant->position.y - (occupant->spriteResolution * .5f)) * tileRes,
					spriteRes, spriteRes,
					overlay.second->frames[0],
					occupant->rotation
				);
			}

			// Switch to Clickmask vertex array.
			activeDrawBuffer = drawBufferClickmask;
			drawBufferCounterActive = &drawBufferCounterClickmask;
			glm::vec4 entityID = ClickMask::intToRBGA(occupant->ID);

			// Draw Clickmask.
			drawShape(
				((occupant->position.x - (occupant->spriteResolution * .5f)) * tileRes) - (Camera::X),
				((occupant->position.y - (occupant->spriteResolution * .5f)) * tileRes) - (Camera::Y),
				spriteRes, spriteRes,
				entityID
			);

			activeDrawBuffer = drawBufferMain;
			drawBufferCounterActive = &drawBufferCounterMain;
		}
	}

	
	#ifdef DEBUG_LOCS_TURF
		for ( int y { 0 }; y < visibleYTiles; ++y ) {
			int worldGridY = y + (Camera::Y / (int)tileRes);
			for ( int x { 0 }; x < visibleXTiles; ++x ) {
				int worldGridX = x + (Camera::X / (int) tileRes);
				Turf* turf = World::activeWorld->turfs[worldGridX][worldGridY];

				if ( !turf->hasOccupants )
					continue;

				// DEBUG: OCCUPANT VIEWER.
				Sprite* sprite = Atlas::mainAtlas->getSprite(SPRITE_BLANK);
				Renderer::drawSprite(
					turf->position.x * Renderer::tileRes,
					turf->position.y * Renderer::tileRes,
					tileRes, tileRes,
					sprite->frames[0]
				);
			}
		}
	#endif // DEBUG_LOCS_TURF

		// DEBUG MARKED
		for ( int y { 0 }; y < visibleYTiles; ++y ) {
			int worldGridY = y + (int) (Camera::Y / tileRes);
			for ( int x { 0 }; x < visibleXTiles; ++x ) {
				int worldGridX = x + (int) (Camera::X / tileRes);
				Turf* turf = World::activeWorld->turfs[worldGridX][worldGridY];

				if ( !turf->debugMarked )
					continue;

				// DEBUG: OCCUPANT VIEWER.
				Sprite* sprite = Atlas::mainAtlas->getSprite(SPRITE_BLANK);
				Renderer::drawSprite(
					turf->position.x * Renderer::tileRes,
					turf->position.y * Renderer::tileRes,
					tileRes, tileRes,
					sprite->frames[0]
				);
			}
		}

	// effects
	for ( std::pair<short, std::shared_ptr<Effect>> effectRef : SSEffect::effects ) {
		Effect* effect = effectRef.second.get();

		if ( !effect->shouldDraw || effect->world != World::activeWorld )
			continue;

		float resolution = effect->spriteResolution * tileRes;

		drawSprite(
			effect->position.x * tileRes,
			effect->position.y * tileRes,
			resolution, resolution,
			effect->sprite->frames[effect->currentFrame],
			effect->rotation
		);
	}

	// Draw entities to World FBO.
	drawToBuffer(Atlas::mainAtlas->atlas);

	//activeDrawBuffer = drawBufferMain;

	// LIGHTING
	// --------
	if ( BIT_CHECK(renderFlags, RENDER_LIGHTING) ) {
		SSLighting::draw(); // Draw lightmap.

		// Set shader's texture1 to be the lightmap.
		glActiveTexture(GL_TEXTURE1);
		glBindTexture(GL_TEXTURE_2D, SSLighting::lightFBO->texture);

		ourShader.setBool("useLighting", true);
	}

	// RENDER WORLD TO SCREEN
	FrameBuffer::switchToMain();
	RenderToScreen(glm::vec4 {
		0,
		0,
		Main::width,
		Main::height },
		worldFBO->texture
	);

	if ( BIT_CHECK(renderFlags, RENDER_LIGHTING) )
		ourShader.setBool("useLighting", false);

	// DEBUG: Draw lightmap in corner
	RenderToScreen(glm::vec4 {
		(Main::width - 256) - Main::width / 8,
		0,
		(Main::width - 256),
		Main::height / 8 },
		SSLighting::lightFBO->texture
	);

	// DEBUG: Draw atlas.
	const float drawSize = 256.0f;
	RenderToScreen(glm::vec4 {
		Main::width - drawSize,
		0,
		Main::width,
		drawSize },
		Atlas::mainAtlas->atlas
	);

	// DEBUG: Draw font atlas.
	//RenderToScreen(glm::vec4{
	//	0,
	//	0,
	//	256,
	//	256 },
	//	SSText::atlas
	//);

	// Draw selection.
	TexCoords* tex = Atlas::mainAtlas->getSprite(SPRITE_SELECT)->frames[0];
	for ( Entity* entity : SSSelect::selectedEntities ) {
		float spriteRes = entity->spriteResolution * tileRes;
		drawSprite(
			(entity->position.x - (entity->spriteResolution * .5f)) * tileRes,
			(entity->position.y - (entity->spriteResolution * .5f)) * tileRes,
			spriteRes, spriteRes,
			tex
		);
	}
	drawToBuffer(Atlas::mainAtlas->atlas);

	// Draw construction ghost.
	if ( BIT_CHECK(Gamemode_Editor::constructionFlags, CONSTR_GHOST_ENABLED) ) {
		ourShader.setFloat("opacity", 0.5f);
		Gamemode_Editor::drawGhost();
		ourShader.setFloat("opacity", 1.0f);
	}

	//PRINT(AStar::paths.size());
	if ( BIT_CHECK(renderFlags, RENDER_ASTAR) )
		AStar::draw();

	renderPathing();
	renderPathingGroups();

	SSSelect::drawSelectBox();

	// CLICKMASK
	// ---------
	ClickMask::createMask();

	// DEBUG: Draw clickmask.
	if ( BIT_CHECK(renderFlags, RENDER_CLICKMASK) )
		RenderToScreen(glm::vec4 {
		0,
		0,
		Main::width,
		Main::height },
		ClickMask::maskFBO->texture);

	
}

void Renderer::RenderToScreen(glm::vec4 position, unsigned int textureToDraw) {

	float values[] = {
		position.x, position.y, 0, 1,
		position.x, position.w, 0, 0,
		position.z, position.y, 1, 1,
		position.z, position.y, 1, 1,
		position.x, position.w, 0, 0,
		position.z, position.w, 1, 0
	};

	addToDrawQueue(values, 24);

	drawToBuffer(textureToDraw);
}

void Renderer::addToDrawQueue(float floatsToAdd[], int numOfValues) {

	std::memcpy(activeDrawBuffer + (*drawBufferCounterActive), floatsToAdd, numOfValues * 4);
	(*drawBufferCounterActive) += numOfValues;

	// This was 17% compared to above 3%. 1000 Human sprite test.
	//for (int i = 0; i < numOfValues; ++i)
		//activeDrawBuffer[(*drawBufferCounterActive)++] = floatsToAdd[i];
}

/// <summary>Uses the toDraw queue to draw to the framebuffer</summary>
/// <param name="textureToDraw">[optional] The texture to draw</param>
/// <param name="floatsPerVertex">[optional] How much data per vertex. E.G. (x, y, z, uvU, uvY) is 5. (x, y, z, r, g, b, a) is 7</param>
void Renderer::drawToBuffer(unsigned int textureToDraw, int floatsPerVertex, int drawMode) {
	if (!(*drawBufferCounterActive))
		return;

	if (textureToDraw)
		glBindTexture(GL_TEXTURE_2D, textureToDraw);

	if (*drawBufferCounterActive > mostValues) {
		mostValues = *drawBufferCounterActive;
		//std::cout << "Top draw floats: " << mostValues << ".\n";
	}

	
	glBufferData(GL_ARRAY_BUFFER, (*drawBufferCounterActive) * sizeof(float), activeDrawBuffer, GL_STREAM_DRAW);
	glDrawArrays(drawMode, 0, (*drawBufferCounterActive) / floatsPerVertex);
	*drawBufferCounterActive = 0;

	drawCalls++;
}

// ROTATION IN RADIANS
void Renderer::drawSprite(
	float x, float y,
	float w, float h,
	TexCoords* spriteLabel,
	float rotation,
	bool isGUI
) {
	if ( rotation ) {
		spriteWidth = w;
		spriteHeight = h;
	}

	if ( !isGUI ) {
		x -= Camera::X;
		y -= Camera::Y;
	}

	drawVertex(x, y, 0, 0, rotation, spriteLabel->x, spriteLabel->y);
	drawVertex(x, y, w, 0, rotation, spriteLabel->x2, spriteLabel->y);
	drawVertex(x, y, w, h, rotation, spriteLabel->x2, spriteLabel->y2);
	drawVertex(x, y, w, h, rotation, spriteLabel->x2, spriteLabel->y2);
	drawVertex(x, y, 0, h, rotation, spriteLabel->x, spriteLabel->y2);
	drawVertex(x, y, 0, 0, rotation, spriteLabel->x, spriteLabel->y);
}

// Drawing shapes.
void Renderer::drawShape(
	float x, float y,
	float w, float h,
	glm::vec4 colour
) {

	float x2 = x + w;
	float y2 = y + h;

	float values[] = {
		x,		y,		colour.r, colour.g, colour.b, colour.a,
		x2,		y,		colour.r, colour.g, colour.b, colour.a,
		x2,		y2,		colour.r, colour.g, colour.b, colour.a,
		x2,		y2,		colour.r, colour.g, colour.b, colour.a,
		x,		y2,		colour.r, colour.g, colour.b, colour.a,
		x,		y,		colour.r, colour.g, colour.b, colour.a,
	};

	addToDrawQueue(values, 36);
}

void Renderer::drawVertex(
	float x, float y,
	float w, float h,
	float rotation,
	float texCoordX, float texCoordY
) {
	//rotation = Main::deltaTimeTotal;

	// Get initial very location
	glm::vec2 vertices(w, h);

	//PRINT(w);
	
	// Rotate
	if (rotation)
		vertices = rotateVertex(vertices, rotation);

	// Translate
	vertices.x += x;
	vertices.y += y;

	// Send transformed vert to render queue
	float values[] = {
		vertices.x,
		vertices.y,
		texCoordX,
		texCoordY
	};

	addToDrawQueue(values, 4);
}

glm::vec2 Renderer::rotateVertex(glm::vec2 vertex, float rotation) {
	float halfWidth = spriteWidth * .5f;
	float halfHeight = spriteHeight * .5f;

	float s = sin(rotation);
	float c = cos(rotation);

	// Move to origin.
	vertex.x -= halfWidth;
	vertex.y -= halfHeight;

	// Rotate.
	vertex = {
		vertex.x * c - vertex.y * s,
		vertex.x * s + vertex.y * c
	};

	// Reset to position.
	vertex.x += halfWidth;
	vertex.y += halfHeight;

	return vertex;
}

void Renderer::drawLine(glm::vec2 pointA, glm::vec2 pointB, glm::vec4 colour, bool worldSpace) {
	Renderer::activeDrawBuffer = Renderer::drawBufferLines;
	Renderer::drawBufferCounterActive = &Renderer::drawBufferCounterLines;

	// TODO: Make camera pos a vector?
	glm::vec2 cameraPos = { Camera::X, Camera::Y };

	if ( worldSpace ) {
		pointA = pointA * tileRes - cameraPos;
		pointB = pointB * tileRes - cameraPos;
	}

	float values[] = {
		pointA.x, pointA.y, colour.r, colour.g, colour.b, colour.a,
		pointB.x, pointB.y, colour.r, colour.g, colour.b, colour.a,
	};

	addToDrawQueue(values, 12);

	activeDrawBuffer = drawBufferMain;
	drawBufferCounterActive = &drawBufferCounterMain;
}

// Draw all queued lines.
void Renderer::drawLines() {
	activeDrawBuffer = drawBufferLines;
	drawBufferCounterActive = &drawBufferCounterLines;

	shapeShader.use();
	glBindVertexArray(shapeVAO);
	glBindBuffer(GL_ARRAY_BUFFER, shapeVBO);

	// Draw.
	drawToBuffer(NULL, VERTEX_PAYLOAD_SHAPE, GL_LINES);

	// Reset.
	activeDrawBuffer = drawBufferMain;
	drawBufferCounterActive = &drawBufferCounterMain;
	ourShader.use();
	glBindVertexArray(VAO);
	glBindBuffer(GL_ARRAY_BUFFER, VBO);
}

void Renderer::nslice(Sprite9* sprite, float x, float y, float w, float h) {

	float border = sprite->border;

	// NORTH
	TexCoords* division = sprite->slices[0];

	idk(x + border,
		y,
		w - border * 2,
		border,
		division->x,
		division->y,
		division->x2,
		division->y2);

	// NE
	division = sprite->slices[1];

	idk(x + w - border,
		y,
		border,
		border,
		division->x,
		division->y,
		division->x2,
		division->y2);

	// E
	division = sprite->slices[2];

	idk(x + w - border,
		y + border,
		border,
		h - border * 2,
		division->x, division->y, division->x2, division->y2);

	// SE
	division = sprite->slices[3];

	idk(x + w - border,
		y + h - border,
		border,
		border,
		division->x, division->y, division->x2, division->y2);

	// S
	division = sprite->slices[4];

	idk(x + border,
		y + h - border,
		w - border * 2,
		border,
		division->x, division->y, division->x2, division->y2);

	// SW
	division = sprite->slices[5];

	idk(x,
		y + h - border,
		border,
		border,
		division->x, division->y, division->x2, division->y2);

	// W
	division = sprite->slices[6];

	idk(x,
		y + border,
		border,
		h - border * 2,
		division->x, division->y, division->x2, division->y2);

	// NW
	division = sprite->slices[7];

	idk(x,
		y,
		border,
		border,
		division->x, division->y, division->x2, division->y2);

	// C
	division = sprite->slices[8];

	idk(x + border,
		y + border,
		w - border * 2,
		h - border * 2,
		division->x, division->y, division->x2, division->y2);
}

void Renderer::idk(float x, float y, float w, float h, float tx, float ty, float tx2, float ty2) {
	drawVertex(x, y, 0, 0, 0, tx, ty);
	drawVertex(x, y, w, 0, 0, tx2, ty);
	drawVertex(x, y, w, h, 0, tx2, ty2);
	drawVertex(x, y, w, h, 0, tx2, ty2);
	drawVertex(x, y, 0, h, 0, tx, ty2);
	drawVertex(x, y, 0, 0, 0, tx, ty);
}

IntPair Renderer::gridToWorldGrid(int x, int y) {
	int gridX = x + (int) (Camera::X / tileRes);
	int gridY = y + (int) (Camera::Y / tileRes);

	return IntPair{gridX, gridY};
}

Turf* Renderer::turfExists(int x, int y) {
	IntPair worldGrid = gridToWorldGrid(x, y);

	return World::activeWorld->turfs[worldGrid.x][worldGrid.y];
}

void Renderer::setTileRes(float newTileRes) {
	tileRes = newTileRes;
}

void Renderer::toggle() {
	BIT_TOGGLE(renderFlags, RENDER_MASTER);
	std::cout << "RENDERING: " << BIT_CHECK(renderFlags, RENDER_MASTER) << "\n";
}

void Renderer::init() {
	setTileRes(64.0f);

	BIT_1(renderFlags, RENDER_MASTER);
	BIT_1(renderFlags, RENDER_REDRAW_TURF);
	//BIT_1(renderFlags, RENDER_CLICKMASK);
	BIT_1(renderFlags, RENDER_ASTAR);
	//BIT_1(renderFlags, RENDER_LIGHTING);
	BIT_1(renderFlags, RENDER_ENTITIES);
	BIT_1(renderFlags, RENDER_RECALC_GUI);
	BIT_1(renderFlags, RENDER_REDRAW_GUI);

	// configure global opengl state
	// -----------------------------
	glEnable(GL_DEPTH_TEST);

	// set up vertex data (and buffer(s)) and configure vertex attributes
	// ------------------------------------------------------------------

	// Shape stuff.
	glGenVertexArrays(1, &shapeVAO);
	glGenBuffers(1, &shapeVBO);
	glBindVertexArray(shapeVAO);
	glBindBuffer(GL_ARRAY_BUFFER, shapeVBO);
	// position attribute
	glVertexAttribPointer(0, 2, GL_FLOAT, GL_FALSE, 6 * sizeof(float), (void*)0);
	glEnableVertexAttribArray(0);
	// texture coord attribute
	glVertexAttribPointer(1, 4, GL_FLOAT, GL_FALSE, 6 * sizeof(float), (void*)(2 * sizeof(float)));
	glEnableVertexAttribArray(1);

	// Main structure.
	glGenVertexArrays(1, &VAO);
	glGenBuffers(1, &VBO);
	glBindVertexArray(VAO);
	glBindBuffer(GL_ARRAY_BUFFER, VBO);
	// position attribute
	glVertexAttribPointer(0, 2, GL_FLOAT, GL_FALSE, 4 * sizeof(float), (void*)0);
	glEnableVertexAttribArray(0);
	// texture coord attribute
	glVertexAttribPointer(1, 2, GL_FLOAT, GL_FALSE, 4 * sizeof(float), (void*)(2 * sizeof(float)));
	glEnableVertexAttribArray(1);

	FrameBuffer::createShader(&ourShader, "shaderBasic");
	ourShader.setInt("texture1", 1);

	staticDrawFBOA = new FrameBuffer();
	staticDrawFBOB = new FrameBuffer();
	worldFBO = new FrameBuffer();

	activeDrawBuffer = drawBufferMain;
	drawBufferCounterActive = &drawBufferCounterMain;
}