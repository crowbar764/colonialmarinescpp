#include "Renderer.h"
#include "../global/Atlas.h"
#include "../common/DefinesSprites.h"

void Renderer::renderMisc() {
	// Frame markers.
	TexCoords* tex = Atlas::mainAtlas->getSprite(SPRITE_DEBUG_POINT)->frames[0];
	for ( Marker* frameMarker : frameMarkers ) {

		drawSprite(
			(frameMarker->position.x - .5f) * tileRes,
			(frameMarker->position.y - .5f) * tileRes,
			tileRes, tileRes,
			tex
		);

		delete frameMarker;
	}

	frameMarkers.clear();

	drawToBuffer(Atlas::mainAtlas->atlas);

	// Debug lines.
	// Unordered multimap can stored multiple with the same key.
	for (auto it = lines.begin(); it != lines.end(); it++ ) {
		auto line = it->second;

		drawLine({ line->x0, line->y0 }, {line->x1, line->y1}, line->colour);
	}
}