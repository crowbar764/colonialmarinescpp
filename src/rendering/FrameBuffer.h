#pragma once

#include <string>

class Shader;

class FrameBuffer
{
public:
	unsigned int ID, texture, width, height;
	bool isAtlas;

	FrameBuffer(unsigned int _width = 0, unsigned int _height = 0, bool isAtlas = false);
	void setAsTarget();
	void setAsSource();
	~FrameBuffer();

	static void switchToMain();
	static void createShader(Shader*, std::string, unsigned int = 0, unsigned int = 0);
	static void init();
};