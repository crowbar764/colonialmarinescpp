#include "Timer.h"
#include "../Main.h"
#include "../subsystems/SSTimer.h"

/**
 * A delayed callback.
 * To discard an unwanted timer, just set its 'obsolete' to true.
 *
 * @param duration
 *            - How many SECONDS from now the timer should trigger.
 * @param callback
 *            - The function fired on completion.
 */
Timer::Timer(float duration, std::function<void()> callback) {
	this->callback = callback;
	endTime = Main::currentTime + duration;

	SSTimer::timers.push_back(this);
}