#pragma once
#include <functional>

class Timer {
public:
	float endTime;
	bool obsolete = false;
	std::function<void()> callback;

	Timer(float duration, std::function<void()> callback);
};