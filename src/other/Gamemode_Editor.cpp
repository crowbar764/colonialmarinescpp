#include "Gamemode_Editor.h"
#include "../rendering/Renderer.h"
#include "../global/Camera.h"
#include "../io/Mouse.h"
#include "../global/World.h"
#include "../abstracts/SSRooms.h"
#include "../abstracts/Room_CIC.h"
#include "../common/DefinesSprites.h"
#include "../atom/turf/Turf.h"
#include "../global/Atlas.h"
#include "../gui/font/SSText.h"
#include "../other/Player.h"

int Gamemode_Editor::constructionFlags;
rapidjson::Document* Gamemode_Editor::constructionGhost;

void Gamemode_Editor::drawGhost() {
	int areaWidth = (*constructionGhost)["meta"]["width"].GetInt();
	int areaHeight = (*constructionGhost)["meta"]["height"].GetInt();

	IntPair mousePos = Mouse::getMousePosGrid();
	int xOffset = mousePos.x - (areaWidth / 2);
	int yOffset = mousePos.y - (areaHeight / 2);

	for (int y = yOffset; y < areaHeight + yOffset; ++y) {
		for (int x = xOffset; x < areaWidth + xOffset; ++x) {
			if (!World::activeWorld->gridIsInBounds(x, y))
				continue;

			short sprite = (*constructionGhost)["tiles"][y - yOffset][x - xOffset]["sprite"].GetInt();
			if ( sprite == SPRITE_BLANK)
				continue;

			if (World::activeWorld->turfs[x][y]->isOpen) {
				Renderer::drawSprite(
					x * Renderer::tileRes,
					y * Renderer::tileRes,
					Renderer::tileRes, Renderer::tileRes,
					Atlas::mainAtlas->getSprite(sprite)->frames[0]
				);
			} else {
				Renderer::drawSprite(
					x * Renderer::tileRes,
					y * Renderer::tileRes,
					Renderer::tileRes, Renderer::tileRes,
					Atlas::blankTexCoords->frames[0]
				);
			}
		}
	}

	Renderer::drawToBuffer(Atlas::mainAtlas->atlas);
}

bool Gamemode_Editor::checkBuildClearance() {
	int areaWidth = (*constructionGhost)["meta"]["width"].GetInt();
	int areaHeight = (*constructionGhost)["meta"]["height"].GetInt();
	IntPair mousePos = Mouse::getMousePosGrid();
	int xOffset = mousePos.x - (areaWidth / 2);
	int yOffset = mousePos.y - (areaHeight / 2);

	//For each map cell (and boundry cell), create the cell or boundry.
	for (int y = yOffset; y < areaHeight + yOffset; ++y) {
		for (int x = xOffset; x < areaWidth + xOffset; ++x) {
			if (!World::activeWorld->gridIsInBounds(x, y))
				return false;

			short sprite = (*constructionGhost)["tiles"][y - yOffset][x - xOffset]["sprite"].GetInt();
			if (sprite == SPRITE_BLANK)
				continue;

			if (!World::activeWorld->turfs[x][y]->isOpen)
				return false;
		}
	}

	return true;
}

void Gamemode_Editor::attemptBuild() {
	PRINT("A");
	// Check we can afford.
	if (1000 > Main::player->credits) {
		SSText::newCursorText("Insufficient credits");
		return;
	}

	// Check placement isn't blocked.
	if (!checkBuildClearance()) {
		SSText::newCursorText("Blocked");
		return;
	}

	int areaWidth = (*constructionGhost)["meta"]["width"].GetInt();
	int areaHeight = (*constructionGhost)["meta"]["height"].GetInt();
	IntPair mousePos = Mouse::getMousePosGrid();

	Room* room;
	room = new Room_CIC;
	room->init(mousePos.x - (areaWidth / 2), mousePos.y - (areaHeight / 2));
	room->build();

	Main::player->credits -= 1000;
	SSText::newCursorText("-$1000");
}