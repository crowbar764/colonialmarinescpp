#pragma once
#include <glm/glm.hpp>
#include <vector>
#include <functional>

//	t - time elapsed from start of animation
//	b - start value
//	c - value change
//	d - duration of animation

class Ease {
public:
	inline static std::vector<Ease*> activeEases;
	float duration, startTime, endTime;
	bool isAutomated, toBeDeleted;
	std::function<void()> callback;

	Ease(float duration, bool isAutomated = true, std::function<void()> callback = NULL );

	bool isFinished();

	static void update();

	virtual bool tick() { return false; };

	~Ease();
};

// FLOAT
// -----
class EaseFloat : public Ease {
public:
	float* target;
	float startValue, endValue;

	EaseFloat(float* target, float endValue, float duration, bool isAutomated = true, std::function<void()> callback = NULL);

	float linear(float t);

	virtual bool tick();
};

// VECTOR
// ------
class EaseVector : public Ease {
public:
	glm::vec2* target;
	glm::vec2 startValue, endValue;

	EaseVector(glm::vec2* target, glm::vec2 endValue, float duration, bool isAutomated = true, std::function<void()> callback = NULL);

	glm::vec2 linear(float t);

	virtual bool tick();
};