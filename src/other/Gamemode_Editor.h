#pragma once
#include <rapidjson/document.h>

class Gamemode_Editor {
public:
	inline static int mode = 0;
	static int constructionFlags;
	inline static short currentTurf;
	static rapidjson::Document* constructionGhost;

	static void drawGhost();
	static bool checkBuildClearance();
	static void attemptBuild();
};