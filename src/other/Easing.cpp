#pragma once
#include "Easing.h"
#include "../Main.h"
#include <algorithm>
#include "../common/Common.h"

//	t - time elapsed from start of animation
//	b - start value
//	c - value change
//	d - duration of animation

Ease::Ease(float duration, bool isAutomated, std::function<void()> callback) {
	this->duration = duration;
	this->isAutomated = isAutomated;
	this->callback = callback;
	startTime = Main::currentTime;
	endTime = startTime + duration;

	if (isAutomated)
		activeEases.push_back(this);
}

bool Ease::isFinished() {
	return Main::currentTime > endTime;
}

void Ease::update() {
	std::vector<Ease*> activeEasesCopy = activeEases;

	//PRINT(activeEases.size());

	for ( Ease* ease : activeEasesCopy ) {

		// Was marked for deletion earlier.
		if ( ease->toBeDeleted ) {
			vErase(activeEases, ease);
			delete(ease);
			continue;
		}

		// Returns true if the ease is complete.
		if ( ease->tick() ) {
			
			if ( ease->callback ) {
				ease->callback();
			}

			vErase(activeEases, ease);
			delete(ease);
		}
	}
}

Ease::~Ease() {
	if (isAutomated) {
		//std::vector<Ease*>::iterator it = find(activeEases.begin(), activeEases.end(), this);
		//Common::eraseFast(activeEases, it);
		//vErase(activeEases, this);
	}
}

// FLOAT
// -----
EaseFloat::EaseFloat(float* target, float endValue, float duration, bool isAutomated, std::function<void()> callback) : Ease(duration, isAutomated, callback) {
	this->target = target;
	this->startValue = *target;
	this->endValue = endValue;
}

float EaseFloat::linear(float progress) {
	return startValue + (endValue - startValue) * progress;
}

bool EaseFloat::tick() {
	// 0 - 1
	float progress = (Main::currentTime - startTime) / duration;
	progress = std::clamp(progress, 0.f, 1.f);

	*target = linear(progress);

	return isFinished();
}

// VECTOR
// ------
EaseVector::EaseVector(glm::vec2* target, glm::vec2 endValue, float duration, bool isAutomated, std::function<void()> callback) : Ease(duration, isAutomated, callback) {
	this->target = target;
	this->startValue = *target;
	this->endValue = endValue;
}

glm::vec2 EaseVector::linear(float progress) {
	return glm::vec2(
		startValue.x + (endValue.x - startValue.x) * progress,
		startValue.y + (endValue.y - startValue.y) * progress
	);
}

bool EaseVector::tick() {
	// 0 - 1
	float progress = (Main::currentTime - startTime) / duration;
	progress = std::clamp(progress, 0.f, 1.f);

	*target = linear(progress);

	return isFinished();
}