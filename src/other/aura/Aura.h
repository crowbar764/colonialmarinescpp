#pragma once

#include "../../atom/entity/mob/Mob.h"

class Modifiers;

// CATRGORY.
// Only the strongest of each category can apply at once.
constexpr short AURA_OFFICER = (1 << 0);
//constexpr short AURA_GAS = (1 << 1);

// STRENGTH.
// Which aura takes priority?
constexpr short AURA_OFFICER_XO = 0;
constexpr short AURA_OFFICER_COMMANDER = 1;



class Aura {
public:
	Mob* owner;
	
	short range = 4;
	short category = 0; // CATRGORY.
	short flag = 0; // STRENGTH.

	// Multiplier amount. -0.5 halves, 0 nothing, 0.5 doubles.
	float speed = 0;

	Aura(Mob* owner);
	void applyOn(Modifiers* target);
	~Aura();
};