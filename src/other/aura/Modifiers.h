#pragma once

constexpr short MOD_FLAG_FAST = (1 << 0);
constexpr short MOD_FLAG_SLOW = (1 << 1);

class Modifiers {
public:
	bool disabled = false;
	short flags = 0; // Currently active overlays.
	short immuneTo = 0;

	// Modifiers.
	float speed = 1;

	void reset() {
		speed = 1;
	}
};