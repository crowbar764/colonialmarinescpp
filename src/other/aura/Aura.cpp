#pragma once

#include "SSAura.h"
#include "Modifiers.h"

Aura::Aura(Mob* owner) {
	this->owner = owner;
	SSAura::auras.push_back(this);
}

Aura::~Aura() {
	vErase(SSAura::auras, this);
}

void Aura::applyOn(Modifiers* target) {
	target->speed += speed;
}