#pragma once
#include "../../subsystems/Subsystem.h"
#include "../../subsystems/Master.h"
#include "../../common/Math.h"
#include "Aura.h"
#include "../../global/Atlas.h"

#include <GLFW/glfw3.h>

class SSAura : public Subsystem {
public:

	inline static std::vector<Aura*> auras;
	inline static std::unordered_map<unsigned int, std::shared_ptr<Mob>> mobs;

	SSAura() {
		name = "SSAura";
		fireSpeed = .25f;
		init();
	}

	void fire(bool resumed) {
		//if ( !resumed )
			//mobs = World::mobs;

		while ( mobs.size() ) {
			std::shared_ptr<Mob> mobRef = mobs.begin()->second;

			// Mob deleted.
			if ( !mobRef ) {
				mobs.erase(mobs.begin());
				continue;
			}
				
			Mob* mob = mobRef.get();

			// Mob mods disabled.
			if ( mob->modifiers->disabled ) {
				mobs.erase(mobs.begin());
				continue;
			}

			mob->modifiers->reset();
			std::unordered_map<short, Aura*> aurasToApply;

			// Calculate what effects should be applied.
			for ( Aura* aura : auras ) {
				// If we are already affected by a superior aura of this type..
				if ( aurasToApply.find(aura->category) != aurasToApply.end() && aura->flag <= aurasToApply.at(aura->category)->flag )
					continue;

				if ( mob->modifiers->immuneTo & aura->category )
					continue;

				// Out of range.
				int distance = (int) std::ceil(getVectorDistanceSquare(mob->position, aura->owner->position) / UNITS_PER_TILE);
				if (distance > aura->range)
					continue;

				aurasToApply[aura->category] = aura;
			}

			// Apply all effects.
			for ( std::pair<short, Aura*> category : aurasToApply )
				category.second->applyOn(mob->modifiers);

			updateHUD(mob);

			mobs.erase(mobs.begin());

			if ( TICK_CHECK )
				return;
		}

		inProgress = false;
	}

	void updateHUD(Mob* mob) {
		// Speed.
		if ( mob->modifiers->speed == 1 ) { // If normal.
			if ( mob->modifiers->flags & MOD_FLAG_FAST ) {
				mob->overlays.erase(SPRITE_FAST);
				BIT_0(mob->modifiers->flags, MOD_FLAG_FAST);
			}
			if ( mob->modifiers->flags & MOD_FLAG_SLOW ) {
				mob->overlays.erase(SPRITE_SLOW);
				BIT_0(mob->modifiers->flags, MOD_FLAG_SLOW);
			}
		} else if (mob->modifiers->speed > 1) { // If fast.
			if ( mob->modifiers->flags & MOD_FLAG_SLOW ) {
				mob->overlays.erase(SPRITE_SLOW);
				BIT_0(mob->modifiers->flags, MOD_FLAG_SLOW);
			}

			if ( !(mob->modifiers->flags & MOD_FLAG_FAST) ) {
				mob->overlays.emplace(SPRITE_FAST, Atlas::mainAtlas->getSprite(SPRITE_FAST));
				BIT_1(mob->modifiers->flags, MOD_FLAG_FAST);
			}
		} else { // If slow.
			if ( mob->modifiers->flags & MOD_FLAG_FAST ) {
				mob->overlays.erase(SPRITE_FAST);
				BIT_0(mob->modifiers->flags, MOD_FLAG_FAST);
			}

			if ( !(mob->modifiers->flags & MOD_FLAG_SLOW) ) {
				mob->overlays.emplace(SPRITE_SLOW, Atlas::mainAtlas->getSprite(SPRITE_SLOW));
				BIT_1(mob->modifiers->flags, MOD_FLAG_SLOW);
			}
		}
	}
};