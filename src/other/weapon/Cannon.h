#pragma once
#include "Weapon.h"
#include "../../atom/effect/Projectile.h"

class Cannon : public Weapon {
public:

	Cannon(Mob* owner) : Weapon(owner) {
		shotsPerMag = 1;
		shotsInMag = shotsPerMag;
		reloadTime = 1;
	};

	bool use(std::shared_ptr<Entity> target) {
		if (!Weapon::use(target)) {
			return false;
		}

		//new Projectile(owner->position, 0, target);
		return true;
	}
};