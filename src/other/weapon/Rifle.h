#pragma once
#include "Weapon.h"
#include "../../atom/effect/Projectile.h"

class Rifle : public Weapon {
public:

	Rifle(Mob* owner) : Weapon(owner) {
		shotsPerMag = 100000;
		shotsInMag = shotsPerMag;
		reloadTime = 1;
		chamberTime = 0.1f;
	};

	bool use(std::shared_ptr<Entity> target) {
		if (!Weapon::use(target)) {
			return false;
		}

		new Projectile(owner->world, owner->position, target);
		return true;
	}
};