#pragma once
#include "../../atom/entity/mob/Mob.h"
#include "../../Main.h"

class Weapon {
public:
	Mob* owner;
	int range, shotsPerMag, shotsInMag;
	float nextReady, reloadTime;
	float chamberTime = .1f; // The delay between shots.

	Weapon(Mob* owner) {
		this->owner = owner;
	}

	virtual bool use(std::shared_ptr<Entity> target) {
		// Are we are reloading?
		if (Main::currentTime < nextReady) {
			return false;
		}

		// This is the last bullet in the mag.
		if (!--shotsInMag) {
			nextReady = Main::currentTime + reloadTime;
			shotsInMag = shotsPerMag;
		} else {
			nextReady = Main::currentTime + chamberTime;
		}
		
		return true;
	};
};