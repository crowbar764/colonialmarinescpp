#pragma once
#include "Weapon.h"
#include "../../atom/effect/Projectile.h"

class Autocannon : public Weapon {
public:

	Autocannon(Mob* owner) : Weapon(owner) {
		shotsPerMag = 1;
		shotsInMag = shotsPerMag;
		reloadTime = .33f;
	};

	bool use(std::shared_ptr<Entity> target) {
		if (!Weapon::use(target)) {
			return false;
		}

		//new Projectile(owner->position, 0, target);
		return true;
	}
};