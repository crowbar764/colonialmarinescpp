#pragma once
#include "Weapon.h"

class Claws : public Weapon {
public:

	Claws(Mob* owner) : Weapon(owner) {
		shotsPerMag = 100000;
		shotsInMag = shotsPerMag;
		reloadTime = 1;
		chamberTime = 1;
	};

	bool use(std::shared_ptr<Entity> target) {
		if ( !Weapon::use(target) ) {
			return false;
		}

		PRINT("CLAW");
		target->damage(20);
		return true;
	}
};