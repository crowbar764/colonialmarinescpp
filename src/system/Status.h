#pragma once

#include "System.h"

class Status : public System
{
public:
	static std::vector<unsigned int short> statuses;
	virtual void tick();
	static void init();
};