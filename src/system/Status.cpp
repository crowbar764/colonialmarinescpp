#include "Status.h"

std::vector<unsigned int short> Status::statuses{ NULL }; // Current level of all statuses

void Status::tick() {
	System::tick();

	std::cout << "Status tick\n";
}

void Status::init() {
	System::init();

	for (int i = 1; i < (int)ENUM_status::SIZE; ++i) {//ARRAY STARTS WITH ONE ENTRY SO COUNT FROM 2
		statuses.push_back(0);
	}

	std::cout << "Status init\n";
}