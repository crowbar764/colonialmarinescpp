#include "SSLighting.h"
#include "../Main.h"
#include "../rendering/Renderer.h"
#include "../global/World.h"
#include "../global/Camera.h"
#include "../abstracts/SSRooms.h"
#include "../rendering/FrameBuffer.h"

glm::vec3 SSLighting::ambientLight;

// To light an area to original colour, convert the shader amplifier (4) into a float (.25). Light should set RGB to this value.

void SSLighting::draw() {
	lightFBO->setAsTarget();
	//FrameBuffer::switchToBuffer(&lightingFBO, &lightingTexture);
	Renderer::shapeShader.use();
	glBindVertexArray(Renderer::shapeVAO);
	glBindBuffer(GL_ARRAY_BUFFER, Renderer::shapeVBO);

	// Reset lightmap to 'darkness'.
	glClearColor(ambientLight.r, ambientLight.g, ambientLight.b, 1);
	glClear(GL_COLOR_BUFFER_BIT);

	for (Room* room : SSRooms::rooms) {
		for (glm::vec4 quad : room->quads) {
			Renderer::drawShape(
				(Renderer::tileRes * (quad.x + room->x)) - (Camera::X),
				(Renderer::tileRes * (quad.y + room->y)) - (Camera::Y),
				Renderer::tileRes * quad.z, Renderer::tileRes * quad.w,
				glm::vec4(room->lighting, 1)
			);
		}
	}

	Renderer::drawToBuffer(NULL, VERTEX_PAYLOAD_SHAPE);

	// Set back to regular.
	Renderer::ourShader.use();
	glBindVertexArray(Renderer::VAO);
	glBindBuffer(GL_ARRAY_BUFFER, Renderer::VBO);

	glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA); // needed??
	glBlendEquation(GL_FUNC_ADD);
}

void SSLighting::init() {
	lightFBO = new FrameBuffer();

	ambientLight = glm::vec3(.1, .1, .1);
}