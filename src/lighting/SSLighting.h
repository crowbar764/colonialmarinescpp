#pragma once

#include <glm/glm.hpp>

class FrameBuffer;

class SSLighting {
public:
	inline static FrameBuffer* lightFBO;
	static glm::vec3 ambientLight;

	static void draw();
	static void init();
};