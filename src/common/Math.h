#pragma once

#include <glm/glm.hpp>

#define SET_MAGNITUDE(vec, magnitude) (vec = glm::normalize(vec) * magnitude)
#define GET_DIRECTION(veca, vecb) (vecb - veca);

/// <summary>Makes sure a value is within two other values</summary>
/// <param name="val">Value to clamp</param>
/// <param name="min">Minimum value</param>
/// <param name="max">Maximum value</param>
/// <returns>int: clamped value</returns> 
int clamp(int val, int min, int max);

/// <summary>Makes sure a value is within two other values</summary>
/// <param name="val">Value to clamp</param>
/// <param name="min">Minimum value</param>
/// <param name="max">Maximum value</param>
/// <returns>float: clamped value</returns> 
float clamp(float val, float min, float max);

//static float getVectorDistance(glm::vec2 pointA, glm::vec2 pointB) {
//	return sqrt(pow((pointA.y - pointB.y), 2) + pow((pointA.x - pointB.x), 2));
//}

//static float getVectorDistance2(glm::vec2 pointA, glm::vec2 pointB) {
//	return sqrt((pointA.y - pointB.y) * (pointA.y - pointB.y) + (pointA.x - pointB.x) * (pointA.x - pointB.x));
//}

//static float getVectorDistance3(glm::vec2 pointA, glm::vec2 pointB) {
//	float distX = pointA.x - pointB.x;
//	float distY = pointA.y - pointB.y;
//	return sqrt(distY * distY + distX * distX);
//}

float getVectorDistance(glm::vec2, glm::vec2);
float getVectorDistanceSquare(glm::vec2, glm::vec2);
float manhattanDistance(glm::vec2 a, glm::vec2 b);
float getVectorAngle(glm::vec2 source, glm::vec2 target);
glm::vec2 rotateVector(glm::vec2 vector, float rotation);

float normalise(float value, float max);
glm::vec2 radiansToVector(float radians);
float magnitude(glm::vec2 vec);
void limitVector(glm::vec2* vec, float limit);

void BresenhamLine(int x0, int y0, int x1, int y1, std::function<void(int x, int y)> func);

int sign(int x);