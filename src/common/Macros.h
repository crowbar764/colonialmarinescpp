#pragma once
#include <bitset>
#include "Defines.h"

// ANSI colour codes (https://gist.github.com/fnky/458719343aabd01cfb17a3a4f7296797#color-codes)
#define CONSOLE_COLOUR_YELLOW 33
#define CONSOLE_COLOUR_CYAN 36
#define CONSOLE_COLOUR_RED 91

// '\033[xm' sets the colour, with x being the colour code. Set back to 0 (white) afterwards.
#define PRINT_COLOURED(msg, colour) (std::cout << "\033[" << colour << "m" << msg << "\033[0m\n")
#define PRINT_WARNING(msg) (PRINT_COLOURED("WARNING: " << msg, CONSOLE_COLOUR_YELLOW))
#define PRINT_ERROR(msg) (PRINT_COLOURED("ERROR: " << msg, CONSOLE_COLOUR_RED))
#define PRINT_CYAN(msg) (PRINT_COLOURED(msg, CONSOLE_COLOUR_CYAN))

#define BIT_1(flags, flag) (flags |= flag)
#define BIT_0(flags, flag) (flags &= ~(flag))
#define BIT_TOGGLE(flags, flag) (flags ^= flag)
#define BIT_SET(flags, flag, newState) ((newState) ? BIT_1(flags, flag) : BIT_0(flags, flag))
#define BIT_CHECK(flags, flag) (flags & flag)

// Honestly not sure about these two:
// Only gets bits of this value and lower.
// E.G. (non binary for demonstration) BIT_MASK_LOW(..00054321, 15) returns ..00004321
// (15 is 1111)
#define BIT_MASK_LOW(flags, maxValue) (flags & maxValue)
// Skips x bits, moving the remaining bits in the process.
// E.G. (non binary for demonstration) BIT_MASK_HIGH(..00054321, 3) returns ..00000054
#define BIT_MASK_HIGH(flags, numOfBitsToSkip) (flags >> numOfBitsToSkip)

#define PRINT(text) (std::cout << text << "\n")
#define PRINTV(label, vector) (std::cout << label << ": " << vector.x << "," << vector.y << "\n")
#define PRINTV4(label, vector) (std::cout << label << ": " << vector.x << "," << vector.y << "," << vector.z << "," << vector.w << "\n")
#define PRINTIP(label, intpair) (std::cout << label << ": " << intpair.x << "," << intpair.y << "\n")
// Types smaller than int are converted to int after bitwise operations (not in this macro), so bit output might look bigger than it actually is.
// For sanity, this just uses an int's bitsize (32).
// https://softwareengineering.stackexchange.com/questions/410717/in-c-why-do-bitwise-operators-convert-8-or-16-bit-integers-to-32-bit
#define PRINTB(flags) (std::cout << std::bitset<sizeof(int) * 8>(flags) << "\n")
#define V(vector) "(" << vector.x << "," << vector.y << ")"

#define vErase(arr, el) (arr.erase(std::find(arr.begin(), arr.end(), el)))
#define vpErase(arr, el) (arr->erase(std::find(arr->begin(), arr->end(), el)))

#define VECTOR_FAST_LENGTH(vec) (abs(vec.x) + abs(vec.y)) 
#define VECTOR_NOT_ZERO(vec) (vec.x || vec.y)

#define LOOP(iterations) for ( int i = 0; i < iterations; i++ )
#define FREEZE while(true) {}

// (Modulo except it works for negative numbers.)
// COPY PASTED FROM REFACTOR, CHECK IT WORKS PLEASE
// DIR_TABLE[(moveDirection - 1 + 8) % 8];