#pragma once

#include <glm/glm.hpp>

struct IntPair {
	int x, y;

	bool operator==(IntPair const& other) const {
		return ((x == other.x) && (y == other.y));
	}

	IntPair operator+(int const& other) {
		return { x + other, y + other };
	}

	IntPair operator+(glm::vec2 const& other) {
		return { x + (int) other.x, y + (int) other.y };
	}

	IntPair operator+(IntPair const& other) {
		return { x + other.x, y + other.y };
	}

	glm::vec2 operator+(float const& other) {
		return { x + other, y + other };
	}

	IntPair operator-(int const& subtractor) {
		return { x - subtractor, y - subtractor };
	}

	IntPair operator*(int const& multiplier) {
		return { x * multiplier, y * multiplier };
	}

	IntPair operator/(int const& divisor) {
		return { x / divisor, y / divisor };
	}

	// IntPair -> glm::vec2
	operator glm::vec2() const {
		return { x, y };
	}
};