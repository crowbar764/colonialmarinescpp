#pragma once

static constexpr int SPRITE_MISSING = 0;

// Turfs 1
static constexpr int SPRITE_BLANK = 1;
static constexpr int SPRITE_SPACE = 2;
static constexpr int SPRITE_PLATING = 3;
static constexpr int SPRITE_HULL = 4;
static constexpr int SPRITE_DOOR = 5;
static constexpr int SPRITE_DESERT = 6;
static constexpr int SPRITE_BARRIER = 7;

// Mobs 200
static constexpr int SPRITE_MARINE = 200;
static constexpr int SPRITE_BUG = 201;

// Effects 400
static constexpr int SPRITE_SELECT = 400;
static constexpr int SPRITE_BULLET = 401;
static constexpr int SPRITE_MARKER = 402;
static constexpr int SPRITE_SELECT_THIN = 403;
static constexpr int SPRITE_ARROW = 404;
static constexpr int SPRITE_LOS = 405;
static constexpr int SPRITE_TROPHEY = 406;

// Aura 700
static constexpr int SPRITE_FAST = 700;
static constexpr int SPRITE_SLOW = 701;
static constexpr int SPRITE_RANK = 702;
static constexpr int SPRITE_LOWRANK = 703;

// GUI 800
static constexpr int SPRITE_GUI_SLICE_DEBUG = 800;
static constexpr int SPRITE_GUI_SLICE_OUT = 801;
static constexpr int SPRITE_GUI_SLICE_IN = 802;

// Debug 900
static constexpr int SPRITE_DEBUG = 900;
static constexpr int SPRITE_DEBUG_OUTLINE_PINK = 901;
static constexpr int SPRITE_DEBUG_LINE = 902;
static constexpr int SPRITE_DEBUG_OUTLINE_WHITE = 903;
static constexpr int SPRITE_DEBUG_OUTLINE_RED = 904;
static constexpr int SPRITE_DEBUG_POINT = 905;
static constexpr int SPRITE_MISSING2 = 906;
static constexpr int SPRITE_DEBUG_DIRECTIONAL_TEST = 907;
static constexpr int SPRITE_DEBUG_ANIMATED_TEST = 908;
static constexpr int SPRITE_DEBUG_DIRECTIONAL_ANIMATED_TEST = 909;
static constexpr int SPRITE_DEBUG_CIRCLE = 910;