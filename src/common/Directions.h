#pragma once

#include <glm/glm.hpp>

static constexpr float PI = 3.14159f;
static constexpr float PI2 = PI * 2;
static constexpr float QUARTER_PI = PI * .25f;

// Bitflags

// Directions that are bit-unique.
static constexpr unsigned int DIR_BIT_N = 1;
static constexpr unsigned int DIR_BIT_E = 2;
static constexpr unsigned int DIR_BIT_NE = 4;
static constexpr unsigned int DIR_BIT_S = 8;
static constexpr unsigned int DIR_BIT_SE = 16;
static constexpr unsigned int DIR_BIT_W = 32;
static constexpr unsigned int DIR_BIT_NW = 64;
static constexpr unsigned int DIR_BIT_SW = 128;

// Directions that can be added together.
static constexpr auto DIR_N = 1;
static constexpr auto DIR_E = 2;
static constexpr auto DIR_NE = 3;
static constexpr auto DIR_S = 4;
static constexpr auto DIR_SE = 6;
static constexpr auto DIR_W = 8;
static constexpr auto DIR_NW = 9;
static constexpr auto DIR_SW = 12;

// Directions that fit in 1 byte. Non combinable.
static constexpr uint8_t DIR_SMPL_N = 0;
static constexpr uint8_t DIR_SMPL_NE = 1;
static constexpr uint8_t DIR_SMPL_E = 2;
static constexpr uint8_t DIR_SMPL_SE = 3;
static constexpr uint8_t DIR_SMPL_S = 4;
static constexpr uint8_t DIR_SMPL_SW = 5;
static constexpr uint8_t DIR_SMPL_W = 6;
static constexpr uint8_t DIR_SMPL_NW = 7;
static constexpr uint8_t DIR_SMPL_NONE = 8;

// Directions that fit in 1 byte. Non combinable.
// Different order than above.
static constexpr uint8_t DIR_SMPL2_N = 0;
static constexpr uint8_t DIR_SMPL2_E = 1;
static constexpr uint8_t DIR_SMPL2_S = 2;
static constexpr uint8_t DIR_SMPL2_W = 3;
static constexpr uint8_t DIR_SMPL2_NE = 4;
static constexpr uint8_t DIR_SMPL2_SE = 5;
static constexpr uint8_t DIR_SMPL2_NW = 6;
static constexpr uint8_t DIR_SMPL2_SW = 7;
static constexpr uint8_t DIR_SMPL2_NONE = 8;

// Vector directions.
static constexpr glm::vec2 VEC_DIR_NONE = { 0.0f,  0.0f };
static constexpr glm::vec2 VEC_DIR_N = { 0.0f, -1.0f };
static constexpr glm::vec2 VEC_DIR_NE = { 1.0f, -1.0f };
static constexpr glm::vec2 VEC_DIR_E = { 1.0f,  0.0f };
static constexpr glm::vec2 VEC_DIR_SE = { 1.0f,  1.0f };
static constexpr glm::vec2 VEC_DIR_S = { 0.0f,  1.0f };
static constexpr glm::vec2 VEC_DIR_SW = { -1.0f,  1.0f };
static constexpr glm::vec2 VEC_DIR_W = { -1.0f,  0.0f };
static constexpr glm::vec2 VEC_DIR_NW = { -1.0f, -1.0f };

// Radian directions.
static constexpr float RADIANS_N = 0;
static constexpr float RADIANS_NE = 0.785398f;
static constexpr float RADIANS_E = 1.5708f;
static constexpr float RADIANS_SE = 2.35619f;
static constexpr float RADIANS_S = 3.14159f;
static constexpr float RADIANS_SW = 3.92699f;
static constexpr float RADIANS_W = 4.71239f;
static constexpr float RADIANS_NW = 5.49779f;

// Use with DIR_SMPL_x.
inline glm::vec2 const DIR_TABLE[] = {
	{ 0.0f, -1.0f}, // N
	{ 1.0f, -1.0f}, // NE
	{ 1.0f,  0.0f}, // E
	{ 1.0f,  1.0f}, // SE
	{ 0.0f,  1.0f}, // S
	{-1.0f,  1.0f}, // SW
	{-1.0f,  0.0f}, // W
	{-1.0f, -1.0f}, // NW
	{ 0.0f,  0.0f}, // NONE
	{ 0.0f,  0.0f}  // LOS
};

// Use with DIR_SMPL_x.
inline glm::vec2 const DIR_TABLE_NORMALISED[9] = {
	{ 0.0f,		-1.0f},		// N
	{ .707107f, -.707107f}, // NE
	{ 1.0f,		0.0f},		// E
	{ .707107f, .707107f},	// SE
	{ 0.0f,		1.0f},		// S
	{-.707107f, .707107f},	// SW
	{-1.0f,		0.0f},		// W
	{-.707107f, -.707107f}, // NW
	{ 0.0f,		0.0f}
};

// Use with DIR_SMPL_x.
float const DIR_TABLE_RADIANS[8] = {
	0.0f,	   // N
	0.785398f, // NE
	1.5708f,   // E
	2.35619f,  // SE
	3.14159f,  // S
	3.92699f,  // SW
	4.71239f,  // W
	5.49779f   // NW
};

//static constexpr unsigned short FLOWFIELD_0 = (1 << 0);
//static constexpr unsigned short FLOWFIELD_1 = (1 << 1);
//static constexpr unsigned short FLOWFIELD_2 = (1 << 2);
//static constexpr unsigned short FLOWFIELD_3 = (1 << 3);
//static constexpr unsigned short FLOWFIELD_4 = (1 << 4);

//static constexpr unsigned short RENDER_MASTER = (1 << 0);
//static constexpr unsigned short RENDER_REDRAW_TURF = (1 << 1);
//static constexpr unsigned short RENDER_USING_STATIC_DRAW_A = (1 << 2);
//static constexpr unsigned short RENDER_ASTAR = (1 << 3);
static constexpr uint8_t FLOWFIELD_LOS = 9;
//static constexpr uint8_t FLOWFIELD_PATHABLE = (1 << 5);
//static constexpr unsigned short FLOWFIELD_BLOCKED = (1 << 5);
//static constexpr unsigned short FLOWFIELD_GOAL = (1 << 6);


static constexpr uint8_t FLOW_NONE = 8;
//static constexpr uint8_t FLOW_LOS  = 9;
static constexpr uint8_t FLOW_BLOCKED = 10; // remove me
static constexpr uint8_t FLOW_GOAL = 11; // remove me

static constexpr float FLOWFIELD_LOS_CODE = 100.0f;


uint8_t radiansToDirection(float rotation);