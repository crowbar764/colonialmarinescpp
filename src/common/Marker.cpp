#include "Marker.h"
#include "../rendering/Renderer.h"
#include "../common/IntPair.h"

// Basically the same as Marker. Please remove one of them.

Marker::Marker(glm::vec2 position, float offset) {
	this->position = position + offset;

	Renderer::frameMarkers.push_back(this);
}

Marker::Marker(IntPair position, float offset) {
	this->position = glm::vec2{ position.x, position.y } + offset;

	Renderer::frameMarkers.push_back(this);
}

Marker::Marker(float x, float y, float offset) {
	this->position = glm::vec2{ x, y } + offset;

	Renderer::frameMarkers.push_back(this);
}

Marker::Marker(int x, int y, float offset) {
	this->position = glm::vec2{ x, y } + offset;

	Renderer::frameMarkers.push_back(this);
}