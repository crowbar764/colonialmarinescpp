#pragma once

#include <vector>
#include <string>

// Removes element from array. WARNING: ruins order.
template <class Container, class Iterator>
void eraseFast(Container& c, Iterator it) {
	if ( &(*it) == &(c.back()) ) {
		c.pop_back();
		return;
	}
	*it = std::move(c.back());
	c.pop_back();
}

//template <typename T>
//void vErase(std::vector<T> searchArray, T searchElement) {
//	searchArray.erase(std::find(searchArray.begin(), searchArray.end(), searchElement));
//}