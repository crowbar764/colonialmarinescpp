#pragma once

#include <iostream>
#include <vector>

#include <glm/glm.hpp>
//#include <glm/gtc/matrix_transform.hpp>
//#include <glm/gtc/type_ptr.hpp>

#include "Defines.h"
#include "Macros.h"
#include "Marker.h"
#include "DebugLine.h"

struct TexCoords {
	float x, y, x2, y2;
};

struct RawImageData {
	unsigned char* imageData;
	int width, height;
};

struct RawImageDataUnsorted {
	RawImageData* data;
	std::string label;
};

struct Sprite9 {
	short ID;
	std::vector<TexCoords*> slices;
	float border;
};

struct Sprite {
	short ID;
	bool directional;				// Does the sprite have alt versions for each direction.
	bool animated;					// Does this sprite require ticks?
	std::vector<TexCoords*> frames; // Array of slice data.
	int frameCount = 1;				// The number of animation frames.
	float frameRate;				// The delta between frames.
	int nextSprite;					// The ID of the sprite to switch to when this animation finishes.
};

enum class EntityType
{
	TURF,
	MOB,
	EFFECT
};

enum class EntityArrayType
{
	TURF,
	ENTITY
};

//enum class TurfType
//{
//	SPACE,
//	HULL,
//	PLATING,
//	BLANK,
//	END
//};

//enum class MobType
//{
//	MARINE,
//	SNIPER
//};

enum class EffectType
{
	SELECT
};

enum class AiState
{
	IDLE,
	SLEPT,
	MOVE,
	WAITING_FOR_PATH,
	DEFEND
};

enum class Faction
{
	WORLD,
	HUMAN,
	ALIEN
};

enum class ENUM_status
{
	CO,
	CIC,
	RTO,
	SIZE
};

enum class ENUM_rooms
{
	CIC
};