#pragma once

static constexpr int EVENT_CHECKBOX_TOGGLE_ASTAR = 0;
static constexpr int EVENT_CHECKBOX_TOGGLE_CLICKMASK = 1;
static constexpr int EVENT_CHECKBOX_TOGGLE_LIGHTING = 2;
static constexpr int EVENT_CHECKBOX_TOGGLE_EDITMODE = 3;
static constexpr int EVENT_CHECKBOX_TOGGLE_GHOST = 4;
static constexpr int EVENT_CHECKBOX_TOGGLE_ENTITIES = 5;

static constexpr int EVENT_BUTTON_SAVE_WORLD = 0;
static constexpr int EVENT_BUTTON_CYCLE_EDITMODE = 1;