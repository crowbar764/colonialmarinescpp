#pragma once

// Turfs.
static constexpr short TYPE_SPACE = 0;
static constexpr short TYPE_HULL = 1;
static constexpr short TYPE_PLATING = 2;
static constexpr short TYPE_BLANK = 3;
static constexpr short TYPE_DOOR = 4;
static constexpr short TYPE_DESERT = 5;
static constexpr short TYPE_BARRIER = 6;

static constexpr short NUM_OF_TURF_TYPES = 6;

// Mobs.
static constexpr short TYPE_HUMAN = 100;

