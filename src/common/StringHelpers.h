#include <vector>
#include <string>

std::vector<std::string> splitString(std::string, char);
std::vector<std::string_view> splitStringFast(std::string_view, std::string_view);