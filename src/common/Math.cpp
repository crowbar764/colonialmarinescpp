#include "../global/Camera.h"
#include "../rendering/Renderer.h"

int clamp(int val, int min, int max) {
	if ( val < min )
		return min;
	else if ( val > max )
		return max;
	else
		return val;
}

float clamp(float val, float min, float max) {
	if ( val < min )
		return min;
	else if ( val > max )
		return max;
	else
		return val;
}

// Returns distance between two vector. // TODO: Should this use ABS?
float getVectorDistance(glm::vec2 pointA, glm::vec2 pointB) {
	float distX = abs(pointA.x - pointB.x);
	float distY = abs(pointA.y - pointB.y);
	return sqrt(distY * distY + distX * distX);
}

// Returns the size of the longest axis. Good for square-shaped distance searches.
// Cheaper than getVectorDistance(). 
float getVectorDistanceSquare(glm::vec2 a, glm::vec2 b) {
	return std::max(
		abs(a.x - b.x), // Distance x.
		abs(a.y - b.y)  // Distance y.
	);
}

float manhattanDistance(glm::vec2 a, glm::vec2 b) {
	return abs(a.x - b.x + a.y - b.y);
}

// Returns the rotation needed in RADIANS (used by renderer) to aim towards target. To get degrees: radians * 180 / 3.141.
float getVectorAngle(glm::vec2 source, glm::vec2 target) {
	return atan2(target.y - source.y, target.x - source.x);
}

/*
 *	Rotates a vector position around 0, 0.
 *	Rotation in Radians.
 *	E.G: Input (1, 2) returns Output (-1, -2).
 *
 *	[ ] [O] [ ] [ ] [ ]
 *	[ ] [ ] [ ] [ ] [ ]
 *	[ ] [ ] [X] [ ] [ ]
 *	[ ] [ ] [ ] [ ] [ ]
 *	[ ] [ ] [ ] [I] [ ]
 */
glm::vec2 rotateVector(glm::vec2 vector, float rotation) {
	float s = sin(rotation);
	float c = cos(rotation);

	// Rotate around top middle, which is (0, 0) because of earlier translations.
	glm::vec2 rotated = {
		vector.x * c - vector.y * s,
		vector.x * s + vector.y * c
	};

	//PRINT("Rotate from: (" << x << ", " << y << ") to (" << rotated.x << ", " << rotated.y << ")");

	return rotated;
}

float normalise(float value, float max) {
	return value / max;
}

glm::vec2 radiansToVector(float radians) {
	return { sin(radians), -cos(radians) };
}

float magnitude(glm::vec2 vec) {
	return sqrt((vec.x * vec.x) + (vec.y * vec.y));
}

// Uses pointer version of SET_MAGNITUDE
void limitVector(glm::vec2* vec, float limit) {
	if ( magnitude(*vec) > limit )
		*vec = glm::normalize(*vec) * limit;
}

// http://members.chello.at/easyfilter/bresenham.html
// http://members.chello.at/easyfilter/canvas.html
// Calls func for every line point along the path.
// Flaw: The major (longer) axis can only ever increment by 1 at most per movement, meaning for steep changes on both axis, minor will look fine, major will skip points.
// Shown here: https://images2.imgbox.com/b5/cd/U3GvEJBb_o.png
void BresenhamLine(int x0, int y0, int x1, int y1, std::function<void(int x, int y)> func) {
	int dx = abs(x1 - x0), sx = x0 < x1 ? 1 : -1;
	int dy = -abs(y1 - y0), sy = y0 < y1 ? 1 : -1;
	int err = dx + dy, e2; /* error value e_xy */
	
	// Algorithm works on grid / pixel centers so offset visually to center.
	new DebugLine(x0 + .5f, y0 + .5f, x1 + .5f, y1 + .5f, "BresenhamDebug");

	while (true) {
		func(x0, y0);

		if ( x0 == x1 && y0 == y1 ) break;
		e2 = 2 * err;
		if ( e2 >= dy ) { err += dy; x0 += sx; } /* e_xy+e_x > 0 */
		if ( e2 <= dx ) { err += dx; y0 += sy; } /* e_xy+e_y < 0 */
	}
}

/*
* Returns the sign, e.g:
* 
* 5 returns 1
* -5 returns -1
* 0 returns 0
* 
* Template version available here: https://stackoverflow.com/questions/1903954/is-there-a-standard-sign-function-signum-sgn-in-c-c
*/
int sign(int x) {
	return (x > 0) - (x < 0);
}