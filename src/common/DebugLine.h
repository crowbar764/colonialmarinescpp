#pragma once
#include <glm/glm.hpp>
#include <string>
#include "DefinesColours.h"

// A line object that will persist and be rendered until its ID is cleared.
// Multiple lines with the same ID can exist at the same time.
struct DebugLine {
	float x0, y0, x1, y1;
	glm::vec4 colour;

	DebugLine(float x0, float y0, float x1, float y1, std::string id = "", glm::vec4 colour = COLOUR_RED);

	// Empty does not clear all (?), just generic, no key lines.
	static void clearLines(std::string id = "");
};