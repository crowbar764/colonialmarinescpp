#pragma once

#include <glm/glm.hpp>

struct IntPair;

class Marker {
public:
	glm::vec2 position;

	Marker(glm::vec2 position, float offset = 0);
	Marker(IntPair position, float offset = 0);
	Marker(float x, float y, float offset = 0);
	Marker(int x, int y, float offset = 0);
};