#pragma once
#include "IntPair.h"

glm::vec2 screenToWorld(IntPair screen);
IntPair screenToGrid(IntPair screen);

IntPair worldToGrid(float x, float y);
IntPair worldToGrid(glm::vec2 position);
glm::vec2 worldToGridF(glm::vec2 position);