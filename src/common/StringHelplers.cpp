#include <vector>
#include <sstream>

std::vector<std::string> splitString(std::string str, char splitter) {
    std::vector<std::string> strParts;

    // construct a stream from the string 
    std::stringstream ss(str);

    std::string s;
    while ( getline(ss, s, splitter) )
        strParts.push_back(s);

    return strParts;
}

std::vector<std::string_view> splitStringFast(std::string_view str, std::string_view delimeters) {
    std::vector<std::string_view> res;

    const char* ptr = str.data();
    size_t size = 0;

    for ( const char c : str ) {
        for ( const char d : delimeters ) {
            if ( c == d ) {
                res.emplace_back(ptr, size);
                ptr += size + 1;
                size = 0;
                goto next;
            }
        }
        ++size;
    next: continue;
    }

    if ( size )
        res.emplace_back(ptr, size);
    return res;
}