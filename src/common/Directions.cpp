#include "Directions.h"

// Input: Radians from -inf to inf.
// Output: The closest(?) direction (N, NE, E..) to that rotation.
uint8_t radiansToDirection(float rotation) {
	uint8_t val = (uint8_t) round((rotation / QUARTER_PI) + 0.5f);

	return (val + 8) % 8; // Modulo but works with negative.
}