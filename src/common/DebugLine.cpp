#include "DebugLine.h"
#include "../rendering/Renderer.h"

DebugLine::DebugLine(float x0, float y0, float x1, float y1, std::string id, glm::vec4 colour) {
	this->x0 = x0;
	this->y0 = y0;
	this->x1 = x1;
	this->y1 = y1;
	this->colour = colour;

	Renderer::lines.insert({ id, this });
}

// Removes all matching IDs.
void DebugLine::clearLines(std::string id) {
	// Can be done faster with find() or something? Who cares;
	for ( auto it = Renderer::lines.begin(); it != Renderer::lines.end(); it++ ) 
		if ( it->first == id )
			delete(it->second);

	Renderer::lines.erase(id);
}