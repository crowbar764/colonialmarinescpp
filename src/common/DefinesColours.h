#pragma once
#include <glm/glm.hpp>

static constexpr glm::vec4 COLOUR_RED = { 1.0f, 0, 0, 1.0f };
static constexpr glm::vec4 COLOUR_GREEN = { 0, 1.0f, 0, 1.0f };
static constexpr glm::vec4 COLOUR_BLUE = { 0, 0, 1.0f, 1.0f };