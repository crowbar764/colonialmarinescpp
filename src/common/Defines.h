#pragma once

static constexpr char TITLE[] = "Colonial Marines";

//static constexpr float FIXED_TIMESTEP = 0.0625f; // 16 Hz (SC2)
static constexpr float FIXED_TIMESTEP =   0.016f; // 60hz
//static constexpr float FIXED_TIMESTEP = 0.5f;

// HARDCODED. FIIIIIIIX.
static constexpr int NUMBER_OF_CHUNKS = 3;

// The maximum width a map can be.
static constexpr int MAX_MAP_DIMENSION = 3000;
static constexpr int UNITS_PER_TILE = 1;
static constexpr float UNITS_PER_TILE_HALF = .5f;
static constexpr short CHUNK_SIZE = 10;
static constexpr short CHUNKS = MAX_MAP_DIMENSION / CHUNK_SIZE; // How many in each axis;

static constexpr short MAX_SPRITES = 1000;

// At this tile res and lower, we don't bother creating a clickmask.
static constexpr int CLICKMASK_MAXIMUM_ZOOM = 8;

// To light an area to original colour, convert the shader amplifier (4) into a float (.25). Light should set RGB to this value.
static constexpr float LIGHTING_AMPLIFY_COEFFICIENT = 0.25f;

static constexpr uint8_t MOVEMENT_MODE_NONE = 0;
static constexpr uint8_t MOVEMENT_MODE_FLOWFIELD = 1;
static constexpr uint8_t MOVEMENT_MODE_GROUP = 2;

static constexpr unsigned short RENDER_MASTER				= (1 << 0);
static constexpr unsigned short RENDER_REDRAW_TURF			= (1 << 1);
static constexpr unsigned short RENDER_USING_STATIC_DRAW_A = (1 << 2);
static constexpr unsigned short RENDER_ASTAR				= (1 << 3);
static constexpr unsigned short RENDER_CLICKMASK			= (1 << 4);
static constexpr unsigned short RENDER_LIGHTING			= (1 << 5);
static constexpr unsigned short RENDER_ENTITIES			= (1 << 6);
static constexpr unsigned short RENDER_RECALC_GUI			= (1 << 7);
static constexpr unsigned short RENDER_REDRAW_GUI			= (1 << 8);

static constexpr unsigned int RENDERER_MAX_VALUES = 8388608; // 2^23

// FONT
static constexpr float LINE_HEIGHT = 0.03f;
static constexpr int SPACE_ASCII = 32;

// IO
static constexpr auto IO_KEY_W = (1 << 0);
static constexpr auto IO_KEY_A = (1 << 1);
static constexpr auto IO_KEY_S = (1 << 2);
static constexpr auto IO_KEY_D = (1 << 3);

// MODE
static constexpr auto MODE_EDIT = 1;

// CONSTRUCTION FLAGS
static constexpr auto CONSTR_GHOST_ENABLED = 1;
static constexpr auto CONSTR_GHOST_ATTEMPT_PLACE = 2;

// VERTEX PAYLOADS
static constexpr auto VERTEX_PAYLOAD_SPRITE = 4;
static constexpr auto VERTEX_PAYLOAD_SHAPE = 6;