#include "Conversions.h"
#include "../global/Camera.h"
#include "../rendering/Renderer.h"

glm::vec2 screenToWorld(IntPair screen) {
	return {
		(screen.x + Camera::X) / Renderer::tileRes,
		(screen.y + Camera::Y) / Renderer::tileRes
	};
}

IntPair worldToGrid(glm::vec2 world) {
	return {
		(int) floor(world.x),
		(int) floor(world.y)
	};
}

IntPair worldToGrid(float x, float y) {
	return {
		(int) floor(x),
		(int) floor(y)
	};
}

// Copy of above, returning a float vector.
glm::vec2 worldToGridF(glm::vec2 position) {
	return floor(position);
}

IntPair screenToGrid(IntPair screen) {
	return worldToGrid(screenToWorld(screen));
}