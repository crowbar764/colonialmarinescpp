#pragma once
#include "Subsystem.h"
#include "../common/Common.h"
#include <set>

struct IntPair;
class PathPackage;
struct Chunk;

struct FloodDirNode {
	int x, y;
	int8_t parentDirection;
	int cost;
};

//struct FloodCacheNode {
//
//};

class SSPathing : public Subsystem {
public:


	//unsigned char costField[MAX_MAP_DIMENSION][MAX_MAP_DIMENSION];
	//int integrationField[MAX_MAP_DIMENSION][MAX_MAP_DIMENSION];

	std::set<int> windowCache[CHUNK_SIZE][CHUNK_SIZE];
	bool floodCache[CHUNK_SIZE][CHUNK_SIZE];
	std::set<int> currentWindows;
	int lastWindowID; // Keep track of what ID to give the next window that's generated.
	short lastNetworkID; // Keep track of what ID to give when generating networks. 0 reserved for confined spaces in chunks.
	// Add an island ID to every tile so we know if a path is impossible from the start?.

	//std::vector<F_PathingRequest*> F_requests;

	//std::vector<FloodDirNode> floodQueue;

	std::vector<PathPackage*> activePaths;

	SSPathing() {
		name = "SSPathing";

		init();
	}

	//void init();

	void fire(bool resumed);
	
	//void fire(bool resumed) {
	//	//while ( Flow_Pathing::currentRequest || Flow_Pathing::requests.size() ) {
	//		//if ( !Flow_Pathing::currentRequest )
	//			//Flow_Pathing::loadRequest();

	//		// Returns false if tick abort.
	//		//if ( !AStar::findPath() )
	//		//	break;
	//	//}
	//	






	//	//while ( AStar::currentRequest || AStar::requests.size() ) {
	//	//	if ( !AStar::currentRequest )
	//	//		AStar::loadRequest();
	//
	//	//	// Returns false if tick abort.
	//	//	if ( !AStar::findPath() )
	//	//		break;
	//	//}
	//}

	void generatePrePathing();

	void generateWindowsForChunk(int chunkX, int chunkY, bool allDirections = false);

	void createWindow(int chunkX, int chunkY, int xOffset, int yOffset, std::vector<IntPair> windowTiles, std::vector<IntPair> neighbourWindowTiles);

	void generateEdgesForChunk(int chunkX, int chunkY);

	void resetCache();

	void flood(int x, int y, int chunkX, int chunkY);

	//void generateCostField();

	//void generateIntegrationField();

	void resetChunk(int chunkX, int chunkY);

	void resetChunkInternalConnections(Chunk* chunk);

	//void reset();
};