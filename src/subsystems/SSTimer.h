#pragma once
#include "Subsystem.h"
#include <vector>

class Timer;

class SSTimer : public Subsystem {
public:
	inline static std::vector<Timer*> timers;

	SSTimer();

	void fire(bool resumed);
};