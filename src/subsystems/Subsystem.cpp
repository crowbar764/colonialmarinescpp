#include "Subsystem.h"
#include "Master.h"
#include "../common/Common.h"

Subsystem::Subsystem() {
	
}

void Subsystem::fire( bool resumed ) {
	PRINT_WARNING(name << " needs the SS_NO_FIRE flag.");
	
	vErase(Master::subsystems, this);
}

void Subsystem::init() {
	if ( !(flags & SS_NO_FIRE) )
		Master::subsystems.push_back(this);
}