#pragma once
#include "SSTimer.h"
#include "../other/Timer.h"
#include "../Main.h"

/*
* Timers are for delayed behavour.
* Duration is in seconds.
* To discard an unwanted timer, just set its 'obsolete' to true.
*/

SSTimer::SSTimer() {
	name = "SSTimer";
	fireSpeed = .1f;

	init();
}

// Loops through ALL timers, triggering the callback for elapsed ones.
// TODO: Make this a heap system or ordered system so we can stop looping when we encounter the first unfinished timer.
void SSTimer::fire(bool resumed) {
	inProgress = false;

	// Timer callbacks can add / delete other timers so a standard for loop must be used, even if it loops imperfectly.
	// This is ASYNC and refires rapidly so unintended skips don't matter.
	// Not fully tested but seems to work fine.
	for ( int i = 0; i < timers.size(); i++ ) {
		Timer* timer = timers[i];

		// This timer is not needed anymore, skip and delete it.
		if ( timer->obsolete ) {
			timers.erase(timers.begin() + i);
			delete(timer);
			continue;
		}
		
		// Timer has not ended.
		if ( Main::currentTime < timer->endTime )
			continue;

		// Timer has ended.
		timer->callback();

		timers.erase(timers.begin() + i);
		delete(timer);
	}
}