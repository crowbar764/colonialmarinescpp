#pragma once
#include <vector>
#include <chrono>

#include "Subsystem.h"

class Subsystem;
class SSPathing;

constexpr double TICK_LIMIT = 1.0f / 2000.0f;

#define MC_SLEEP

#ifdef MC_SLEEP
	#define TICK_CHECK ( glfwGetTime() > Master::tickEnd ) // current time > end tick time
#else
	#define TICK_CHECK false
#endif

class Master {
public:
	inline static short gameState = GAMESTATE_MAIN_MENU;
	inline static std::vector<Subsystem*> subsystems;
	inline static double tickEnd;

	inline static SSPathing* SSPathing;

	static void fire();

	//static void initialiseSystems() {
	//	for ( Subsystem* subsystem : subsystems ) {
	//		PRINT("Flags for " << subsystem->name << ": ");
	//		PRINTB(subsystem->flags);;

	//		if (!(subsystem->flags & SS_NO_INIT))
	//			subsystem->init();
	//	}
	//}
};