#pragma once
#include "SSPathing.h"
#include "../ai/pathing/Flow_Pathing.h"
#include "../atom/entity/mob/Mob.h"
#include "../ai/pathing/PathPackage.h"

void SSPathing::fire(bool resumed) {
	// Calculate some new paths.
	while ( Flow_Pathing::currentRequest || Flow_Pathing::requests.size() ) {

		if ( ! Flow_Pathing::currentRequest )
			Flow_Pathing::loadRequest();

		Flow_Pathing::findPath();
		break; // remove this
	}

	// Process existing paths (move formation abstracts).
	// THIS SAMPLING MOVEMENT METHOD GETS STUCK.
	for ( PathPackage* package : activePaths ) {
		package->tick();
		// Check if any slots are now in a wall.
		//package->checkCollision();

		//continue;

		// TO TALLY CHUNKS THAT SHOULD HAVE FLOW FIELD, FOR EACH UNIT, CHECK FURTHEST DISTANCE, IF ITS ADDED, STOP, IF ITS NOT ADDED, ADD IT AND THEN CHECK FURTHEST - 1... ETC.

		// Sample each corner of our pilot square postion to get grid coords. Single point sampling causes problems.
		//const int farOffset = UNITS_PER_TILE - 1;
		//IntPair currentGridPositionNW = worldToGrid((int) package->formationPosition.x, (int) package->formationPosition.y);
		//IntPair currentGridPositionNE = worldToGrid((int) package->formationPosition.x + farOffset, (int) package->formationPosition.y);
		//IntPair currentGridPositionSW = worldToGrid((int) package->formationPosition.x, (int) package->formationPosition.y + farOffset);
		//// Technically we can use above results to avoid this last sampling.
		//IntPair currentGridPositionSE = worldToGrid((int) package->formationPosition.x + farOffset, (int) package->formationPosition.y + farOffset);

		//// Use the grid coords to get the movement vectors.
		//glm::vec2 movementVector = DIR_TABLE[package->flowfield[currentGridPositionNW.x][currentGridPositionNW.y]];
		//movementVector			+= DIR_TABLE[package->flowfield[currentGridPositionNE.x][currentGridPositionNE.y]];
		//movementVector			+= DIR_TABLE[package->flowfield[currentGridPositionSW.x][currentGridPositionSW.y]];
		//movementVector			+= DIR_TABLE[package->flowfield[currentGridPositionSE.x][currentGridPositionSE.y]];

		//// (0, 0) vectors cannot be normalised. Not that we need to do anything with zero anyway.
		//if ( !movementVector.x && !movementVector.y )
		//	continue;

		//// Normalise the movement vector to avoid to make move speed consistent.
		//movementVector = glm::normalize(movementVector);

		//// Finally, we can move our pilot along one step.
		//package->formationPosition.x += movementVector.x * package->groupSpeed * FIXED_TIMESTEP;
		//package->formationPosition.y += movementVector.y * package->groupSpeed * FIXED_TIMESTEP;

		//// Check if any slots are now in a wall.
		//package->checkCollision();
	}
}

//void SSPathing::init() {
//	PRINT("sspath init");
//	Subsystem::init();
//
//	//Flow_Pathing::init();
//}