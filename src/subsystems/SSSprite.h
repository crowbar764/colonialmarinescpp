#pragma once
#include "Subsystem.h"
#include "../global/Atlas.h"

// How many directions our directional spritesheets currently support.
static constexpr int SUPPORTED_DIRECTIONS = 4;

class SSSprite : public Subsystem {
public:

	SSSprite() {
		name = "SSSprite";
		fireSpeed = .1f;

		init();
	}

	void fire(bool resumed) {
		
		for ( std::pair<unsigned int, std::shared_ptr<Entity>> ent : World::entities ) {
			Entity* entity = ent.second.get();

			if ( entity->sprite->animated && entity->nextFrame < Main::currentTime ) {

				// If animation over, switch to fallback.
				//if ( entity->sprite->nextSprite && entity->currentFrame == entity->sprite->frameCount ) {
				//	entity->setSprite(entity->sprite->nextSprite);

				//	if ( TICK_CHECK )
				//		return;

				//	continue;
				//}

				// Advance animation. Animation looping (modulo) handled in updateSpriteState().
				entity->currentFrame++;
				entity->updateSpriteState();

				// Set time for next animation change.
				entity->nextFrame = Main::currentTime + entity->sprite->frameRate;
			}
		}

		inProgress = false;
	}

	void init() {
		Subsystem::init();
		Atlas::mainAtlas = new Atlas;
		
		// Load main sprites and add them to main atlas map.
		rapidjson::Document* spriteJSON = ResourceManager::loadJSON("assets/sprites/entities/sprite-data");
		const rapidjson::Value& spriteData = *spriteJSON;
		assert(spriteData.IsArray());
		int arraySize = spriteData.Size();

		// Prepare texture data.
		for ( int i = 0; i < arraySize; i++ ) {
			// Ignore headings.
			if ( spriteData[i].HasMember("section") )
				continue;

			std::string fileName = spriteData[i]["fileName"].GetString();
			RawImageData* frameData = ResourceManager::loadImageFromFile("assets/sprites/entities/" + fileName + ".png");
			Atlas::mainAtlas->addToLoadBuffer(frameData, fileName);
		}

		// Draw sprites to atlas.
		Atlas::mainAtlas->createAtlas(1024);

		for ( int i = 0; i < arraySize; i++ ) {
			// Ignore headings.
			if ( spriteData[i].HasMember("section") )
				continue;

			Sprite* sprite = new Sprite();
			std::string fileName = spriteData[i]["fileName"].GetString();
			sprite->ID = spriteData[i]["id"].GetInt();
			sprite->directional = spriteData[i].HasMember("directional");
			int numOfDirections = sprite->directional ? SUPPORTED_DIRECTIONS : 1;

			// Sanity checks.
			checkSpriteID(sprite->ID, fileName);

			if ( spriteData[i].HasMember("frames") ) {
				sprite->frameCount = spriteData[i]["frames"].GetInt();
				sprite->frameRate = (float) spriteData[i]["frameRate"].GetDouble();
				sprite->animated = true;

				//if ( spriteData[i].HasMember("nextSprite") )
					//sprite->nextSprite = spriteData[i]["nextSprite"].GetInt();
			}

			// The spritesheet.
			auto spriteSheet = Atlas::loadBufferSorted[fileName];

			// The seperation between sub-sprites.
			float xSpacing = (spriteSheet->x2 - spriteSheet->x) * (1.0f / sprite->frameCount);
			float ySpacing = (spriteSheet->y2 - spriteSheet->y) * (1.0f / numOfDirections);

			// 'Cut' spritesheet into its singular frames. Single frame textures just do this once.
			for ( int direction = 0; direction < numOfDirections; direction++ ) {
				for ( int frame = 0; frame < sprite->frameCount; frame++ ) {

					auto x1 = spriteSheet->x + (xSpacing * frame);
					auto y1 = spriteSheet->y + (ySpacing * direction);

					auto coords = new TexCoords {
						x1,
						y1,
						x1 + xSpacing,
						y1 + ySpacing
					};

					// Add 'cut-out' sprite from our spritesheet.
					// This will just do the entier image if no dir / anims.
					sprite->frames.emplace_back(coords);
				}
			}

			Atlas::mainAtlas->sprites[sprite->ID] = sprite;
		}

		Atlas::clearLoadBuffer();
	}

	void checkSpriteID(int ID, std::string fileName) {
		// Invalid ID.
		if ( ID >= MAX_SPRITES || ID < 0 ) {
			PRINT_ERROR("Sprite index " << ID << " (" << fileName << ") overflows the sprite array (0 - " << MAX_SPRITES - 1 << ")");
			abort();
		}

		// ID already in use.
		if ( Atlas::mainAtlas->sprites[ID] ) {
			PRINT_ERROR("Sprite ID " << ID << " (" << fileName << ") already in use.");
			abort();
		}
	}
};

#undef SUPPORTED_DIRECTIONS