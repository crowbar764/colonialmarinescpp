#pragma once
#include "Subsystem.h"
#include "../atom/effect/Effect.h"
//#include "../rendering/Renderer.h"
#include <unordered_map>

class SSEffect : public Subsystem {
public:
	inline static std::unordered_map<short, std::shared_ptr<Effect>> effects;

	SSEffect() {
		name = "SSEffect";
		init();
	}

	void fire(bool resumed) {
		std::unordered_map<short, std::shared_ptr<Effect>> copy = effects;
		//PRINT(effects.size());

		for ( std::pair<short, std::shared_ptr<Effect>> effect : copy ) {
			//entity.second->drawn = false;
			effect.second->tick();
		}
	}

	static void addEffect(Effect* effect) {
		effects.emplace(effect->ID, effect);
	}

	static void deleteEffect(Effect* effect) {
		effects.erase(effect->ID);
	}
};