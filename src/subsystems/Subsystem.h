#pragma once
#include <bitset>
#include <vector>

static constexpr int SS_NO_FIRE = (1 << 0);


static constexpr int SS_INIT_FAIL = 0;
static constexpr int SS_INIT_SUCCESS = 1;

#define RUNLEVELS_DEFAULT (RUNLEVEL_SETUP | RUNLEVEL_GAME | RUNLEVEL_POSTGAME)

static constexpr short GAMESTATE_MAIN_MENU = (1 << 0);
inline constexpr short GAMESTATE_IN_WORLD = (1 << 1);

class Subsystem {
public:
	std::string name;
	short flags = 0;
	short runlevels = GAMESTATE_IN_WORLD; // What stages of the game does this system fire()?
	float fireSpeed = 0; // How many seconds, minimum, between non-resuming fires?
	float nextFreshFire = -1;
	bool inProgress = false; // Is this system processing data from a previous fire?
	//int lastFireDuration = 0;

	virtual void fire(bool resumed);

	virtual void init();

	Subsystem();
};