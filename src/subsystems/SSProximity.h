#pragma once
#include "Subsystem.h"
#include "../ai/BehavourTree.h"

class SSProximity : public Subsystem {
public:
	inline static std::unordered_map<unsigned int, std::weak_ptr<Entity>> copy;

	SSProximity() { 
		name = "SSProximity";
		fireSpeed = .5f;
		flags = SS_NO_FIRE;
		init();
	}

	void fire(bool resumed) {
		if ( !resumed )
			createCopy();

		while ( copy.size() ) {

			if ( std::shared_ptr<Entity> target = copy.begin()->second.lock() ) {
				Mob* mob = (Mob*)target.get();
				mob->aiState = mob->ai->query();
			}

			copy.erase(copy.begin());

			if ( TICK_CHECK )
				return;
		}

		inProgress = false;
	}

	void createCopy() {
		for ( std::pair<unsigned int, std::shared_ptr<Entity>> entity : World::entities ) {
			if ( dynamic_cast<Mob*>(entity.second.get()) ) {
				copy.emplace(entity.first, entity.second);
			}
		}
		PRINT("Copy: " << Main::currentTime);
	}
};