#pragma once
#include "Subsystem.h"
//#include "../atom/Atom.h"
#include "../global/World.h"
#include "../atom/entity/Entity.h"

class SSEntity : public Subsystem {
public:

	SSEntity() {
		name = "SSEntity";
		//fireSpeed = 0.1f;
		//flags = SS_NO_FIRE;
		init();
	}

	void fire(bool resumed) {
		// Error from [x] creating new entity, screwing up vector.
		//std::vector<Entity*> copy = World::entities; // or do entities.reserve(20000) on init;
		//std::vector<std::shared_ptr<Mob>> copy = World::entities;
		for ( std::pair<unsigned int, std::shared_ptr<Entity>> entity : World::entities ) {
			entity.second->tick();
		}
	}
};