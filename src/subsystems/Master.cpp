#include "Master.h"

#include "../Main.h"

#include <GLFW/glfw3.h>

void Master::fire() {
	tickEnd = glfwGetTime() + TICK_LIMIT;

	//double before, after;

	for ( Subsystem* subsystem : subsystems ) {
		if ( !(subsystem->runlevels & gameState) )
			continue;

		if ( !subsystem->inProgress) {
			if ( Main::currentTime > subsystem->nextFreshFire ) {
				subsystem->nextFreshFire = Main::currentTime + subsystem->fireSpeed;
				subsystem->inProgress = true;
				subsystem->fire(false);
			}
			continue;
		}

		//before = glfwGetTime();
		subsystem->fire(true);
		//after = glfwGetTime();

		//PRINT(subsystem->name << " took " << after - before << "ms to run.");
	}
}