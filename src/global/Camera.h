#pragma once
#include <vector>
#include <glm/glm.hpp>
//#include "../common/Common.h"
#include "../common/IntPair.h"

class Camera
{
public:
	static int X; // Pixels from world 0 left
	static int Y; // Pixels from world 0 up
	static float pX, pY; // float precision of position since delta time causes very small adjustments.
	static int xLimit; // How far the camera can move along x before the right side would hit the edge of the world.
	static int yLimit; // How far the camera can move along y before the bottom side would hit the edge of the world.
	static float panSpeed; // How many pixels we move across per second (affected by deltatime)
	static int zoomStage, zoomValue;
	static int lastX, lastY;
	static unsigned int directionsMoved;
	static int pixelsMovedX, pixelsMovedY;
	static int pixelsMovedXneg, pixelsMovedYneg;
	inline static glm::vec4 gridBounds; // The columns and rows which intersect the screen boundaries. TURF NUMBERS, NOT CHUNKS.
	inline static std::vector<IntPair> visibleChunks;

	static void zoom(int);
	static void update();
	static void calculateMoveLimits();
	static void calculateZoomLimits();
	static void moveCameraIntoBounds();
	static void zoomCameraIntoBounds();
	static void calculateLimit();
	static void calculateVisibleChunks();
	static void setZoom(int newZoom);
	static void init();

private:
	static std::vector<int> magnifications;
	static int maxZoomLevel;

	static void centreZoom(float, float);
	static int calculateCameraLimits(int, int);
};