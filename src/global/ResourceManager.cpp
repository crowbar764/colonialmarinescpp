#include "ResourceManager.h"
#include "../rendering/FrameBuffer.h"
#include "../common/DefinesSprites.h"
#include "Atlas.h"

#include <stb_image.h>
#include <algorithm>
#include <filesystem>

rapidjson::Document* ResourceManager::loadJSON(std::string filePath) {
    std::ifstream ifs(filePath + ".json");

    if ( !ifs.is_open() )
        PRINT_ERROR("File '" << filePath << "' does not exist or is already open!");

    rapidjson::IStreamWrapper isw(ifs);
    rapidjson::Document* doc = new rapidjson::Document;
    doc->ParseStream(isw);
    ifs.close();

    if ( doc->HasParseError() )
        PRINT_ERROR("JSON failed to parse: " << doc->GetParseError() << "\nOffset : " << doc->GetErrorOffset());

    return doc;
}

RawImageData* ResourceManager::loadImageFromFile(std::string path) {
    int width, height, nrChannels;

    unsigned char* data = stbi_load(path.c_str(), &width, &height, &nrChannels, 0);

    if ( !data ) {
        PRINT_ERROR("Failed to load texture: " << path);
        abort();
    }

    return new RawImageData {
        data,
        width,
        height
    };
}

void ResourceManager::init() {
    Atlas::missingTexCoords = Atlas::mainAtlas->getSprite(SPRITE_MISSING);
    Atlas::blankTexCoords = Atlas::mainAtlas->getSprite(SPRITE_BLANK);



    // Please check sprite number refactor didn't break this.
    //return;




    // 9 slice stuff.
    rapidjson::Document* sliceJSON = loadJSON("assets/sprites/slice-data");
    const rapidjson::Value& sliceList = *sliceJSON;
    assert(sliceList.IsArray());

    for ( int i = 0; i < (int)sliceList.Size(); i++ ) {
        short sliceID = sliceList[i]["id"].GetInt();
        float sliceBorder = (float)sliceList[i]["border"].GetInt();
        float sliceSize = (float)sliceList[i]["size"].GetInt();
        float unit = 1.0f / Atlas::mainAtlas->atlasSize;
        

        struct Size {
            float x, y, w, h;
        };

        // Get tex coords for entire slicemap.
        TexCoords* sprite2 = Atlas::mainAtlas->getSprite(sliceID)->frames[0];

        // Calculate pixel sizes for each slice.
        std::vector<Size> sizes;
        sizes.push_back({ sliceBorder, 0, sliceBorder, sliceSize - sliceBorder }); // N
        sizes.push_back({ sliceSize - sliceBorder, 0, 0, sliceSize - sliceBorder }); // NE
        sizes.push_back({ sliceSize - sliceBorder, sliceBorder, 0, sliceBorder }); // E
        sizes.push_back({ sliceSize - sliceBorder, sliceSize - sliceBorder, 0, 0 }); // SE
        sizes.push_back({ sliceBorder, sliceSize - sliceBorder, sliceBorder, 0 }); // S
        sizes.push_back({ 0, sliceSize - sliceBorder, sliceSize - sliceBorder, 0 }); // SW
        sizes.push_back({ 0, sliceBorder, sliceSize - sliceBorder, sliceBorder }); // W
        sizes.push_back({ 0, 0, sliceSize - sliceBorder, sliceSize - sliceBorder }); // NW
        sizes.push_back({ sliceBorder, sliceBorder, sliceBorder, sliceBorder }); // C

        // Calculate texCoords.
        std::vector<TexCoords*> divisions;
        for ( Size size : sizes ) {
            divisions.push_back(new TexCoords {
                sprite2->x + unit * size.x,
                sprite2->y - unit * size.y,
                sprite2->x2 - unit * size.w,
                sprite2->y2 + unit * size.h
                }
            );
        }

        // Save to array.
        Atlas::mainAtlas->sprites9[sliceID] = new Sprite9 {
            sliceID,
            divisions,
            sliceBorder
        };
    }
    
    delete(sliceJSON);
}