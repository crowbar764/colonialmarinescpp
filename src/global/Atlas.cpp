#include "Atlas.h"
#include <algorithm>
#include <stb_image.h>
#include "../common/Math.h"
#include "../rendering/FrameBuffer.h"

/*
*
* ATLAS LOAD PROCESS:
* ///////////////////
*
* 0: Create atlas.
* 1: Cycle through all images in assets folder.
* 2: For each image, add it's sprite data (data, width, height..) to the atlasLoadBuffer.
* 3: Sort atlasLoadBuffer by sprite height.
* 4: For each item in atlasLoadBuffer, calculate best atlas position.
* 4.1: Draw sprite to atlas.
* 4.2: Save [sprite-label / tex coords] to texCoordMap.
*
*/

void Atlas::createAtlas(unsigned int size) {
    atlasSize = size;

    // Create pensil texture.
    glGenTextures(1, &pensilTexture);
    glBindTexture(GL_TEXTURE_2D, pensilTexture);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_NEAREST);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_NEAREST);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);

    // Change to static FBO somehow so only need 1 for all atlasses
    FBO = new FrameBuffer(atlasSize, atlasSize, true);

    //FrameBuffer::createFramebuffer(&fbo, &atlas, atlasSize, atlasSize);
    FrameBuffer::createShader(&atlasShader, "shaderBasic", atlasSize, atlasSize);

    glBindTexture(GL_TEXTURE_2D, pensilTexture);

    std::sort(loadBufferUnsorted.begin(), loadBufferUnsorted.end(), compareByHeight);
    addSpritesToAtlas();

    for ( RawImageDataUnsorted* spriteData : loadBufferUnsorted ) {
        stbi_image_free(spriteData->data);
        delete(spriteData);
    }

    loadBufferUnsorted.clear();
    loadBufferUnsorted.shrink_to_fit();

    atlas = FBO->texture;

    // Delete pensil texture and most of FBO after use.
    glDeleteTextures(1, &pensilTexture);
    delete(FBO);

    //PRINT("atlas init, should only happen once");
}

void Atlas::addToLoadBuffer(RawImageData* data, std::string name) {
    RawImageDataUnsorted* spriteData = new RawImageDataUnsorted {
        data,
        name
    };

    loadBufferUnsorted.push_back(spriteData);
}

void Atlas::addSpritesToAtlas() {
    unsigned int pointerOffsetX, pointerOffsetY, rowHeight;

    pointerOffsetX = atlasSize;
    pointerOffsetY = 0;
    rowHeight = 0;

    for (int i{ 0 }; i < loadBufferUnsorted.size(); ++i) {
        RawImageDataUnsorted* sprite = loadBufferUnsorted[i];

        // Need new row
        if (sprite->data->width + pointerOffsetX > atlasSize) {
            pointerOffsetX = 0; // Set to start of texture X
            pointerOffsetY += rowHeight; // Next row should be shifted Y by the largest sprite of that row (rowHeight)
            rowHeight = sprite->data->height; // Set new row's largest sprite
        }

        float topLeftX = (float)pointerOffsetX;
        float topLeftY = (float)pointerOffsetY;
        float bottomLeftX = (float)sprite->data->width + pointerOffsetX;
        float bottomLeftY = (float)sprite->data->height + pointerOffsetY;

        float verts[] = {
            topLeftX,	    topLeftY,	    0.0f, 0.0f,
            bottomLeftX,	topLeftY,	    1.0f, 0.0f,
            bottomLeftX,	bottomLeftY,	1.0f, 1.0f,
            bottomLeftX,    bottomLeftY,	1.0f, 1.0f,
            topLeftX,	    bottomLeftY,	0.0f, 1.0f,
            topLeftX,	    topLeftY,	    0.0f, 0.0f,
        };

        pointerOffsetX += sprite->data->width;

        glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, sprite->data->width, sprite->data->height, 0, GL_RGBA, GL_UNSIGNED_BYTE, sprite->data->imageData);
        glBufferData(GL_ARRAY_BUFFER, sizeof(verts), verts, GL_STATIC_DRAW);
        glDrawArrays(GL_TRIANGLES, 0, 6);

        loadBufferSorted[sprite->label] = new TexCoords {
            normalise(topLeftX, (float) atlasSize),
            1 - normalise(topLeftY, (float) atlasSize),
            normalise(bottomLeftX, (float) atlasSize),
            1 - normalise(bottomLeftY, (float) atlasSize)
        };
    }

    // TODO: Make warning is atlas size is too small.
    //PRINT();
}

Sprite* Atlas::getSprite(short textureLabel) {
    return sprites[textureLabel];
}

Sprite9* Atlas::getSprite9(short spriteID) {
    return sprites9[spriteID];
}

bool Atlas::compareByHeight(const RawImageDataUnsorted* a, const RawImageDataUnsorted* b)
{
    return a->data->height > b->data->height;
}

void Atlas::clearLoadBuffer(bool deleteCoords) {
    if ( deleteCoords ) {
        for ( std::pair<std::string, TexCoords*> sprite : loadBufferSorted ) {
            delete(sprite.second);
        }
    }

    loadBufferSorted.clear();
}