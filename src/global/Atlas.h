#pragma once
#include <string>
#include <unordered_map>
#include "../common/Common.h"
#include "../rendering/shader.h"

struct Sprite;
struct Sprite9;
class FrameBuffer;

class Atlas {
public:
	unsigned int atlas;
	unsigned int atlasSize;

	inline static Sprite* missingTexCoords;
	inline static Sprite* blankTexCoords;
	std::unordered_map<short, Sprite9*> sprites9;

	Sprite* sprites[MAX_SPRITES];
	inline static std::vector<RawImageDataUnsorted*> loadBufferUnsorted;
	inline static std::unordered_map<std::string, TexCoords*> loadBufferSorted;

	inline static Atlas* mainAtlas;

	Sprite* getSprite(short spriteID);
	Sprite9* getSprite9(short spriteID);
	void addToLoadBuffer(RawImageData*, std::string);
	void createAtlas(unsigned int);
	static void clearLoadBuffer(bool deleteCoords = false);

private:
	inline static Shader atlasShader;
	inline static unsigned int pensilTexture;
	FrameBuffer* FBO;

	void addSpritesToAtlas();
	static bool compareByHeight(const RawImageDataUnsorted*, const RawImageDataUnsorted*);
};