#pragma once
#include <stdio.h>
#include <stdlib.h>
#include <string>

#include "../common/Common.h"

#include <rapidjson/document.h>
#include <rapidjson/istreamwrapper.h>
#include <rapidjson/writer.h>
#include <rapidjson/stringbuffer.h>
#include <rapidjson/ostreamwrapper.h>
#include <rapidjson/filewritestream.h>

struct RawImageData;

class ResourceManager
{
public:
	static rapidjson::Document* loadJSON(std::string filePath);
	static RawImageData* loadImageFromFile(std::string);
	static void init();
};