#include <vector>
#include "Camera.h"
#include "../rendering/Renderer.h"
#include "World.h"
#include "../gui/font/SSText.h"
#include "../io/ClickMask.h"
#include "../io/Keyboard.h"
#include "../common/Math.h"
#include "../common/Directions.h"

//TODO init zoomStage with renderer's init tileres
int Camera::X, Camera::Y, Camera::maxZoomLevel, Camera::zoomStage, Camera::zoomValue;
int Camera::xLimit, Camera::yLimit;
float Camera::pX, Camera::pY, Camera::panSpeed;
std::vector<int> Camera::magnifications{ 1, 2, 4, 8, 16, 32, 48, 64, 96, 128, 160, 192, 256, 320, 384, 448 };
unsigned int Camera::directionsMoved;
int Camera::lastX, Camera::lastY;

// Pixels moved (e.g. 10 or 10)
int Camera::pixelsMovedX, Camera::pixelsMovedY; // Pixels moved in last tick
// Pixels moved (e.g. 10 or -10)
int Camera::pixelsMovedXneg, Camera::pixelsMovedYneg; // Pixels moved in last tick

// Does this check if we actually changed zoom?
void Camera::zoom(int direction) {
	float initialZoom = (float)zoomValue;
	float initialScale = Renderer::tileRes;

	zoomStage += direction;
	zoomCameraIntoBounds();

	// We didn't actually change zoom level.
	if (zoomValue == initialZoom)
		return;

	// Move camera so zoom is centred.
	centreZoom(initialZoom, initialScale);

	// --- ZOOM OUT OF BOUNDS CHECK ---
	calculateMoveLimits();

	moveCameraIntoBounds();
	calculateVisibleChunks();

	SSText::regenerateMeshes();

	BIT_1(Renderer::renderFlags, RENDER_REDRAW_TURF);
}

void Camera::update() {
	// --- MOVEMENT ---

	float speed = panSpeed * Main::deltaTime;

	// --- X ---
	if (BIT_CHECK(Keyboard::keys, IO_KEY_A))
		pX -= speed;
	else if ( BIT_CHECK(Keyboard::keys, IO_KEY_D) )
		pX += speed;

	// --- Y ---
	if (BIT_CHECK(Keyboard::keys, IO_KEY_W))
		pY -= speed;
	else if (BIT_CHECK(Keyboard::keys, IO_KEY_S))
		pY += speed;


	X = (int)pX;
	Y = (int)pY;

	moveCameraIntoBounds();

	directionsMoved = 0;

	if (Y < lastY) {
		BIT_1(directionsMoved, DIR_N);
	}
	if (Y > lastY) {
		BIT_1(directionsMoved, DIR_S);
	}
	if (X < lastX) {
		BIT_1(directionsMoved, DIR_W);
	}
	if (X > lastX) {
		BIT_1(directionsMoved, DIR_E);
	}

	if ( !directionsMoved )
		return;

	calculateVisibleChunks();

	pixelsMovedXneg = X - lastX;
	pixelsMovedYneg = Y - lastY;
	pixelsMovedX = abs(pixelsMovedXneg);
	pixelsMovedY = abs(pixelsMovedYneg);
	lastX = X;
	lastY = Y;
}

void Camera::centreZoom(float initialZoom, float initialScale) {
	float diff = 1 - (initialZoom / zoomValue); // 1 - (3 / 4) = .25

	float newX = diff * (Main::width / 2.0f); // returns 0 to half_width. It's the offset from left screen that will be new X.
	float newY = diff * (Main::height / 2.0f);

	newX = (newX + X) / initialScale; // Converts to world coords
	newY = (newY + Y) / initialScale;

	newX *= Renderer::tileRes; // Converts to pixel coords
	newY *= Renderer::tileRes;

	pX = newX;
	X = (int)newX;
	pY = newY;
	Y = (int)newY;
}

/// <summary>Returns the clearance the camera can move before hitting the edge of the World.</summary>
/// <param name="mapGridLength">The World meta width with buffers, e.g. 40</param>
/// <param name="viewportPixelLength">The length in pixels of the screen in the desired direction, e.g. 1920</param>
/// <returns>int: clearance</returns>  
int Camera::calculateCameraLimits(int mapGridLength, int viewportPixelLength) {
	int mapPixelLength = mapGridLength * (int)Renderer::tileRes; // How many pixels wide the map is with current zoom.
	int clearance = mapPixelLength - viewportPixelLength; // Map pixel size - screen size = available space to move before hitting edge.

	return clearance;
}

void Camera::calculateMoveLimits() {
	xLimit = calculateCameraLimits(World::activeWorld->width, Main::width);
	yLimit = calculateCameraLimits(World::activeWorld->height, Main::height);
}

void Camera::calculateZoomLimits() {
	// TODO: seperate zoom limit for y
	int minTileRes = (int)ceil((float)Main::width / World::activeWorld->width); // The minimum size each tile has to be, for the world to fit on the screen.
	for (int i = (int)magnifications.size() - 1; i > -1; i--) // Finds the next best zoom in magnification table
	{
		if (magnifications[i] < minTileRes)
		{
			maxZoomLevel = i + 1;
			break;
		}
	}
}

void Camera::moveCameraIntoBounds() {
	if (X > xLimit)
	{
		X = xLimit;
		pX = (float)xLimit;
	}
	else if (X < 0)
	{
		X = 0;
		pX = 0;
	}
	if (Y > yLimit)
	{
		Y = yLimit;
		pY = (float)yLimit;
	}
	else if (Y < 0)
	{
		Y = 0;
		pY = 0;
	}
}

void Camera::zoomCameraIntoBounds() {
	int newZoom = clamp(zoomStage, maxZoomLevel, (int) magnifications.size() - 1);
	setZoom(newZoom);
	Renderer::setTileRes((float) zoomValue);

	// Disable entity-clicking if too zoomed out.
	if (Renderer::tileRes > CLICKMASK_MAXIMUM_ZOOM)
		ClickMask::active = true;
	else
		ClickMask::active = false;
}

void Camera::setZoom(int newZoom) {
	zoomStage = newZoom;
	zoomValue = magnifications[zoomStage];
}

void Camera::calculateLimit() {
	calculateZoomLimits();
	zoomCameraIntoBounds();
	calculateMoveLimits();
}

void Camera::calculateVisibleChunks() {
	int xStartChunk = (X / (int) Renderer::tileRes - 1) / CHUNK_SIZE; // The chunk intersecting the left of the screen.
	int xEndChunk = ((X + Main::width - 1) / (int) Renderer::tileRes) / CHUNK_SIZE; // The chunk intersecting the right of the screen.
	int yStartChunk = (Y / (int) Renderer::tileRes - 1) / CHUNK_SIZE;
	int yEndChunk = ((Y + Main::height - 1) / (int) Renderer::tileRes) / CHUNK_SIZE;

	visibleChunks.clear();

	for ( int y = yStartChunk; y <= yEndChunk; y++ ) {
		for ( int x = xStartChunk; x <= xEndChunk; x++ ) {
			visibleChunks.emplace_back(x, y);
			//std::cout << "(" << x << "," << y << ") ";
		}
		//std::cout << "\n";
	}
	//std::cout << "\n";

	gridBounds = {
		Camera::X / Renderer::tileRes,
		Camera::Y / Renderer::tileRes,
		(Camera::X + Main::width) / Renderer::tileRes,
		(Camera::Y + Main::height) / Renderer::tileRes
	};
}

void Camera::init() {
	panSpeed = 2000.0f;
}