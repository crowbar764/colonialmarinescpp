#pragma once

#include "../common/IntPair.h"
#include "../common/Common.h"

#include <rapidjson/document.h>
#include <unordered_map>

//constexpr short WORLD_LOADED = (1 << 0);
//constexpr short WORLD_EDITOR = (1 << 1); // Is this editor mode?

constexpr short WORLD_SHIP = 0;
constexpr short WORLD_PLANET = 1;

struct Flow_Window;
struct Flow_Network;
class Entity;
class Turf;

struct Chunk {
	IntPair position;
	std::vector<Entity*> occupants;
	std::unordered_map<int, Flow_Window*> windows;
	std::unordered_map<short, Flow_Network*> networks;
};

class World
{
public:
	// Static.
	inline static std::unordered_map<short, World*> worlds;
	inline static World* activeWorld;
	//inline static std::unordered_map<unsigned int, std::shared_ptr<Effect>> effects;
	inline static std::unordered_map<unsigned int, std::shared_ptr<Entity>> entities;
	//inline static std::unordered_map<unsigned int, std::weak_ptr<Mob>> mobs;
	inline static short flags = 0;

	static void addEntity(Entity* entity);
	static std::shared_ptr<Entity> getEntity(unsigned int);
	static void deleteEntity(Entity* entity);
	static void switchWorld(World* world);
	static void switchWorld(short ID);
	static World* getWorld(short ID);
	static void saveWorld(std::string path, bool saveSpriteLabel = false);
	static void clear();
	static Chunk* worldPosToChunk(glm::vec2 worldPos);


	// Member.
	short ID;
	int width, height;

	Turf* turfs[MAX_MAP_DIMENSION][MAX_MAP_DIMENSION];
	Chunk* chunks[CHUNKS][CHUNKS];
	//std::vector<Entity*> chunks[CHUNKS][CHUNKS];

	glm::vec2 cameraPos; // The camera pos the last time we used this world.
	int cameraZoom = 0; // The camera zoomStage last ttime we used this world.

	World(short ID, short width, short height);
	World(short ID, std::string path);
	void loadArea(rapidjson::Document* data, bool isAnInsert = false, int xOffset = 0, int yOffset = 0);
	bool gridIsInBounds(int x, int y);
	
	
	Turf* newTurf(short, float, float);
	~World();
};