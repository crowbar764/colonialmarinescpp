#include "World.h"
#include "../atom/turf/Hull.h"
#include "../atom/turf/Space.h"
#include "../atom/turf/Plating.h"
#include "../atom/turf/Blank.h"
#include "../atom/entity/mob/Mob.h"
#include "../atom/entity/mob/Human.h"
#include "../common/DefinesTypes.h"
#include "../atom/entity/mob/XO.h"
#include "../atom/entity/mob/Commander.h"
#include "Camera.h"
#include "../rendering/TurfRenderer.h"
#include "../subsystems/Master.h"
#include "../gui/scenes/GUI_Scene_Main_Menu.h"
#include "../gui/scenes/GUI_Scene_Debug.h"
#include "../io/SSSelect.h"
#include "../atom/turf/Desert.h"
#include "../atom/entity/mob/Alien.h"
#include "ResourceManager.h"

// Create world from nothing.
World::World(short ID, short width, short height) {
	this->ID = ID;
	this->width = width;
	this->height = height;

	for ( int y { 0 }; y < height; ++y ) {
		for ( int x { 0 }; x < width; ++x ) {
			turfs[x][y] = newTurf(TYPE_PLATING, (float) x * UNITS_PER_TILE, (float) y * UNITS_PER_TILE);
		}
	}

	for ( int y = 0; y < CHUNKS; y++ ) {
		for ( int x = 0; x < CHUNKS; x++ ) {
			chunks[x][y] = new Chunk { x, y };
		}
	}

	if ( worlds.find(ID) != worlds.end() )
		PRINT_WARNING("Two worlds with same ID: " << ID);

	worlds.emplace(ID, this);
}

// Create world from file.
World::World(short ID, std::string path) {
	this->ID = ID;

	rapidjson::Document* data = ResourceManager::loadJSON(path);

	if ( !data ) {
		PRINT_ERROR("World file '" << path << "' failed to load.");
		abort();
	}

	// Set map size.
	width = (*data)["meta"]["width"].GetInt();
	height = (*data)["meta"]["height"].GetInt();

	// Create map.
	loadArea(data);

	delete(data);

	for ( int y = 0; y < CHUNKS; y++ ) {
		for ( int x = 0; x < CHUNKS; x++ ) {
			chunks[x][y] = new Chunk{ x, y };
		}
	}
	








	for ( int y = 0; y < 1; y++ ) {
		for ( int x = 0; x < 9; x++ ) {
			//auto human = new Human(this, { 10.5f + x, 10.5f + y });
			//human->setSprite(SPRITE_BUG);
		}
	}

	auto mob = new Human(this, { 10.5f, 10.5f });
	mob->setSprite(SPRITE_BUG);

	mob = new Human(this, { 11.5f, 10.5f });
	mob->setSprite(SPRITE_BUG);

	mob = new Human(this, { 18.5f, 10.5f });
	mob->setSprite(SPRITE_BUG);

	mob = new Human(this, { 10.5f, 11.5f });
	mob->setSprite(SPRITE_BUG);







	

	if ( worlds.find(ID) != worlds.end() )
		PRINT_WARNING("Two worlds with same ID: " << ID);

	worlds.emplace(ID, this);

	//for ( int y = 0; y < height; y++ ) {
	//	for ( int x = 0; x < width; x++ ) {
	//		std::string label = "(" + std::to_string(x) + "," + std::to_string(y) + ")";
	//		SSText::newText(label, (float) x * UNITS_PER_TILE, (float) y * UNITS_PER_TILE, .025f, glm::vec4(1, 1, 1, 1), false);
	//	}
	//}
}

// For actually creating turfs / ents.
void World::loadArea(rapidjson::Document* data, bool isAnInsert, int xOffset, int yOffset) {
	int areaWidth = (*data)["meta"]["width"].GetInt();
	int areaHeight = (*data)["meta"]["height"].GetInt();

	//For each map cell (and boundry cell), create the cell or boundry.
	for (int y = yOffset; y < areaHeight + yOffset; ++y) {
		for (int x = xOffset; x < areaWidth + xOffset; ++x) {
			short turfType = (*data)["tiles"][y - yOffset][x - xOffset]["type"].GetInt();

			if (isAnInsert) {
				if (turfType == TYPE_BLANK)
					continue;

				delete(turfs[x][y]);
			}

			turfs[x][y] = newTurf(turfType, (float)x * UNITS_PER_TILE, (float)y * UNITS_PER_TILE);
		}
	}
}

Turf* World::newTurf(short type, float x, float y) {

	switch (type) {
	case TYPE_SPACE:
		return new Space(this, glm::vec2{ x, y });
	case TYPE_HULL:
		return new Hull(this, glm::vec2{ x, y });
	case TYPE_PLATING:
		return new Plating(this, glm::vec2{ x, y });
	case TYPE_BLANK:
		return new Blank(this, glm::vec2{ x, y });
	case TYPE_DOOR:
		return new Door(this, glm::vec2 { x, y });
	case TYPE_DESERT:
		return new Desert(this, glm::vec2 { x, y });
	case TYPE_BARRIER:
		return new Barrier(this, glm::vec2 { x, y });
	default:
		PRINT_WARNING("No turf type found with ID : " << static_cast<int>(type));
		return NULL;
	}
}

void World::addEntity(Entity* entity) {
	std::shared_ptr<Entity> sptr;
	sptr.reset(entity);
	entities.insert({ entity->ID, sptr });
}

std::shared_ptr<Entity> World::getEntity(unsigned int entityID) {
	return entities.at(entityID);
}

void World::deleteEntity(Entity* entity) {
	entities.erase(entity->ID);
}

// Remember, only late-placable maps need sprite names saved for the ghost mechanic.
void World::saveWorld(std::string path, bool saveSpriteName) {
	rapidjson::Document d;
	rapidjson::Document::AllocatorType& allocator = d.GetAllocator();
	rapidjson::StringBuffer s;
	rapidjson::Writer<rapidjson::StringBuffer> writer(s);
	d.SetObject();

	rapidjson::Value save_meta(rapidjson::kArrayType);
	save_meta.SetObject();
	save_meta.AddMember("width", activeWorld->width, allocator);
	save_meta.AddMember("height", activeWorld->height, allocator);
	d.AddMember("meta", save_meta, allocator);

	rapidjson::Value save_tiles(rapidjson::kArrayType);
	for (int y{ 0 }; y < activeWorld->height; ++y) {
		rapidjson::Value save_tileColumn(rapidjson::kArrayType);
		for (int x{ 0 }; x < activeWorld->width; ++x) {

			rapidjson::Value save_tile(rapidjson::kArrayType);
			save_tile.SetObject();
			save_tile.AddMember("type", activeWorld->turfs[x][y]->typeID, allocator);

			if (saveSpriteName) {
				short sprite = activeWorld->turfs[x][y]->sprite->ID;
				save_tile.AddMember("sprite", sprite, allocator);
			}

			save_tileColumn.PushBack(save_tile, allocator);
		}
		save_tiles.PushBack(save_tileColumn, allocator);
	}
	d.AddMember("tiles", save_tiles, allocator);

	d.Accept(writer);
	std::ofstream ofs("maps/" + path + ".json");
	ofs << s.GetString();
	ofs.close();
}

bool World::gridIsInBounds(int x, int y) {
	return !(x < 0 || y < 0 || x > width - 1 || y > height - 1);
}

void World::switchWorld(short ID) {
	switchWorld(worlds[ID]);
}

// TODO: Multi-world support for orbit.
void World::switchWorld(World* world) {
	// Save camera information for when we switch back.
	if ( activeWorld ) {
		activeWorld->cameraPos = { Camera::pX, Camera::pY };
		activeWorld->cameraZoom = Camera::zoomStage;
	}

	activeWorld = world;

	// Unload current world (if there is one).
	TurfRenderer::invalidTurfs.clear();

	// Reset mouse cursor.
	Main::setCursor();
	
	// Reset camera.
	if ( activeWorld->cameraZoom ) {
		Camera::pX = activeWorld->cameraPos.x;
		Camera::pY = activeWorld->cameraPos.y;
		Camera::setZoom(activeWorld->cameraZoom);
	}	
	Camera::calculateLimit();
	Camera::calculateVisibleChunks();

	// Flags.
	//flags = 0; // Can be done faster
	//flags |= WORLD_LOADED;
	//flags |= WORLD_EDITOR; // THIS SHOULD ONLY BE FOR EDITOR WORLDS. FIX THIS.
	
	SSSelect::deselect();


	Renderer::renderFlags |= RENDER_REDRAW_TURF;
}

World* World::getWorld(short ID) {
	return worlds.at(ID);
}

// Deletes all worlds.
void World::clear() {
	for ( std::pair<short, World*> world : worlds )
		delete(world.second);

	worlds.clear();

	TurfRenderer::invalidTurfs.clear();

	Master::gameState = GAMESTATE_MAIN_MENU; // TODO move someplace else.

	SSUI::clearScene();
	//new GUI_Scene_Main_Menu;

	 //Delete mobs and empty mob array.
	if ( entities.size() )
		PRINT("World::clear failed to delete all entities (" << entities.size() << " remaining).");
}

//Chunk* worldPosToChunk(glm::vec2 worldPos) {
//	
//}

World::~World() {
	for ( int y = 0; y < height; y++ ) {
		for ( int x = 0; x < width; x++ ) {
			delete(turfs[x][y]);
		}
	}
}