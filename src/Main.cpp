#define STB_IMAGE_IMPLEMENTATION

#include <iostream>
#include <stb_image.h>
#include <glm/gtc/matrix_transform.hpp>

#include "Main.h"
#include "global/ResourceManager.h"
#include "rendering/Renderer.h"
#include "global/Camera.h"
#include "rendering/FrameBuffer.h"
#include "io/Keyboard.h"
#include "io/Mouse.h"
#include "io/SSSelect.h"
#include "io/ClickMask.h"
#include "gui/font/SSText.h"
#include "Test.h"
#include "atom/turf/Plating.h"
#include "lighting/SSLighting.h"
#include "abstracts/SSRooms.h"
#include "other/Gamemode_Editor.h"
#include "global/World.h"
#include "subsystems/Master.h"
#include "subsystems/SSPathing.h"
#include "gui/SSUI.h"
#include "subsystems/SSEntity.h"
#include "subsystems/SSSprite.h"
#include "other/aura/SSAura.h"
#include "gui/scenes/GUI_Scene_Debug.h"
#include "gui/scenes/GUI_Scene_Test.h"
#include "subsystems/SSEffect.h"
#include "subsystems/SSProximity.h"
#include "subsystems/SSTimer.h"
#include "ai/pathing/Flow_Pathing.h"
#include "atom/entity/mob/Human.h"
#include "other/Player.h"



GLFWwindow* Main::window;
int Main::width;
int Main::height;
int MAX_FPS, SCREEN_TICK_PER_FRAME;
bool showFramerate;
bool Main::isResizing;
int Main::resize_newWidth, Main::resize_newHeight;
bool Main::resize_skipWindowResize;

// timing
float Main::deltaTime = 0.0f;	// time between current frame and last frame
float Main::currentTime = 0.0f;	// application lifetime
float Main::nextFPSPrint = 0.0f; // When to next print the FPS

bool Main::vSync;
Text* FPSText;
Text* DeltaText;
Text* CreditText;

Player* Main::player;

// Game loop
void Main::loop() {
	float accumulator = 0;
	float newTime = 0; // When the last frame happened

	//deltaTime = .01f; // almost 1.0f / 60.0f;

	int forcedFramerate = 0;
	int sims = 3;
	//forcedFramerate = 10;


	while (!glfwWindowShouldClose(window)) {
		// per-frame time logic
		currentTime = (float)glfwGetTime();
		deltaTime = currentTime - newTime;
		//if ( deltaTime > 0.25 )
		//	deltaTime = 0.25;

		accumulator += deltaTime;
		newTime = currentTime;

		if (showFramerate) {
			printFPS();
		}

		// Input
		// -----
		glfwPollEvents();
		SSUI::updateHover();
		Mouse::calculateMousePos();
		Keyboard::process();
		Camera::update();

		sims = 3;
		while ( accumulator >= FIXED_TIMESTEP && sims ) {
			sims--;
			

			Master::fire();
			SSText::update(FIXED_TIMESTEP);

			//std::this_thread::sleep_for(std::chrono::nanoseconds(1000000));

			//PRINT(accumulator);


			accumulator -= FIXED_TIMESTEP;
		}

		magic = accumulator / FIXED_TIMESTEP;

		//if ( !sims ) {
		//	PRINT("FALLING BEHIND");
		//}


		// Render
		// ------
		Renderer::draw();

		// Lag simulation.
		if ( forcedFramerate )
			std::this_thread::sleep_for(std::chrono::milliseconds((int) (1000 / forcedFramerate)));

		// glfw: swap buffers and poll IO events (keys pressed/released, mouse moved etc.)
		// -------------------------------------------------------------------------------
		glfwSwapBuffers(window);

		if (isResizing) {
			resize();
		}
	}
}

bool Main::init()
{
	// Generates seed for rand. Does this do anything?
	srand(static_cast <unsigned> (time(0)));

	//width = 640;
	//height = 360;
	width = 1280;
	height = 720;
	//width = 1920;
	//height = 1080;
	MAX_FPS = 900;
	SCREEN_TICK_PER_FRAME = 1000 / MAX_FPS;
	showFramerate = true;
	vSync = true;
	//running = true;

	initGL();

	player = new Player();
	player->credits = 9000;

	// --- SUBSYSTEMS ---
	FrameBuffer::init();
	Renderer::init();

	// * = No init dependancies.
	new SSSprite();
	ResourceManager::init(); // SSSprite
	Keyboard::init(); // *
	Mouse::init(); // *
	ClickMask::init(); // *
	SSRooms::init(); // *
	Camera::init(); // *
	SSText::init(); // *
	SSLighting::init(); // *
	
	new SSUI();
	Master::SSPathing = new SSPathing();
	new SSEntity();
	new SSEffect();
	new SSAura();
	new SSProximity();
	new SSTimer();

	//World* ship = new World(WORLD_SHIP, 30, 30);

	World* ship = new World(WORLD_SHIP, "maps/ships/ship02");
	//World* planet = new World(WORLD_PLANET, "maps/ships/desert");
	World::switchWorld(ship);
	Master::SSPathing->generatePrePathing();

	//Human* human = new Human(ship, { 2 * UNITS_PER_TILE, 2 * UNITS_PER_TILE });
	//Human* human2 = new Human(ship, { 52 * UNITS_PER_TILE, 42 * UNITS_PER_TILE });
	//Human* human3 = new Human(ship, { 5 * UNITS_PER_TILE, 5 * UNITS_PER_TILE });
	//Human* human4 = new Human(ship, { 5 * UNITS_PER_TILE, 15 * UNITS_PER_TILE });
	//new Human(ship, { 15 * UNITS_PER_TILE, 25 * UNITS_PER_TILE });
	//new Human(ship, { 15 * UNITS_PER_TILE, 35 * UNITS_PER_TILE });


	//std::vector<Mob*> mobs = { human, human2, human3, human4 };

	//Flow_Pathing::requestPath(mobs, { 25, 25 });
	//Flow_Pathing::requestPath(mobs, { 12, 55 });

	//World::switchWorld(planet);
	

	//World* world = new World(WORLD_SHIP, 100, 100);
	//World::switchWorld(world);
	//World::saveWorld("ships/desert");

	// Debug.
	Test::init();
	FPSText = SSText::newText("0", 0, 0, .25, glm::vec4(1, 1, 1, 1), true);
	DeltaText = SSText::newText("0", 0, 25, .25, glm::vec4(1, 1, 1, .0001), true);
	CreditText = SSText::newText("0", 0, 50, .25, glm::vec4(1, 1, 1, .0001), true);

	Master::gameState = GAMESTATE_IN_WORLD;
	//new GUI_Scene_Debug;
	//new GUI_Scene_Test;

	return 1;
}

bool Main::initGL()
{
	bool success = true;

	// ------------------------------
	glfwInit();
	glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 4);
	glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 6);
	glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);

	#ifdef __APPLE__
		glfwWindowHint(GLFW_OPENGL_FORWARD_COMPAT, GL_TRUE);
	#endif

	// glfw window creation
	// --------------------
	glfwWindowHint(GLFW_VISIBLE, false);
	window = glfwCreateWindow(width, height, TITLE, NULL, NULL);
	centreWindow();
	glfwShowWindow(window);

	if (window == NULL)
	{
		std::cout << "Failed to create GLFW window" << std::endl;
		glfwTerminate();
		//return -1;
	}
	glfwMakeContextCurrent(window);

	if (!vSync) {
		glfwSwapInterval(0);
	}

	glfwSetFramebufferSizeCallback(window, resize_callback);

	// glad: load all OpenGL function pointers
	// ---------------------------------------
	if (!gladLoadGLLoader((GLADloadproc)glfwGetProcAddress))
	{
		std::cout << "Failed to initialize GLAD" << std::endl;
		//return -1;
	}

	GLFWimage images[1];
	images[0].pixels = stbi_load("assets/sprites/icon.png", &images[0].width, &images[0].height, 0, 4); //rgba channels 
	glfwSetWindowIcon(window, 1, images);
	stbi_image_free(images[0].pixels);

	return success;
}

void Main::resize() {
	width = resize_newWidth;
	height = resize_newHeight;

	// Resize and centre window.
	if (!resize_skipWindowResize) {
		centreWindow();
	}

	// Make sure viewport is inside World.
	Camera::calculateZoomLimits();
	Camera::zoomCameraIntoBounds();
	Camera::calculateMoveLimits();
	Camera::moveCameraIntoBounds();

	// Reset framebuffers with new size.
	
	delete(Renderer::staticDrawFBOA);
	delete(Renderer::staticDrawFBOB);
	delete(Renderer::worldFBO);
	delete(ClickMask::maskFBO);
	delete(SSLighting::lightFBO);
	delete(SSUI::GUIFBO);

	Renderer::staticDrawFBOA = new FrameBuffer();
	Renderer::staticDrawFBOB = new FrameBuffer();
	Renderer::worldFBO = new FrameBuffer();
	ClickMask::maskFBO = new FrameBuffer();
	SSLighting::lightFBO = new FrameBuffer();
	SSUI::GUIFBO = new FrameBuffer();

	// Update main shader projection
	glm::mat4 projection = glm::ortho(0.0f, (float)Main::width, (float)Main::height, 0.0f, -500.0f, 1000.0f);

	Renderer::shapeShader.use();
	Renderer::shapeShader.setMat4("projection", projection);

	SSText::fontShader.use();
	SSText::fontShader.setMat4("projection", projection);

	Renderer::ourShader.use();
	Renderer::ourShader.setMat4("projection", projection);

	BIT_1(Renderer::renderFlags, RENDER_REDRAW_TURF);
	BIT_1(Renderer::renderFlags, RENDER_RECALC_GUI);
	BIT_1(Renderer::renderFlags, RENDER_REDRAW_GUI);

	// GUI
	SSUI::main->resize();

	isResizing = false;
}

void Main::prepareResize(int width, int height, bool skipResize) {
	isResizing = true;
	resize_newWidth = width;
	resize_newHeight = height;
	resize_skipWindowResize = skipResize;
}

// glfw: whenever the window size changed (by OS or user resize) this callback function executes
// ---------------------------------------------------------------------------------------------
void Main::resize_callback(GLFWwindow* window, int width, int height) {
	Main::prepareResize(width, height, true);
}

void Main::centreWindow() {
	GLFWmonitor* monitor = glfwGetPrimaryMonitor();
	const GLFWvidmode* monitor_mode = glfwGetVideoMode(monitor);
	glfwSetWindowMonitor(window, NULL, (monitor_mode->width / 2) - (width / 2), (monitor_mode->height / 2) - (height / 2), width, height, GLFW_DONT_CARE);
	//glfwSetWindowSize(window, SCREEN_WIDTH, SCREEN_HEIGHT); In case I want it later. Does not centre.
}

void Main::printFPS() {

	if (currentTime > nextFPSPrint) {
		FPSText->content = "FPS: " + std::to_string((int)(1.0f / deltaTime));
		FPSText->regenerateMesh();

		DeltaText->content = "Delta: " + std::to_string(deltaTime);
		DeltaText->regenerateMesh();

		CreditText->content = "Credits: " + std::to_string(player->credits);
		CreditText->regenerateMesh();

		nextFPSPrint += .1f; // output 10 times a second
	}
}

// Sets mouse cursor icon. Pass nothing to reset to default.
void Main::setCursor(int shape) {
	glfwDestroyCursor(cursor);
	cursor = NULL;

	//unsigned char pixels[16 * 16 * 4];
	//memset(pixels, 0xff, sizeof(pixels));

	//GLFWimage image;
	//image.width = 16;
	//image.height = 16;
	//image.pixels = pixels;

	//cursor = glfwCreateCursor(&image, 0, 0);

	if (shape) {
		cursor = glfwCreateStandardCursor(shape);
		glfwSetCursor(Main::window, cursor);
	}
}

void Main::close()
{
	World::clear();
	//std::unordered_map<unsigned int, std::shared_ptr<Inhabi>> copy = World::entities;
	//for (std::pair<unsigned int, std::shared_ptr<Mob>> entity : copy) {
	//	World::deleteEntity(entity.second.get());
	//}

	// optional: de-allocate all resources once they've outlived their purpose:
	// ------------------------------------------------------------------------
	//glDeleteVertexArrays(1, &VAO);
	//glDeleteBuffers(1, &VBO);

	// glfw: terminate, clearing all previously allocated GLFW resources.
	// ------------------------------------------------------------------
	glfwTerminate();
	
	//while (true) {}
}

int main()
{
	if (!Main::init())
		printf("Failed to initialise!\n");
	else
		Main::loop();

	Main::close();
	return 0;
}