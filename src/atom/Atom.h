#pragma once

#include "../common/Common.h"
#include "../common/DefinesSprites.h"
#include "../common/DefinesTypes.h"
#include "../common/Directions.h"

class World;

#define ENTITY_CONSTRUCTOR_1 World* world, glm::vec2 position
#define ENTITY_CONSTRUCTOR_2 world, position

class Atom {
public:
	glm::vec2 position, positionOld;
	uint8_t dir = DIR_SMPL2_N; // Should this be in Atom?
	Sprite* sprite = NULL;
	short currentFrame = 0;
	float nextFrame = 0;
	short typeID = 0;

	World* world;
	
	Atom(ENTITY_CONSTRUCTOR_1);
	virtual void setSprite(short label);
	virtual void updateSpriteState();
	virtual void setDir(uint8_t newDir);
};