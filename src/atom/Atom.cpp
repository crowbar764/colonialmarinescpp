#include "atom.h"
#include "../io/ClickMask.h"
#include "../io/SSSelect.h"
#include "../global/World.h"
#include "../Main.h"
#include "../global/Atlas.h"

Atom::Atom(ENTITY_CONSTRUCTOR_1) {
	this->world = world;
	this->position = position;
	this->positionOld = position;
}

//void Entity::init() {
//	if ( !sprite )
//		setSprite(SPRITE_MISSING);
//}

void Atom::setSprite(short label) {
	sprite = Atlas::mainAtlas->getSprite(label);
	currentFrame = 0;

	if ( sprite->frameRate )
		nextFrame = Main::currentTime + sprite->frameRate;
}

// Set the correct spritesheet sprite based on direction and animation progress.
// It's complicated because it's a 2D spritesheet stored in a 1D array.
void Atom::updateSpriteState() {
	// If the sprite is directional, then offset the Y based on the current direction.
	// * .5f is to convert the entity's 8 direction system to currently implimented 4 direction sheets. Imperfect.
	int directionalOffset = sprite->directional ? (((int) floor(dir * .5f)) * sprite->frameCount) : 0;

	//PRINT(directionalOffset);

	// Basic logic: vector_index_to_use = (direction * total_animation_frames) + anim_state
	// Modulo at the end ensures the animation wraps and loops.
	currentFrame = directionalOffset + (currentFrame % sprite->frameCount);
}

// Input: Cardinal direction (0 for N, 1 for NW..)
void Atom::setDir(uint8_t newDir) {
	if ( dir == newDir ) 
		return;

	// Sanity.
	if ( newDir >= DIR_SMPL2_NONE ) {
		PRINT_ERROR("Setting atom dir to " << (int) newDir << " (max is " << (int) DIR_SMPL2_NONE - 1 << ").");
		abort();
	}

	dir = newDir;
	updateSpriteState();
}