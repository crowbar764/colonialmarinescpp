#pragma once
#include "Atom.h"

class Turf;

class Inhabitant : public Atom {
public:
	unsigned int ID;
	inline static unsigned int lastID = 1;

	
	float spriteResolution = 1.0f;
	float rotation;
	Faction faction;




	

	Inhabitant(ENTITY_CONSTRUCTOR_1) : Atom(ENTITY_CONSTRUCTOR_2) {
		ID = lastID++;
	}

	virtual void tick() {

	}

	

	virtual ~Inhabitant() {

	}
};