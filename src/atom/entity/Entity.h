#pragma once
#include "../Inhabitant.h"
#include <unordered_map>

struct Chunk;

class Entity : public Inhabitant {
public:
	std::vector<Chunk*> chunks;
	int health = 100;
	bool isSelected, isSelectable;
	float lastDrawn;
	std::vector<Turf*> locs;
	std::unordered_map<short, Sprite*> overlays;

	Entity(ENTITY_CONSTRUCTOR_1);
	virtual void damage(int amount);
	void updateLocs();
	virtual ~Entity();
};