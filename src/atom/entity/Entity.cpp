#include "Entity.h"
#include "../../global/World.h"
#include "../../io/SSSelect.h"
#include "../turf/Turf.h"

Entity::Entity(ENTITY_CONSTRUCTOR_1) : Inhabitant(ENTITY_CONSTRUCTOR_2) {
	updateLocs();
}

void Entity::damage(int amount) {
	health -= amount;

	if ( health < 1 )
		World::deleteEntity(this);
}

void Entity::updateLocs() {
	// Unregister old position (if we have one).
	if ( locs.size() ) {
		for ( Turf* turf : locs )
			turf->removeOccupant(this);

		for ( Chunk* chunk : chunks )
			vErase(chunk->occupants, this); // TRY MAKING FAST ERASE FOR POINTER.

		locs.clear();
		chunks.clear();
	}

	// Loc is based on where the sprite is, not the entity origin.
	glm::vec2 spritePosition = position - (spriteResolution * .5f);

	// If entity is not exactly aligned to grid, we need another loc unit in that direction.
	bool needsExtraX = (ceil(spritePosition.x) != spritePosition.x);
	bool needsExtraY = (ceil(spritePosition.y) != spritePosition.y);

	// Calculate final start / end grid points for our loc area. E.g. for X: 5 to 9
	int startX = (int) floor(spritePosition.x);
	int endX = startX + (int)ceil(spriteResolution) + needsExtraX;
	endX = std::min(endX, world->width);

	int startY = (int) floor(spritePosition.y);
	int endY = startY + (int)ceil(spriteResolution) + needsExtraY;
	endY = std::min(endY, world->height);

	//PRINT(needsExtraX << " (" << position.x << ")");

	// Add this mob to new turf(s).
	for ( int y = startY; y < endY; ++y ) {
		for ( int x = startX; x < endX; ++x ) {
			//PRINT(x << " " << y);
			Turf* turf = world->turfs[x][y];
			turf->addOccupant(this);
			locs.push_back(turf);
		}
	}

	// CHUNKS
	// ------

	// -1 blindly added. Seems to work.
	// @TODO check this doesn't overflow map bounds?
	int chunkStartX = startX / CHUNK_SIZE;
	int chunkStartY = startY / CHUNK_SIZE;
	int chunkEndX = (endX - 1) / CHUNK_SIZE; 
	int chunkEndY = (endY - 1) / CHUNK_SIZE;

	// Add this mob to new chunk(s).
	for ( int y = chunkStartY; y <= chunkEndY; ++y ) {
		for ( int x = chunkStartX; x <= chunkEndX; ++x ) {
			world->chunks[x][y]->occupants.push_back(this);
			chunks.push_back(world->chunks[x][y]);
		}
	}
}

Entity::~Entity() {
	if ( isSelected )
		SSSelect::deselectSingle(this);
}