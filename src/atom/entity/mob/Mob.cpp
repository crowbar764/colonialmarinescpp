#include "Mob.h"
#include "../../../other/weapon/Weapon.h"
#include "../../../global/World.h"
#include "../../../ai/pathing/PathPackage.h"
#include "../../../ai/BehavourTree.h"
#include "../../turf/Turf.h"

Mob::Mob(ENTITY_CONSTRUCTOR_1) : Entity(ENTITY_CONSTRUCTOR_2) {
	//easeDestination = position;
	isSelectable = true;

	//reserveTurf(true);

	World::addEntity(this);
}

void Mob::tick() {
	movementTick();
}

Mob::~Mob() {
	delete(ai);
	delete(signals);
	delete(modifiers);

	for ( Weapon* weapon : weapons )
		delete weapon;

	//if ( moveRequest )
		//AStar::deleteRequest(this);

	//if ( movePath)
		//AStar::deletePath(movePath);

	for ( Turf* turf : locs )
		turf->removeOccupant(this);
}