#pragma once
#include "../entity.h"
#include "../../../other/aura/Modifiers.h"
#include "../../../components/Comp_FlowFollower.h"
#include "../../../components/Comp_Signals.h"
#include "../../../common/IntPair.h"

class Weapon;
class Timer;
struct F_PathingRequest;
class Weapon;
class PathPackage;
struct PathPackageSlot;
class BehavourTree;

class Mob: public Entity, public FlowFollower
{
public:
	glm::vec2 velocity = { 0, 0 };
	float speed = 4.0f;

	BehavourTree* ai = NULL;
	AiState aiState = AiState::IDLE;
	float sleepEnd = 0;
	int range = 3;
	std::weak_ptr<Entity> targetRef;
	std::vector<Weapon*> weapons;

	short movementMode = MOVEMENT_MODE_NONE;
	PathPackageSlot* groupOffset;
	IntPair moveDestination;
	PathPackage* movePath = NULL;
	F_PathingRequest* moveRequest = NULL;

	SignalManager* signals = new SignalManager();
	Modifiers* modifiers = new Modifiers;

	Mob(ENTITY_CONSTRUCTOR_1);
	virtual void tick();
	void movementTick();
	virtual ~Mob();
	bool prepareMoveOrder(int x, int y, bool skipChecks = false);
	void onPathFound(PathPackage* path);
	bool checkMovementForCollision(glm::vec2 imminentPosition);
	void escapeWalls();
	static void moveTo(std::vector<Mob*>, int x, int y, bool skipChecks = false);

	// Boid shit.
	glm::vec2 boid_followFlow();
	glm::vec2 boid_arrive();
};