#pragma once

#include "../Mob.h"
#include "../../../../ai/BT_Hardpoint.h"

class Hardpoint : public Mob
{
public:
	Hardpoint(ENTITY_CONSTRUCTOR_1) : Mob(ENTITY_CONSTRUCTOR_2)
	{
		ai = new BT_Hardpoint(this);
		faction = Faction::HUMAN;
		range = 5;

		weapons.push_back(new Rifle(this));
	}

	void tick() {
		if (aiState == AiState::SLEPT && Main::currentTime < sleepEnd) {
			return;
		}

		aiState = ai->query();

		if (aiState == AiState::DEFEND) {
			
			auto target = targetRef.lock();
			//std::weak_ptr<Entity> target = World::getEntity(targetID);
			rotation = Common::getVectorAngle(position, target->position);

			for (Weapon* weapon : weapons) {
				weapon->use(target);
			}

			//aiState = AiState::SLEPT;
			//sleepEnd = Main::deltaTimeTotal + .25;
		}
	}
};