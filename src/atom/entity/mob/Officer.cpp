#include "Officer.h"

Officer::Officer(ENTITY_CONSTRUCTOR_1) : Human(ENTITY_CONSTRUCTOR_2) {
	aura->category = AURA_OFFICER;
	modifiers->immuneTo = AURA_OFFICER;
}

Officer::~Officer() {
	delete(aura);
}