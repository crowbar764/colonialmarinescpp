#pragma once

#include "Mob.h"
#include "../../../ai/BT_Alien.h"
#include "../../../subsystems/SSPathing.h"
#include "../../../other/weapon/Claws.h"

class Alien : public Mob
{
public:
	Alien(ENTITY_CONSTRUCTOR_1) : Mob(ENTITY_CONSTRUCTOR_2)
	{
		setSprite(SPRITE_DEBUG);
		ai = new BT_Alien(this);
		faction = Faction::ALIEN;
		range = 20;
		speed = 64.0f;
		spriteResolution = 16.0f;
		updateLocs();


		weapons.push_back(new Claws(this));
	}

	//void ohFuck() {
	//	PRINT("OH FUCK!");
	//	std::shared_ptr<Entity> target = targetRef.lock();
	//	IntPair targetPos = worldToGrid((int) target->position.x, (int) target->position.y);
	//	moveTo(targetPos.x, targetPos.y);
	//}

	void tick() {
		return;
		Mob::tick();
		return;

	/*	if ( aiState == AiState::SLEPT && Main::deltaTimeTotal < sleepEnd ) {
			return;
		}*/
		
		aiState = ai->query();

		if ( aiState == AiState::DEFEND ) {
			//PRINT(Main::deltaTimeTotal);
			//std::shared_ptr<Entity> target = targetRef.lock();

			//float distance = getVectorDistance(glm::vec2(position.x, position.y), glm::vec2(target->position.x, target->position.y));

			//if ( distance > 64 ) {
				//IntPair targetPos = worldToGrid((int) target->position.x, (int) target->position.y);
				//moveTo(targetPos.x, targetPos.y);

				//signals->registerSignal(target->signals, SIGNAL_TEST, std::bind(&Alien::ohFuck, this));

			//} else {
				//for (Weapon* weapon : weapons) {
					//weapon->use(target);
				//}
				//aiState = AiState::SLEPT;
				//sleepEnd = Main::deltaTimeTotal + .25;
			//}

			//Entity* target = World::getEntity(targetID);



			//aiState = AiState::SLEPT;
			//sleepEnd = Main::deltaTimeTotal + .25;
		}
	}
};