#pragma once
#include "Human.h"
#include "../../../other/aura/Aura.h"

class Officer : public Human {
public:
	Aura* aura = new Aura(this);

	Officer(ENTITY_CONSTRUCTOR_1);
	~Officer();
};