#pragma once

#include "Structure.h"

class Depot : public Structure {
public:

	Depot(ENTITY_CONSTRUCTOR_1) : Structure(ENTITY_CONSTRUCTOR_2)
	{
		ai = new BT_Marine(this);
		faction = Faction::HUMAN;
	}

	void tick() {
		//Mob::tick();

		if (aiState == AiState::SLEPT && Main::currentTime < sleepEnd) {
			return;
		}

		sleepEnd = Main::currentTime + 1.0f;

		//Human* human = new Human((int)MobType::MARINE, position, "marine", true);
		//human->moveTo(5, 5);
	}
};