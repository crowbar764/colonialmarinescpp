#pragma once

#include "Structure.h"

class Hive : public Structure {
public:

	Hive(ENTITY_CONSTRUCTOR_1) : Structure(ENTITY_CONSTRUCTOR_2)
	{
		ai = new BT_Marine(this);
		faction = Faction::ALIEN;
	}

	void tick() {
		//Mob::tick();

		if (aiState == AiState::SLEPT && Main::currentTime < sleepEnd) {
			return;
		}
	}
};