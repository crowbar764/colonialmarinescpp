#pragma once
#include "Officer.h"
#include "../../../global/Atlas.h"

class XO : public Officer {
public:
	XO(ENTITY_CONSTRUCTOR_1) : Officer(ENTITY_CONSTRUCTOR_2) {
		overlays.emplace(SPRITE_RANK, Atlas::mainAtlas->getSprite(SPRITE_LOWRANK));
		aura->flag = AURA_OFFICER_XO;
		aura->speed = 4;
	}
};