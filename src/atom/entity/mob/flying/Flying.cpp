#pragma once
#include "Flying.h"
#include "../Human.h"
#include "../../../../ai/BT_Marine.h"
#include "../../../../other/weapon/Rifle.h"
#include "../../../../other/weapon/Autocannon.h"
#include "../../../../common/Math.h"

Flying::Flying(ENTITY_CONSTRUCTOR_1) : Mob(ENTITY_CONSTRUCTOR_2) {
	ai = new BT_Marine(this);
	faction = Faction::ALIEN;
	range = 15;

	accellerationSpeed = 150.0f;
	maxSpeed = 150.0f;
	breakingSpeed = 4.0f;

	//weapons.push_back(new Rifle(this));
	//weapons.push_back(new Autocannon(this));
	//weapons.push_back(new Cannon(this));

}

void Flying::tick() {

	//PRINTV("Vel", velocity);

	// If we have a move target.
	if (moveDestination.x || moveDestination.y) {

		float angleToTarget;

		// If orbitting, rotate to broadside.
		if (orbitMode) {
			angleToTarget = getVectorAngle(position, targetPos);
			rotation = angleToTarget - (PI / 2.0f);
		}

		// If we arrived...
		if (processMove()) {
			// If orbitting, set targetPos to next 'point' of the radius.
			if (orbitMode) {
				moveDestination = {
					targetPos.x + (cos(angleToTarget - (PI / 2)) * 200),
					targetPos.y + (sin(angleToTarget - (PI / 2)) * 200)
				};

				//new Human((int)MobType::MARINE, moveDestination, "marine", true);
			} else { // Otherwise end the move order.
				moveDestination = { 0, 0 };
			}
		}
	}








	return;

	aiState = ai->query();

	if ( movePath)
		processMove();

	if (aiState == AiState::DEFEND) {
		auto target = targetRef.lock();

		position.x = target->position.x + (cos(Main::currentTime) * 300);
		position.y = target->position.y + (sin(Main::currentTime) * 300);
		rotation = getVectorAngle(position, target->position);

		for (Weapon* weapon : weapons) {
			weapon->use(target);
		}

		//aiState = AiState::SLEPT;
		//sleepEnd = Main::deltaTimeTotal + .25;
	}

	aiState = ai->query();

	if ( movePath)
		processMove();
}

void Flying::moveTo(int x, int y) {
	moveDestination = { x, y };
	targetPos = { x, y };

	if (orbitMode) {
		arrivalThreshold = 100.0f;
	}
	else {
		arrivalThreshold = 1.0f;
	}
}

bool Flying::processMove() {
	glm::vec2 breakingDistance = { abs(velocity.x) / breakingSpeed, abs(velocity.y) / breakingSpeed };
	glm::vec2 distanceToDestination = { abs(position.x - moveDestination.x), abs(position.y - moveDestination.y) };
	float angleToTarget = getVectorAngle(position, moveDestination);

	// We have arrived.
	if (distanceToDestination.x + distanceToDestination.y < arrivalThreshold)
		return 1;

	// Are we further than the stopping distance? If so, add velocity. If not, subtract velocity.
	if (distanceToDestination.x > breakingDistance.x) {
		
		velocity.x += cos(angleToTarget) * accellerationSpeed * FIXED_TIMESTEP;
	} else {
		velocity.x -= velocity.x / ((1.0f / breakingSpeed) / FIXED_TIMESTEP);
	}

	if (distanceToDestination.y > breakingDistance.y) {
		float distanceRatio = 0.75f;
		velocity.y += sin(angleToTarget) * accellerationSpeed * FIXED_TIMESTEP * distanceRatio;
	} else {
		velocity.y -= velocity.y / ((1.0f / breakingSpeed) / FIXED_TIMESTEP);
	}

	// Clamp velocity.
	velocity.x = std::clamp(velocity.x, -maxSpeed, maxSpeed);
	velocity.y = std::clamp(velocity.y, -maxSpeed, maxSpeed);

	// Apply velocity to position.
	position.x += velocity.x * FIXED_TIMESTEP;
	position.y += velocity.y * FIXED_TIMESTEP;
	
	return 0;
}

void Flying::moveToOrbitPoint(float x, float y) {


	//position.x = cos(angleToTarget) * 64;
	//position.y = sin(angleToTarget) * 64;

	//PRINT("after " << position.x << " " << position.y << " " << angleToTarget);
	//rotation = angleToOrbit;
	//position.x += cos(rotation) / 20;
	//position.y += sin(rotation) / 20;
	//glm::vec2 targetPosition = targetRef.lock()->position;

	//position = glm::vec2(
	//	targetPosition.x + (sin(Main::deltaTimeTotal * 2) * 64),
	//	targetPosition.y + (cos(Main::deltaTimeTotal * 2) * 64)
	//);
}