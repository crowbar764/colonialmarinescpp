#pragma once
#include <algorithm>
#include "../Mob.h"

class Flying : public Mob {
public:
	glm::vec2 moveDestination, velocity;
	float accellerationSpeed, maxSpeed, breakingSpeed;
	float arrivalThreshold = 1.0f;
	glm::vec2 targetPos;
	bool orbitMode;

	Flying(ENTITY_CONSTRUCTOR_1);
	void tick();
	void moveTo(int x, int y);
	bool processMove();
	void moveToOrbitPoint(float x, float y);
};