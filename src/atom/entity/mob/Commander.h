#pragma once
#include "Officer.h"

class Commander : public Officer {
public:
	Commander(ENTITY_CONSTRUCTOR_1) : Officer(ENTITY_CONSTRUCTOR_2) {
		overlays.emplace(SPRITE_RANK, Atlas::mainAtlas->getSprite(SPRITE_RANK));
		aura->flag = AURA_OFFICER_COMMANDER;
		aura->speed = 1;
	}
};