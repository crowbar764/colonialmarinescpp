#pragma once

#include "Mob.h"
#include "../../../ai/BT_Marine.h"
#include "../../../other/weapon/Rifle.h"

class Human : public Mob
{
public:
	Human(ENTITY_CONSTRUCTOR_1) : Mob(ENTITY_CONSTRUCTOR_2) {
		setSprite(SPRITE_MARINE);
		ai = new BT_Marine(this);
		faction = Faction::HUMAN;
		//spriteResolution = 128;
		//speed = .165f;
		speed = .4f;

		weapons.push_back(new Rifle(this));
	}

	void tick() {
		Mob::tick();

		if ( Main::currentTime < sleepEnd )
			return;

		aiState = ai->query();

		//if ( aiState == AiState::IDLE ) {
		//	sleepEnd = Main::deltaTimeTotal + 2;
		//} else {
		//	float r = static_cast <float> (rand()) / static_cast <float> (RAND_MAX);
		//	sleepEnd = Main::deltaTimeTotal + .5f + r;
		//}

		//float r = static_cast <float> (rand()) / static_cast <float> (RAND_MAX);
		//sleepEnd = Main::deltaTimeTotal + r;
		//PRINT(Main::deltaTimeTotal << " " << sleepEnd);


		//return;
		if (aiState == AiState::DEFEND) {
			
			std::shared_ptr<Entity> target = targetRef.lock();
			//PRINT(ID);
			for (Weapon* weapon : weapons) {
				weapon->use(target);
			}

			//aiState = AiState::SLEPT;
			//sleepEnd = Main::deltaTimeTotal + .25f;
		}
	}
};