#include "MarkerBig.h"
#include "../../subsystems/SSEffect.h"


MarkerBig::MarkerBig(ENTITY_CONSTRUCTOR_1, short sprite) : Effect(ENTITY_CONSTRUCTOR_2) {
	setSprite(sprite);

	timer = new Timer(.25, std::bind(&MarkerBig::finished, this));
}

void MarkerBig::finished() {
	SSEffect::deleteEffect(this);
}