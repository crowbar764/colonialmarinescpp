#pragma once
#include "Effect.h"
#include "../../other/Timer.h"

class MarkerBig : public Effect {
public:
	Timer* timer;

	MarkerBig(ENTITY_CONSTRUCTOR_1, short sprite = SPRITE_MARKER);
	void finished();
};