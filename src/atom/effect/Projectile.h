#pragma once
#include "Effect.h"
#include "../../other/Easing.h"
#include "../entity/Entity.h"

class Projectile : public Effect
{
public:
	float velocity;
	EaseVector* ease;
	std::weak_ptr<Entity> targetRef;
	int damage;

	virtual void tick();
	Projectile(ENTITY_CONSTRUCTOR_1, std::shared_ptr<Entity> target);
	~Projectile();
};