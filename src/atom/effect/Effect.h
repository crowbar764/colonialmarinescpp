#pragma once
#include "../inhabitant.h"

class Effect : public Inhabitant
{
public:
	bool shouldDraw = true;
	//IntPair position;
	//IntPair dimensions; // For collision rotx/y calculation. Not tested out of standard 32.
	//SDL_Rect collisionBox;

	Effect(ENTITY_CONSTRUCTOR_1);
	virtual void tick();
	void cullCheck();
};