#include "Projectile.h"
#include<numeric>
#include "../../global/World.h"
#include "../../subsystems/SSEffect.h"
#include "../../common/Math.h"

Projectile::Projectile(ENTITY_CONSTRUCTOR_1, std::shared_ptr<Entity> target) : Effect(ENTITY_CONSTRUCTOR_2) {
	setSprite(SPRITE_BULLET);

	//spriteResolution = 2;
	velocity = 500; // 700 for bullet
	damage = 0;
	targetRef = target;

	// Destination point is the centre of the target sprite.
	glm::vec2 targetCentre = target->position + glm::vec2((target->spriteResolution * .5f) - (spriteResolution * .5f));

	float distance = getVectorDistance(position, targetCentre);
	float duration = distance / velocity;
	ease = new EaseVector(&this->position, targetCentre, duration);
	rotation = getVectorAngle(position, targetCentre);
}

void Projectile::tick() {
	//Effect::tick();

	//												THIS IS WHY THIS IS BROKEN. NEW SYSTEM.
	if (!ease->isFinished())
		return;

	if (auto target = targetRef.lock())
		target->damage(damage);

	SSEffect::deleteEffect(this);
}

Projectile::~Projectile() {
	//												THIS IS WHY THIS IS BROKEN. NEW SYSTEM.
	delete(ease);
	ease = NULL;
}