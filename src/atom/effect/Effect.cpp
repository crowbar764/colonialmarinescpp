#include "Effect.h"
#include "../../subsystems/SSEffect.h"
#include "../../global/World.h"
#include "../../global/Camera.h"

constexpr int EFFECT_CULL_MARGIN = 3;

Effect::Effect(ENTITY_CONSTRUCTOR_1) : Inhabitant(ENTITY_CONSTRUCTOR_2) {
	SSEffect::addEffect(this);

	//PRINT(position.x);
	cullCheck();
}

void Effect::tick() {
}

// Check if this effect is far off-screen. If it is, never render it, ever.
void Effect::cullCheck() {
	int gridX = (int) position.x / UNITS_PER_TILE;
	int gridY = (int) position.y / UNITS_PER_TILE;

	if ( 
		gridX < Camera::gridBounds.x - EFFECT_CULL_MARGIN ||
		gridY < Camera::gridBounds.y - EFFECT_CULL_MARGIN ||
		gridX > Camera::gridBounds.z + EFFECT_CULL_MARGIN ||
		gridY > Camera::gridBounds.w + EFFECT_CULL_MARGIN
		) {
		shouldDraw = false;
	}
}