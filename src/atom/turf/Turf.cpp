#include "Turf.h"
#include <iostream>
#include "../../rendering/TurfRenderer.h"
#include "../../global/World.h"
#include "../../common/VectorHelpers.h"

Turf::Turf(ENTITY_CONSTRUCTOR_1) : Atom(ENTITY_CONSTRUCTOR_2) {
	// Fix this so it doesn't double-draw on first frame.
	invalidate();
}

void Turf::setSprite(short label) {
	Atom::setSprite(label);
	TurfRenderer::invalidTurfs.push_back(this);
}

// This turf needs redrawing.
void Turf::invalidate() {
	TurfRenderer::invalidTurfs.push_back(this);
}

void Turf::addOccupant(Entity* entity) {
	occupants.push_back(entity);
	hasOccupants = true;
}

void Turf::removeOccupant(Entity* entity) {
	eraseFast(occupants, std::find(occupants.begin(), occupants.end(), entity));

	if ( !occupants.size() )
		hasOccupants = false;
}

Turf::~Turf() {
	for ( Entity* occupant : occupants )
		World::deleteEntity(occupant);
}