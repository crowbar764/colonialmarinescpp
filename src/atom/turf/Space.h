#pragma once

#include "Turf.h"

class Space : public Turf
{
public:
	Space(ENTITY_CONSTRUCTOR_1) : Turf(ENTITY_CONSTRUCTOR_2) {
		typeID = TYPE_SPACE;
		setSprite(SPRITE_SPACE);
	}
};