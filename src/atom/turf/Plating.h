#pragma once

#include "Turf.h"

class Plating : public Turf
{
public:
	Plating(ENTITY_CONSTRUCTOR_1) : Turf(ENTITY_CONSTRUCTOR_2) {
		typeID = TYPE_PLATING;
		setSprite(SPRITE_PLATING);
	}
};