#pragma once
#include "../Atom.h"

class Entity;

class Turf : public Atom {
public:
	bool isOpen = true;
	//short isReserved = 0; // If a mob is directly moving to this tile, block other mobs from pathing through it.
	std::vector<Entity*> occupants;
	bool hasOccupants = false;
	short pathNetworkID; // The ID of the path network this turf occupies.
	bool debugMarked = false;

	Turf(ENTITY_CONSTRUCTOR_1);
	//void init();
	void setSprite(short label);
	void invalidate();
	void addOccupant(Entity* entity);
	void removeOccupant(Entity* entity);
	~Turf();
};