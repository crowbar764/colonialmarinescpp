#pragma once

#include "Turf.h"

class Hull : public Turf
{
public:
	Hull(ENTITY_CONSTRUCTOR_1) : Turf(ENTITY_CONSTRUCTOR_2) {
		typeID = TYPE_HULL;
		setSprite(SPRITE_HULL);
		isOpen = false;
	}
};

class Door : public Turf {
public:
	Door(ENTITY_CONSTRUCTOR_1) : Turf(ENTITY_CONSTRUCTOR_2) {
		typeID = TYPE_DOOR;
		setSprite(SPRITE_DOOR);
		//isOpen = false;
	}
};