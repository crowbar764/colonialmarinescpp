#pragma once

#include "Turf.h"

class Desert : public Turf {
public:
	Desert(ENTITY_CONSTRUCTOR_1) : Turf(ENTITY_CONSTRUCTOR_2) {
		typeID = TYPE_DESERT;
		setSprite(SPRITE_DESERT);
	}
};

class Barrier : public Turf {
public:
	Barrier(ENTITY_CONSTRUCTOR_1) : Turf(ENTITY_CONSTRUCTOR_2) {
		typeID = TYPE_BARRIER;
		setSprite(SPRITE_BARRIER);
	}
};