#pragma once
#include "Turf.h"

class Blank : public Turf
{
public:
	Blank(ENTITY_CONSTRUCTOR_1) : Turf(ENTITY_CONSTRUCTOR_2) {
		typeID = TYPE_BLANK;
		setSprite(SPRITE_BLANK);
	}
};