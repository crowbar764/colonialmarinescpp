#pragma once
#include <functional>
#include <map>

//constexpr int SIGNAL_TEST = 0;
//constexpr int SIGNAL_MOVED = 1;

class SignalManager {
public:
	/**
	* Any datum registered to receive signals from this datum is in this list
	*
	* Lazy associated list in the structure of `signal:registree/list of registrees`
	*/
	// A vector of elements to ping if a signal is sent.
	std::map<int, std::vector<SignalManager*>> comp_lookup;

	/// Lazy associated list in the structure of `signals:proctype` that are run when the datum receives that signal
	std::map<SignalManager*, std::map<int, std::function<void()>>> signal_procs;

	void registerSignal(SignalManager* target, int sigType, std::function<void()> callback) {
		if ( signal_procs[target][sigType] )
			PRINT_WARNING("Signal overriden [" << sigType << "]");
		else
			target->comp_lookup[sigType].push_back(this);

		signal_procs[target][sigType] = callback;
	}

	void unregisterSignal(SignalManager* target, int sigType) {
		std::vector<SignalManager*>* arr = &target->comp_lookup[sigType];
		std::vector<SignalManager*>::iterator it = find(arr->begin(), arr->end(), this);
		arr->erase(it);

		signal_procs[target].erase(sigType);
	}

	void sendSignal(int sigType) {
		std::vector<SignalManager*> targets = comp_lookup[sigType];

		for ( SignalManager* target : targets ) {
			target->signal_procs[this][sigType]();
		}
	}
};