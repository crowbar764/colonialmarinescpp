#include "../atom/entity/mob/Mob.h"
#include "../common/Math.h"
#include "../rendering/Renderer.h"
#include "../ai/pathing/PathPackage.h"

glm::vec2 Mob::boid_followFlow() {
	float MAXSPEED = 4.0f;
	float MAXFORCE = 0.4f;

	// The vector direction the flowfield is pushing us. Already normalised.
	glm::vec2 desired = sampleFlowfield(movePath, position);

	// LOS mode, skip everything.
	if ( desired.x == FLOWFIELD_LOS_CODE )
		return desired;

	if ( desired.x == 0 && desired.y == 0 )
		return { 0, 0 };

	// Steering = Desired minus velocity.
	glm::vec2 steer = (desired * MAXSPEED) - velocity;

	// Limit to maximum steering force. This controls how responsive the agent is.
	limitVector(&steer, MAXFORCE);

	// Limit to maximum steering force (THIS IS SPEED INSTEAD, MATTERS?)
	//limitVector(&steer, MAXSPEED);

	Renderer::drawLine(position, position + steer);

	return steer;
}

glm::vec2 Mob::boid_arrive() {
	float MAXSPEED = 4.0f;
	float MAXFORCE = 0.4f;

	glm::vec2 desired = (glm::vec2) movePath->goal + 0.5f - position; // A vector pointing from the location to the target

	float distance = magnitude(desired);
	// Surely there is something better than this.
	float stoppingDistance = MAXSPEED / 2;

	// Apply less force the closer we are to the target location.
	if ( distance < stoppingDistance ) {
		float magnitude = std::lerp(0.0f, MAXSPEED, distance / stoppingDistance);
		SET_MAGNITUDE(desired, magnitude);
	} else {
		SET_MAGNITUDE(desired, MAXSPEED);
	}

	Renderer::drawLine(position, position + desired);



	// Steering = Desired minus velocity.
	glm::vec2 steer = desired - velocity;

	// Limit to maximum steering force. This controls how responsive the agent is.
	limitVector(&steer, MAXFORCE);

	//Renderer::drawLine(position, position + steer);

	return steer;
}