#include "Comp_FlowFollower.h"
#include "../ai/pathing/PathPackage.h"
#include "../common/Math.h"

// Secret ingredient!!
// Samples four corners of agent to read flowfield.
// Gets average direction, with more bias towards flow tiles we are closer to.
// Returns float of final direction.
// This crashes if the mob is reading from out of bounds, but with map wall padding this should not happen.
// https://gamedev.stackexchange.com/questions/387/how-does-flow-field-pathfinding-work
// (heavily modified to work with '-1' tiles, and fixes north not working)
glm::vec2 FlowFollower::sampleFlowfield(PathPackage* movePath, glm::vec2 position) {
	// Grid pos of NW corner
	int topLeftX = (int) floor(position.x - .5f);
	int topLeftY = (int) floor(position.y - .5f);

	// Basically if we are mostly towards the right tile, it will have rightBias 75% for example.
	float rightBias = abs(position.x - .5f - topLeftX);
	float leftBias = 1.0f - rightBias;
	float bottomBias = abs(position.y - .5f - topLeftY);
	float topBias = 1.0f - bottomBias;

	// Get flowfield instruction for each corner.
	uint8_t fNW = movePath->flowfield[topLeftX][topLeftY];
	uint8_t fNE = movePath->flowfield[topLeftX + 1][topLeftY];
	uint8_t fSW = movePath->flowfield[topLeftX][topLeftY + 1];
	uint8_t fSE = movePath->flowfield[topLeftX + 1][topLeftY + 1];

	// All corners are LOS, let's use arrival boid instead.
	if ( fNW == FLOWFIELD_LOS && fNE == FLOWFIELD_LOS && fSW == FLOWFIELD_LOS && fSE == FLOWFIELD_LOS )
		return { FLOWFIELD_LOS_CODE, 0 };

	// All four corners without flow.
	if ( fNW == FLOW_NONE || fNE == FLOW_NONE || fSW == FLOW_NONE || fSE == FLOW_NONE ) {
		//PRINT_WARNING("Agent missing corner.");
		//return { 0, 0 };
	}

	//PRINT("a " << manhattanDistance(position, movePath->goal));
	//PRINT("b " << manhattanDistance(movePath->goal, position));

	//glm::vec2 a = { 1, 1 };
	//PRINTV("NORMAL", glm::normalize(a));



	// All four corners without flow.
	//if ( fNW + fNE + fSW + fSE == FLOW_NONE * 4 ) {
	//	PRINT_WARNING("Agent in no man's land.");
	//	return { 0, 0 };
	//}

	// Convert float directions into vec2s.
	// This format is needed because magnitude is needed.
	// Magnitude is needed as we are adjusting the strength of directions based on distance to flow tiles and then merging them.
	// otherwise NORTH (0) * 123 still equals 0.
	//glm::vec2 NW, NE, SW, SE;

	//if ( fNW == FLOWFIELD_LOS )
	//	NW = (glm::vec2) movePath->goal - glm::vec2(topLeftX, topLeftY);
	//else
	//	NW = DIR_TABLE[fNW];

	//if ( fNE == FLOWFIELD_LOS )
	//	NE = (glm::vec2) movePath->goal - glm::vec2(topLeftX + 1, topLeftY);
	//else
	//	NE = DIR_TABLE[fNE];

	//if ( fSW == FLOWFIELD_LOS )
	//	SW = (glm::vec2) movePath->goal - glm::vec2(topLeftX, topLeftY + 1);
	//else
	//	SW = DIR_TABLE[fSW];

	//if ( fSE == FLOWFIELD_LOS )
	//	SE = (glm::vec2) movePath->goal - glm::vec2(topLeftX + 1, topLeftY + 1);
	//else
	//	SE = DIR_TABLE[fSE];



	glm::vec2 NW = DIR_TABLE[fNW];
	glm::vec2 NE = DIR_TABLE[fNE];
	glm::vec2 SW = DIR_TABLE[fSW];
	glm::vec2 SE = DIR_TABLE[fSE];


	glm::vec2 averageXTop;
	//If both corners are blocked, do nothing.
	if ( fNW + fNE == (FLOW_NONE * 2) )
		averageXTop = { 0, 0 };
	// If only one corner is blocked, use other corner's direction, without reduction.
	else if ( (fNW == FLOW_NONE) || (fNE == FLOW_NONE) )
		averageXTop = (fNW == FLOW_NONE) ? NE : NW;
	// If both corners have slow, get an interpolated vector based on the biases.
	else
		averageXTop = NW * leftBias + NE * rightBias;

	// Same as above, for south corners.
	glm::vec2 averageXBottom;
	if ( fSW + fSE == (FLOW_NONE * 2) )
		averageXBottom = { 0, 0 };
	else if ( (fSW == FLOW_NONE) || (fSE == FLOW_NONE) )
		averageXBottom = (fSW == FLOW_NONE) ? SE : SW;
	else
		averageXBottom = SW * leftBias + SE * rightBias;

	// Same as above basically, but for merging the top and bottom averages.
	glm::vec2 average;
	if ( !VECTOR_NOT_ZERO(averageXTop) || !VECTOR_NOT_ZERO(averageXBottom) )
		average = averageXTop + averageXBottom;
	else
		average = averageXTop * topBias + averageXBottom * bottomBias;

	return glm::normalize(average);
}