#pragma once

#include <glm/glm.hpp>

class PathPackage;

class FlowFollower {
public:
	glm::vec2 sampleFlowfield(PathPackage* movePath, glm::vec2 position);
};