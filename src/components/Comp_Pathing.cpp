#include "../atom/entity/mob/Mob.h"
#include "../ai/pathing/Flow_Pathing.h"
#include "../other/Timer.h"
#include "../common/Directions.h"
#include "../common/Conversions.h"
#include "../common/Math.h"
#include "../atom/turf/Turf.h"
#include "../global/World.h"
#include "../ai/pathing/PathPackage.h"
//#include "../rendering/Renderer.h"
#include <algorithm>

/*
* MOB PATHING LOGIC:
* 
* > New move command recieved.
* 
* If prepareMoveOrder are complete:
*	Delete current path package.
*	But let mob finish movement ease.
* 
* INCOMPLETE.
*/

//constexpr float MOB_LOC_UPDATE_COOLDOWN = .2f;

// The distance the mob needs to be to the destination before it just finishes.
constexpr float MOB_LOC_FINISH_DISTANCE = 0.25f;

//float getLength(glm::vec2 vec) {
//	return sqrt((vec.x * vec.x) + (vec.y * vec.y));
//}



//void setMagnitude(glm::vec2* vec, float limit) {
//	if ( magnitude(*vec) > limit ) {
//		*vec = glm::normalize(*vec) * limit;
//	}
//}

// Step 1: Add movement from our chosen pathfinding method.
// Step 2: Check projected new position for collisions.
// Step 3: Use projected position.
void Mob::movementTick()	 {
	// If not meant to be gaining velocity, and has no velocity, skip movement simulation.
	if ( !movementMode && !VECTOR_FAST_LENGTH(velocity) )
		return;

	//PRINT(movementMode << " " << VECTOR_FAST_LENGTH(velocity));

	float MAXSPEED = 5.0f;
	float GRIP = 40.0f;	

	glm::vec2 acceleration = { 0, 0 };

	IntPair currentGridPosition = worldToGrid(position);

	if (movementMode == MOVEMENT_MODE_FLOWFIELD ) {
		// Do a broad check of the tile, then a specific distance check.
		if (false && currentGridPosition == movePath->goal && getVectorDistanceSquare(position, movePath->goal + UNITS_PER_TILE_HALF) < MOB_LOC_FINISH_DISTANCE ) {
			// If we are at the destination, stop movement.
			//movePath->removeUnit(this);
			//movePath = NULL;
			//movementMode = MOVEMENT_MODE_NONE;
		} else {
			// Otherwise keep moving.


			// My attempt.
			//if ( flowFieldRadian != FLOW_NONE ) {
			//	flowFieldVector = glm::normalize(flowFieldVector) * 10.0f; // Speed
			//	desiredVelocity = flowFieldVector - velocity;
			//}

			//PRINTV("Desired velocity", desiredVelocity);

			glm::vec2 flowfieldVector = boid_followFlow();
			//if ( flowfieldVector.x == FLOWFIELD_LOS_CODE )
				//acceleration += boid_arrive();
			//else
				//acceleration += flowfieldVector;

			

			//PRINT_CYAN("---------");

			// ---- SEPERATION ----
			//float desiredSeperation = 1.0f;
			//int count = 0;
			//glm::vec2 sum = { 0, 0 };
			//for ( std::pair<unsigned int, std::shared_ptr<Entity>> entity : World::entities ) {
			//	if ( entity.first != this->ID ) {
			//		float distance = getVectorDistance(this->position, entity.second->position);

			//		if ( distance < desiredSeperation ) {
			//			// Get direction away from other mob.
			//			glm::vec2 difference = position - entity.second->position;

			//			// Set the strength of that force to be based on distance.
			//			difference = glm::normalize(difference) * (1.0f / distance);
			//			PRINTV("Sum", sum);

			//			sum += difference;
			//			count++;
			//		}
			//	}
			//}

			//if ( count ) {
			//	// Set magnitude.
			//	sum = glm::normalize(sum) * MAXSPEED * 10.0f;

			//	glm::vec2 steer = sum - velocity;


			//	acceleration += sum;
			//}


			//acceleration += sum * 10.0f;




		}
	}

	// Friction.
	//velocity *= pow(.0001f, FIXED_TIMESTEP);

	// Apply velocity.
	velocity += acceleration;

	//limitVector(&velocity, MAXSPEED); // Max speed.
	
	positionOld = position;
	position += velocity * FIXED_TIMESTEP;

	//PRINT_CYAN("---");
	//PRINTV("Old", positionOld);
	//PRINTV("New", position);
	

	//Update direction.
	//setDir(radiansToDirection(flowFieldRadian));

	// Collision.
	escapeWalls();

	// If slow enough, just stop.
	//if ( !movementMode && VECTOR_FAST_LENGTH(velocity) < 0.1f ) 
		//velocity = { 0.0f, 0.0f };

	updateLocs();
}

// This checks our current position and snaps our location / velocity out of any walls we are overlapping.
void Mob::escapeWalls() {
	// World position of sprite corner (if velocity was applied).
	glm::vec2 topLeft = position - (spriteResolution * .5f);
	// Check all four corners (of projected position) for collisions.
	for ( int y = 0; y < 2; y++ ) {
		for ( int x = 0; x < 2; x++ ) {
			glm::vec2 cornerWorldPosition = topLeft + (spriteResolution * glm::vec2(x, y));
			IntPair cornerGridPosition = worldToGrid(cornerWorldPosition);

			//PRINTV("CURRENT GRID POS", cornerGridPosition);
			Turf* turf = world->turfs[cornerGridPosition.x][cornerGridPosition.y];

			if ( turf->isOpen )
				continue;

			// 1: Get axis distances between turf / mob. E.G. 0.9, 1.0
			glm::vec2 difference = topLeft - glm::vec2(cornerGridPosition);
			glm::vec2 absoluteDifference = { abs(difference.x), abs(difference.y) };

			// 2: Use axis distances to determine which direction is best to escape to (shortest distance best).
			if ( absoluteDifference.x > absoluteDifference.y ) {
				// Based on if we want to impulse left or right, we calculate the impulse. E.G. -0.0125
				// Not sure whether 1 should be tile size of sprite size.
				float impulse = difference.x == absoluteDifference.x ? 1 - absoluteDifference.x : absoluteDifference.x - 1;

				position.x += impulse;
				velocity.x = 0;
			} else {
				float impulse = difference.y == absoluteDifference.y ? 1 - absoluteDifference.y : absoluteDifference.y - 1;

				position.y += impulse;
				velocity.y = 0;
			}
		}
	}
}

void Mob::onPathFound(PathPackage* path) {
	movePath = path;
	moveRequest = NULL;

	//if ( movementMode == MOVEMENT_MODE_FLOWFIELD) {
	//	// If we are moving from an old order, finish it.
	//	if ( moveEase ) {
	//		onPathSegmentComplete();
	//	} else {
	//		startNextPathSegment();
	//	}
	//}
}

// Runs a few checks to see if we can / want to the new move order.
// Does from preperation if we are going ahead with it.
// Returning false ignores the order.
bool Mob::prepareMoveOrder(int x, int y, bool skipChecks) {
	if ( !skipChecks ) {
		if ( movePath && (movePath->goal.x == x) && (movePath->goal.y == y) ) {
			//PRINT("Already moving to this location!");
			return false;
		}
		if ( ((position.x / UNITS_PER_TILE) == x) && ((position.y / UNITS_PER_TILE) == y) ) {
			//PRINT("Already at to this location!");
			return false;
		}
	}

	// We are considered accepting the request at this point.

	// If waiting for a path, abandon the request.
	if ( moveRequest ) {
		moveRequest->removeMob(this);
		moveRequest = NULL;
	} else if ( movePath ) { // If moving already, then stop any further movements, but let it finish its current ease.
		movePath->removeUnit(this);
		movePath = NULL;
		movementMode = MOVEMENT_MODE_NONE;
	}

	return true;
}

void Mob::moveTo(std::vector<Mob*> mobs, int x, int y, bool skipChecks) {
	// Destination is closed turf.
	// OBVIOUSLY NEEDS REWORKING WITH FLYING UNITS.
	if ( !mobs[0]->world->turfs[x][y]->isOpen )
		return;

	std::vector<Mob*> validMobs;

	for ( Mob* mob : mobs )
		if ( mob->prepareMoveOrder(x, y, skipChecks) )
			validMobs.push_back(mob);

	if ( !validMobs.size() )
		return;

	F_PathingRequest* request = Flow_Pathing::requestPath(validMobs, { x, y });

	for ( Mob* mob : validMobs )
		mob->moveRequest = request;
}

bool Mob::checkMovementForCollision(glm::vec2 imminentPosition) {
	IntPair imminentGridLocation = worldToGrid(imminentPosition);
	Turf* imminentTurf = world->turfs[imminentGridLocation.x][imminentGridLocation.y];

	return true;
}