#pragma once
#include "Room.h"

class Room_CIC : public Room {
public:
	void init(int x, int y) {
		label = "Test Room 01";
		filePath = "rooms/room02";
		labelX = 1;
		labelY = 8.5;
		quads.push_back(glm::vec4(0, 0, 6, 5));
		quads.push_back(glm::vec4(0, 4, 10, 6));
		lighting = glm::vec3(LIGHTING_AMPLIFY_COEFFICIENT, LIGHTING_AMPLIFY_COEFFICIENT, LIGHTING_AMPLIFY_COEFFICIENT);
		cost = 1000;

		Room::init(x, y);
	}
};