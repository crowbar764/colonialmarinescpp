#pragma once
#include "../common/Common.h"
#include <rapidjson/document.h>

class Text;

class Room {
public:
	int x, y, cost;
	float labelX, labelY;
	std::string filePath, label;
	std::vector<glm::vec4> quads;
	glm::vec3 lighting;
	Text* labelText;

	rapidjson::Document* jsonData;

	void build();
	//void loadData();
	virtual void init(int x, int y);
	~Room();
};