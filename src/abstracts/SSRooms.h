#pragma once
#include "Room.h"

class SSRooms {
public:
	static std::vector<Room*> rooms; // List of all built rooms in the World.
	static rapidjson::Document* roomData; // Array of all JSON data for all room types.

	static rapidjson::Document* getData(int roomID); // Get JSON data of a room type.
	static void addRoom(Room*);
	static void removeRoom(Room*);
	static void init();
};