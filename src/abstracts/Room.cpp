#include "Room.h"
#include "SSRooms.h"
#include "../global/World.h"
#include "../gui/font/SSText.h"

void Room::build() {
	World::activeWorld->loadArea(jsonData, true, x, y);
	labelText = SSText::newText(label, x + labelX, y + labelY, .1f, glm::vec4(1, 1, 1, 1), false);
	SSRooms::addRoom(this);
	//delete jsonData;
}

//void Room::loadData() {
//
//}

void Room::init(int x, int y) {
	this->x = x;
	this->y = y;

	jsonData = SSRooms::getData((int)ENUM_rooms::CIC);
}

Room::~Room() {
	SSText::deleteText(labelText);
	//PRINT("delete")
}