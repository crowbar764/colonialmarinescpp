#include "SSRooms.h"
#include "../global/ResourceManager.h"
#include "../other/Gamemode_Editor.h"
#include "../common/VectorHelpers.h"

std::vector<Room*> SSRooms::rooms;
rapidjson::Document* SSRooms::roomData;

rapidjson::Document* SSRooms::getData(int roomID) {
	return roomData;
}

void SSRooms::addRoom(Room* room) {
	rooms.push_back(room);
}

void SSRooms::removeRoom(Room* room) {
	std::vector<Room*>::iterator it = find(rooms.begin(), rooms.end(), room);
	eraseFast(rooms, it);
	delete room;
}

void SSRooms::init() {
	roomData = ResourceManager::loadJSON("maps/rooms/room02");
	Gamemode_Editor::constructionGhost = roomData;
}